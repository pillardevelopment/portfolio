pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-zos/contracts/ownership/Ownable.sol";
import "../Exchange/IExchangeRate.sol";
import "../Exchange/IDiscountStore.sol";

contract ExchangeRateControl is Ownable{
    using SafeMath for uint256;

    IExchangeRate public ethRate_;

    IExchangeRate public tokenRate_;
    IDiscountStore public discountStore_;

    mapping (uint256 => uint256) public usdPrices;

    function setExchangeRateContract(address _address) public onlyOwner {
        ethRate_ = IExchangeRate(_address);
    }

    function setTokenRateContract(address _address) public onlyOwner {
        tokenRate_ = IExchangeRate(_address);
        discountStore_ = IDiscountStore(_address);
    }

    function setPrices(uint16 _type, uint256 _price) public onlyOwner {
        usdPrices[_type] = _price;
    }

    function _calculateEtherPrice(uint16 _type) internal view returns(uint256) {
        uint256 price = usdPrices[_type].mul(ethRate_.getRate());

        require(price > 0, "Price is equal to zero");

        return price;
    }

    function _calculateTokenPrice(uint16 _type) internal view returns(uint256) {
        uint256 price = usdPrices[_type].mul(tokenRate_.getRate());

        require(price > 0, "Price is equal to zero");

        return price;
    }
}
