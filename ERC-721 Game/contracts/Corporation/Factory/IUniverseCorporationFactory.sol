pragma solidity ^0.4.24;

contract IUniverseCorporationFactory {
    function getCorporationAddress(uint256 _id) public view returns (address);
}
