pragma solidity ^0.4.24;

import "../../Common/Converter.sol";
import "../../Access/TreasurerMigratable.sol";
import "../../Access/WhitelistMigratable.sol";
import "../UniverseCorporation.sol";
import "../UniverseCorporationProxy.sol";
import "../../Registry/IUniverseRegistryV1.sol";

contract UniverseCorporationFactory is WhitelistMigratable, TreasurerMigratable, Converter {

    address private registry_;

    address[] private corporations_;
    address public promoter;
    address public proxyAdmin;

    string public rootUrl;

    event NewCorporationCreated(uint256 corpId, address newCorporationProxy, address newCorporationImplementation);

    function initialize(address _registry, address _promoter, address _proxyAdmin) public isInitializer("UniverseCorporationFactory", "1.0.0") {
        registry_ = _registry;
        promoter = _promoter;
        proxyAdmin = _proxyAdmin;
        Ownable.initialize(msg.sender);
        rootUrl = "CORP";
        transferTreasurer(owner);
    }

    function setPromoterAddress(address _newPromoter) public onlyOwner {
        promoter = _newPromoter;
    }

    function getCorporationAddress(uint256 _id) public view returns (address) {
        return corporations_[_id];
    }

    function createCorporation(string name) public onlyWhitelisted() {
        uint256 id = corporations_.length;
        string memory sys = strConcat(rootUrl, uint2str(id));

        address universeCorporation = new UniverseCorporation();
        UniverseCorporationProxy universeCorporationProxy = new UniverseCorporationProxy(universeCorporation);
        universeCorporationProxy.changeAdmin(proxyAdmin);
        emit NewCorporationCreated(id, universeCorporationProxy, universeCorporation);
        UniverseCorporation(universeCorporationProxy).initialize(
            registry_,
            promoter,
            name,
            sys,
            id
        );

        corporations_.push(universeCorporationProxy);
    }

    function () public payable {
        revert("Contract cannot be funded");
    }

    uint256[50] private ______gap;
}