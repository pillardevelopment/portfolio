pragma solidity ^0.4.24;

contract IUniverseCorporation {

    function transferStake(address _from, address _to, uint256  _amount ) external;

    function balanceOf(address _who) public view returns (uint256);

    function isVoting(address _voter) public view returns(bool);
}