pragma solidity ^0.4.24;

contract IUniverseCorporationMarket {

    function createStakeSale(uint256 _corpId, uint256 _shareAmount, uint256 _price, uint256 _cashOutPart) public;

    function cancelStakeSale(uint256 _corpId, address _owner) public;

    function buyStake(uint256 _corpId, address _seller) public payable ;

    function getCurrentPrice(uint256 _corpId, address _seller) public view returns(uint256);

    function getStake(uint256 _corpId, address _seller) public view returns (uint256 price, uint256 shareAmount, uint256 cashOutPart);
}
