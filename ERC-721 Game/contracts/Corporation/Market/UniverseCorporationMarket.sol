pragma solidity ^0.4.24;

import "../../Access/TreasurerMigratable.sol";
import "../Factory/IUniverseCorporationFactory.sol";
import "../IUniverseCorporation.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "../../Registry/IUniverseRegistryV1.sol";

contract UniverseCorporationMarket is TreasurerMigratable {
    using SafeMath for uint256;

    IUniverseRegistryV1 private registry_;

    struct  Stake {
        uint256 price;
        uint256 shareAmount;
        uint256 cashOutPart;
    }

    /*** STORAGE ***/
    //    struct Stake {
    //         uint104 price;
    //         uint16 shareAmount;
    //         uint8 cashOutPart;
    //    }

    event StakeSaleCreated (address seller,
        uint256 corpId,
        uint256 shareAmount,
        uint256 price,
        uint256 cashOutPart );
    event StakeSaleCancelled( address seller, uint256 corpId);
    event StakeSold(address seller, address buyer, uint256 corpId);

    mapping(uint256 => mapping (address => uint256)) public stakes;

    address public promoter;
    uint256 public commission;

    function initialize(address _universeRegistry, address _promoter, uint256 _commission) public isInitializer("UniverseCorporationsMarket", "1.0.0") {
        Ownable.initialize(msg.sender);
        registry_ = IUniverseRegistryV1(_universeRegistry);
        promoter = _promoter;
        commission = _commission;
        transferTreasurer(owner);
    }

    function setPromoterAddress(address _newPromoter) public onlyOwner {
        promoter = _newPromoter;
    }

    function createStakeSale(uint256 _corpId, uint256 _shareAmount, uint256 _price, uint256 _cashOutPart) public {
        require(_price != 0 && _shareAmount != 0);
        require(_cashOutPart <= 100);
        IUniverseCorporationFactory factory_= IUniverseCorporationFactory(registry_.getCorporationFactory());
        IUniverseCorporation corporation = IUniverseCorporation(factory_.getCorporationAddress(_corpId));

        require(corporation.balanceOf(msg.sender) >= _shareAmount, "Insufficient share amount");
        require(!corporation.isVoting(msg.sender), "Shall not sale when voting");

        Stake memory newStake = _createStakeSale(_shareAmount, _price, _cashOutPart);
        stakes[_corpId][msg.sender] = _convertToHash(newStake);
        emit StakeSaleCreated(msg.sender, _corpId, _shareAmount, _price, _cashOutPart);
    }

    function cancelStakeSale(uint256 _corpId, address _owner) public {
        IUniverseCorporationFactory factory_= IUniverseCorporationFactory(registry_.getCorporationFactory());
        IUniverseCorporation corporation = IUniverseCorporation(factory_.getCorporationAddress(_corpId));

        require(_owner == msg.sender || factory_.getCorporationAddress(_corpId) == msg.sender);

        (uint256 _currentPrice, uint256 _currentAmount,) = getStake(_corpId, _owner);
        require(_currentPrice != 0 && _currentAmount != 0, "This lot does not exist");

        Stake memory newStake = _createStakeSale( 0, 0, 0);
        stakes[_corpId][_owner] = _convertToHash(newStake);
        emit StakeSaleCancelled(_owner, _corpId);
    }

    function buyStake(uint256 _corpId, address _seller) public payable  {
        (uint256 price,uint256 shareAmount, uint256 cashOutPart) = getStake(_corpId,  _seller);
        uint256 inputValue = price.mul(shareAmount);
        require(price > 0  && msg.value >= inputValue);

        if (inputValue < msg.value) {
            msg.sender.transfer(msg.value.sub(inputValue));
        }

        uint256 availableAmount = inputValue.sub((inputValue.mul(commission)).div(10000));
        uint256 sumForCorp = ((100 - cashOutPart).mul(availableAmount)).div(100);
        uint256 sumForSeller = availableAmount.sub(sumForCorp);

        IUniverseCorporationFactory factory_= IUniverseCorporationFactory(registry_.getCorporationFactory());
        IUniverseCorporation corporation = IUniverseCorporation(factory_.getCorporationAddress(_corpId));
        require(corporation.balanceOf(_seller) >= shareAmount);

        address(corporation).transfer(sumForCorp);
        _seller.transfer(sumForSeller);
        corporation.transferStake(_seller, msg.sender, shareAmount);

        Stake memory newStake = _createStakeSale( 0, 0, 0);
        stakes[_corpId][_seller] = _convertToHash(newStake);
        emit StakeSold (_seller, msg.sender, _corpId );
    }

    function getCurrentPrice(uint256 _corpId, address _seller)
    public view returns(uint256)
    {
        uint256 price;
        (price,,) = getStake(_corpId, _seller);
        return price;
    }

    function getStake(uint256 _corpId, address _seller)
    public view returns (uint256 price,
        uint256 shareAmount,
        uint256 cashOutPart)
    {
        uint256 hash = stakes[_corpId][_seller];
        Stake  memory currentStake = _getStake(hash);

        price = currentStake.price;
        shareAmount = currentStake.shareAmount;
        cashOutPart = currentStake.cashOutPart;
    }


    function withdrawBalance() external onlyTreasurer {
        uint256 balance = address(this).balance;

        treasurer.transfer(balance);
    }

    function _createStakeSale(   uint256 _shareAmount,
        uint256 _price,
        uint256 _cashOutPart)
    internal pure returns(Stake memory newStake) {
        newStake = Stake({
            price: _price,
            shareAmount: _shareAmount,
            cashOutPart: _cashOutPart
            });
    }

    function _convertToHash(Stake  convertStake) internal pure returns(uint256 _hash) {

        _hash = convertStake.price;
        _hash |= convertStake.shareAmount<<104;
        _hash |= convertStake.cashOutPart<<120;
    }

    function _getStake(uint256 _hash) internal pure returns(Stake  memory currentStake) {
        currentStake.price = uint256(uint104(_hash));
        currentStake.shareAmount = uint256(uint16(_hash>>104));
        currentStake.cashOutPart = uint256(uint8(_hash>>120));
    }

    uint256[50] private ______gap;
}