pragma solidity ^0.4.24;

import "../Galaxy/IUniverseGalaxy.sol";
import "../SaleAuction/ISaleAuctionExtended.sol";
import  "../LaunchRenting/IUniverseLaunchRenting.sol"; //setRentPrice
import "../PlanetNaming/IUniversePlanetNaming.sol"; //setPlanetName
import "../Settings/IUniverseBalance.sol";
import "./Market/IUniverseCorporationMarket.sol";
import "./UniverseCorporationStore.sol";
import "../Registry/IUniverseRegistryV1.sol";
import "./UniverseCorporationVoting.sol";

contract UniverseCorporation is UniverseCorporationVoting {
    using SafeMath for uint128;
    using SafeMath for uint256;

    event DividendPaidout(uint256 corpId, uint256 dividends);

    function initialize (address _universeRegistry,
                        address _promoter,
                        string _name,
                        string _symbol,
                        uint256  _corpId) public isInitializer("UniverseCorporation", "1.0.0") {
                UniverseCorporationStore.initialize (_name, _symbol, _corpId, _promoter);
                UniverseCorporationVoting.initialize (_universeRegistry);
                transferTreasurer(_promoter);
                transferOwnership(_promoter);
    }

    function () public payable { }

    function withdrawDividends(uint256 _dividends, address[] _holders) public onlyHead {
        uint256 balance = address(this).balance;
        require(balance > 0 && balance >= _dividends, "Incorrect balance");

        uint256 count;
        for (uint256 i = 0; i < _holders.length; i++) {
            count += balances[_holders[i]];
        }
        require(count == totalSupply(), "Incorrect holders");

        for (uint256 y = 0; y < _holders.length; y++) {
            _holders[y].transfer((balances[_holders[y]].mul(_dividends)).div(totalSupply()));
        }

        emit DividendPaidout(corporationId, _dividends);
    }

    // Patched ERC20 protocol :(((((
    function transfer(address _to, uint256 _value) public returns (bool){
        require(!isVoting(msg.sender), "Your tokens are blocking during the voting time");
        return super.transfer(_to, _value);
    }

    // Patched ERC20 protocol :(((((
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool) {
        require(!isVoting(_from), "Your tokens are blocking during the voting time");
        return super.transferFrom(_from, _to, _value);
    }

    function transferStake(address _from, address _to, uint256  _amount ) external onlyWhitelisted() {
        allowed[_from][msg.sender] = allowed[_from][msg.sender].add(_amount);
        transferFrom(_from, _to, _amount);
    }

    function buyPlanet(uint256 _planetID) public onlyHead {
        ISaleAuctionExtended planetAuction = ISaleAuctionExtended(registry.getSaleAuction());

        uint256 price = planetAuction.getCurrentPrice(_planetID);
        require(price <=  address(this).balance, "insufficient funds");
        planetAuction.bid.value(price)(_planetID);
    }

    function sellPlanet(uint256 _planetID, uint256 _start, uint256 _end, uint256 _duration) public onlyHead {
        IUniverseGalaxy planetContact= IUniverseGalaxy(registry.getGalaxy());
        planetContact.createSaleAuction(_planetID, _start, _end, _duration);
    }

    function giftPlanet(uint256 _planetID, address _to) public onlyHead {
        IUniverseGalaxy planetContact= IUniverseGalaxy(registry.getGalaxy());
        planetContact.transferFrom(this, _to, _planetID);
    }


    function renamePlanet(uint256 _planetID, string _newName) public onlyHead {
        IUniversePlanetNaming planetNaming = IUniversePlanetNaming(registry.getPlanetNaming());
        uint256 planetNamePrice = IUniverseBalance(registry.getBalance()).getUIntValue(43);
        require(planetNamePrice <=  address(this).balance, "insufficient funds");

        planetNaming.setPlanetName.value(planetNamePrice)(_planetID, _newName);
    }

    function leasePlanet(uint256 _planetID, uint256 _rent) public onlyHead {
        IUniverseLaunchRenting launchRenting = IUniverseLaunchRenting(registry.getLaunchRenting());
        launchRenting.setRentPrice(_planetID, _rent);
    }

    function cancelPlanetAuction(uint256 _planetID) public onlyHead {
        ISaleAuctionExtended planetAuction = ISaleAuctionExtended(registry.getSaleAuction());
        planetAuction.cancelAuction(_planetID);
    }


    uint256[50] private ______gap;
}