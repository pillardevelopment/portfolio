pragma solidity ^0.4.24;

import "openzeppelin-zos/contracts/token/ERC20/StandardToken.sol";
import "../Access/TreasurerMigratable.sol";
import "../Access/WhitelistMigratable.sol";

contract UniverseCorporationStore is StandardToken, TreasurerMigratable, WhitelistMigratable {

    event CorporationNameChanged(uint256 corpId, string oldName, string newName, address sender);
    event NewCorporationHeadChosen(uint256 corpId, address newHead, address oldHead);

    string public name;
    string public symbol;
    uint256 public corporationId;

    uint128 public duration;
    address public head;

    modifier onlyHead() {
        require(msg.sender == head);
        _;
    }

    function initialize (string  _name,
                         string  _symbol,
                         uint256 _corpId,
                         address _promoter) public  isInitializer("UniverseCorporationStore", "1.0.0") {

        Ownable.initialize(msg.sender);

        totalSupply_ = 10000;
        symbol = _symbol;
        corporationId = _corpId;
        _rename(_name);
        balances[_promoter] = totalSupply();
        emit Transfer(address(this), _promoter, totalSupply_);
    }

    function rename(string _newName) public onlyHead {
        _rename(_newName);
    }

    function _rename(string _newName) internal {
        string memory oldName = name;
        name = _newName;
        emit CorporationNameChanged(corporationId, oldName, _newName, msg.sender);
    }

    function _changeNewHead(address _newHead) internal {
        address currentHead = head;
        head = _newHead;
        emit NewCorporationHeadChosen(corporationId, _newHead, currentHead);
    }
}