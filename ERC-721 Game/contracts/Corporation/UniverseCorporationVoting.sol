pragma solidity ^0.4.24;

import "../Galaxy/IUniverseGalaxy.sol";
import "../SaleAuction/ISaleAuctionExtended.sol";
import  "../LaunchRenting/IUniverseLaunchRenting.sol"; //setRentPrice
import "../PlanetNaming/IUniversePlanetNaming.sol"; //setPlanetName
import "../Settings/IUniverseBalance.sol";
import "./Market/IUniverseCorporationMarket.sol";
import "./UniverseCorporationStore.sol";
import "../Registry/IUniverseRegistryV1.sol";

contract UniverseCorporationVoting is UniverseCorporationStore {
    using SafeMath for uint128;
    using SafeMath for uint256;

    event VoteAdded(uint256 corpId, address newHead, address voter, uint256 sharedAmount, uint256 expirationDate);
    event NewVotingCreated(uint256 corpId, address newHead, uint256 expirationDate);

    struct Voting {
        uint128 votes;
        uint128 expiration;
        address[] votersList;
    }

    IUniverseRegistryV1 public registry;

    mapping (address => Voting) public votings;

    //user's voting heads mapping
    mapping (address => address[]) internal _votingHeadsByVoter;

    function initialize(address _universeRegistry) public isInitializer("UniverseCorporationVoting", "1.0.0") {
        registry = IUniverseRegistryV1(_universeRegistry);
        addAddressToWhitelist(registry.getCorporationMarket());
        duration = 1209600; // 14 days
    }

    function updateVotingDuration(uint128 _newDuration) public onlyOwner{
        require(_newDuration > 0, "Duration need more");
        duration = _newDuration;
    }

    function isVoting(address _voter) public view returns(bool) {
        address[] storage votingHeads = _votingHeadsByVoter[_voter];
        for(uint256 i = 0; i < votingHeads.length; i++) {
            if(votings[votingHeads[i]].expiration > now) return true;
        }
        return false;
    }

    function vote(address _candidate) public {
        address voter = msg.sender;

        require(balances[voter] > 0, "You are not a member of this corporation");
        require(_candidate != head, "Don't vote for current head");

        if (votings[_candidate].votes ==  uint128(0) || votings[_candidate].expiration < now) { // createNewVoting
            require(balances[voter] >= 500, "You need more 500 stakes for creation of a new voting");
            _createVoting(_candidate);
        }

        _forbidRepeatedVote(_candidate, voter);

        _addVote(_candidate, voter);
        _checkAndAppointNewHead(_candidate, voter);
        _cancelStakeSaleFor(voter);
    }

    function _createVoting(address _candidate) internal {
        votings[_candidate] = Voting({
            votes: 0,
            expiration: uint128(now.add(duration)),
            votersList: new address[](0)
        });

        emit NewVotingCreated(corporationId, _candidate, now.add(duration) );
    }

    function _appointNewHead(address _winner, address _voter) internal {
        _changeNewHead(_winner);
        _cleanVoting(_winner, _voter);
    }

    function _cleanVoting(address _candidate, address _voter) internal {
        votings[_candidate].votes = 0;
        votings[_candidate].expiration = 0;
        delete votings[_candidate].votersList;

        _removeVotingHead(_candidate, _voter);
    }

    function _removeVotingHead(address _candidate, address _voter) internal {
        address[] storage votingHeads = _votingHeadsByVoter[_voter];
        for(uint256 i = 0; i < votingHeads.length; i++) {
            if(votingHeads[i] != _candidate) continue;
            votingHeads[i] = votingHeads[votingHeads.length - 1];
            votingHeads.length--;
            break;
        }
    }

    function _forbidRepeatedVote(address _candidate, address  _voter) internal view {
        for(uint256 i = 0; i < votings[_candidate].votersList.length; i++) {
            require(votings[_candidate].votersList[i] != _voter, "The address has already voted ");
        }
    }

    function _checkAndAppointNewHead(address _candidate, address _voter) internal {
        if (votings[_candidate].votes <= 5000) return;

        _appointNewHead(_candidate, _voter);
    }

    function _addVote(address _candidate, address _voter) internal  {
        uint128 stake = uint128(balances[_voter]);
        votings[_candidate].votes = uint128(votings[_candidate].votes.add(stake));
        votings[_candidate].votersList.push(_voter);

        addVotingHead(_candidate, _voter);

        emit VoteAdded(corporationId, _candidate, _voter, balances[_voter], votings[_candidate].expiration);
    }

    function addVotingHead(address _candidate, address _voter) internal {
        _votingHeadsByVoter[_voter].push(_candidate);
    }

    function _cancelStakeSaleFor(address voter) internal {
        IUniverseCorporationMarket market = IUniverseCorporationMarket(registry.getCorporationMarket());
        if (market.getCurrentPrice(corporationId, voter) > 0) {
            market.cancelStakeSale(corporationId, voter);
        }
    }

    function getCandidateVotes(address _candidate) public view returns(uint128) {
        return votings[_candidate].votes;
    }

    function getCandidateVoters(address _candidate) public view returns(address[] votersList) {
        votersList =  votings[_candidate].votersList;
    }

    function getCandidateVotersCount(address _candidate) public view returns(uint256) {
        return votings[_candidate].votersList.length;
    }

    uint256[50] private ______gap;
}