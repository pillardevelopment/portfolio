pragma solidity ^0.4.24;

contract IUniverseDiscovery {

    function createPromoPlanet( uint256 _sectorX,
                                uint256 _sectorY,
                                uint256 _rarity,
                                address _owner) external returns (uint);
}
