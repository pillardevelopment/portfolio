pragma solidity 0.4.24;

//TODO: run as separate contract
contract UniverseDiscoveryConstant {
    //ships
    uint256 internal constant MAX_RANKS_COUNT = 20;

    //resources
    uint256 internal constant MAX_ID_LIST_LENGTH = 5;
}
