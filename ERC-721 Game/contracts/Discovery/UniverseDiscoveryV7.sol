pragma solidity 0.4.24;

import "./UniversePromotion.sol";

contract UniverseDiscoveryV7 is UniversePromotion {

    function initialize(address _registryAddress, uint _companyCommission, uint _ownerCut) public isInitializer("UniverseDiscovery", "2.0.0") {
        UniverseLaunch.initialize(_companyCommission, _ownerCut);

        require(_registryAddress != address(0), "Registry contract address should be defined");
        registry_ = IUniverseRegistryV1(_registryAddress);
        paused = true;
    }

    function unpause() public onlyOwner whenPaused {
        require(galaxy_ != address(0), "Planet contract should be defined");
        require(saleAuction_ != address(0), "SaleAuction contract should be defined");
        require(balance_ != address(0), "Balance contract should be defined");

        // Actually unpause the contract.
        super.unpause();
    }
}
