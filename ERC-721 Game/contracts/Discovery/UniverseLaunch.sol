pragma solidity 0.4.24;

import "openzeppelin-zos/contracts/lifecycle/Pausable.sol";
import "./../Company/CommissionCollectorMigratable.sol";
import "./../Common/Random.sol";
import "../Common/MathTools.sol";
import "../Registry/IUniverseRegistryV1.sol";
import "../SaleAuction/ISaleAuctionExtended.sol";
import "../Settings/IUniverseBalance.sol";
import "../Galaxy/IUniverseGalaxy.sol";
import "../SpaceshipStore/IUniverseSpaceshipStoreV1.sol";
import "../LaunchRenting/IUniverseLaunchRenting.sol";
import "../SpaceshipInvention/IUniverseSpaceshipInvention.sol";
import "./UniverseMap.sol";
import "../Common/SignatureVerifier.sol";
import "../Access/WhitelistMigratable.sol";

contract UniverseLaunch is CommissionCollectorMigratable, UniverseMap, UniverseDiscoveryConstant, WhitelistMigratable, Random, Pausable, MathTools, SignatureVerifier  {
    using SafeMath for uint256;

    // Cut owner takes on each rent, measured in basis points (1/100 of a percent).
    // Values 0-10,000 map to 0%-100%
    uint public ownerCut;

    // Count of launches by user address
    mapping(address => uint256) public launches;

    function initialize(uint _companyCommission, uint _ownerCut) public isInitializer("UniverseLaunch", "1.0.0") {
        CommissionCollectorMigratable.initialize(_companyCommission);

        ownerCut = _ownerCut;
    }

    event SpaceshipBuilt(
        address indexed owner,
        uint256 indexed spaceshipDraftId,
        uint[MAX_ID_LIST_LENGTH] nextResourcesId,
        uint[MAX_ID_LIST_LENGTH] nextResourcesNeeded,
        uint256 indexed launchPlanetId
    );

    IUniverseRegistryV1 public registry_;
    ISaleAuctionExtended public saleAuction_;
    IUniverseBalance public balance_;
    IUniverseGalaxy public galaxy_;
    IUniverseSpaceshipStoreV1 public spaceshipStore_;
    IUniverseLaunchRenting public launchRenting_;
    IUniverseSpaceshipInvention public spaceshipInvention_;

    function cacheRegistry() onlyOwner whenPaused external  {
        saleAuction_ = ISaleAuctionExtended(registry_.getSaleAuction());
        balance_ = IUniverseBalance(registry_.getBalance());
        galaxy_ = IUniverseGalaxy(registry_.getGalaxy());
        launchRenting_ = IUniverseLaunchRenting(registry_.getLaunchRenting());

//      // Invention has been removed from blockchain
//        spaceshipStore_ = IUniverseSpaceshipStoreV1(registry_.getSpaceshipStore());
//        spaceshipInvention_ = IUniverseSpaceshipInvention(registry_.getSpaceshipInvention());
    }

    function discoveryPlanet(
        uint _launchPlanetId,
        uint _spaceshipDraftId,
        uint _spaceshipRadar,
        uint _spaceshipEngine,
        uint _spaceshipPeople,
        bytes _signature)
    payable
    external whenNotPaused {
        uint sectorX;
        uint sectorY;
        (,, sectorX, sectorY,,) = galaxy_.getPlanet(_launchPlanetId);

        address discoverer = msg.sender;

        uint256 currentLaunch = launches[discoverer] + 1;

        // This recreates the message that was signed on the client.
        bytes32 message = prefixed(keccak256(
                discoverer,
                _launchPlanetId,
                _spaceshipDraftId,
                _spaceshipRadar,
                _spaceshipEngine,
                _spaceshipPeople,
                currentLaunch,
                address(this))
        );

        require(whitelist[recoverSigner(message, _signature)], "The signature is an incorrect");

        launches[discoverer] = currentLaunch;

        _launchExpedition(
            galaxy_.ownerOf(_launchPlanetId),
            _getSector(sectorX, sectorY),
            _launchPlanetId,
            _spaceshipDraftId,
            _spaceshipRadar,
            _spaceshipEngine,
            _spaceshipPeople
        );
    }

    function _launchExpedition(
        address _planetOwner,
        Sector _sector,
        uint256 _launchPlanetId,
        uint256 _spaceshipDraftId,
        uint _spaceshipRadar,
        uint _spaceshipEngine,
        uint _spaceshipPeople
    )
    internal
    returns(uint) {
        if ( msg.sender != _planetOwner ) {
            _chargeRentingLaunchPrice(_launchPlanetId, _planetOwner, _spaceshipPeople, _sector);
        }

        // Строим звездолет, но без траты ресурсов
        _buildSpaceship(
            _launchPlanetId,
            _spaceshipDraftId
        );

        // Определяем зону поиска neighbors и отделяем от нее самый разряженный сектор bestSector
        Sector[MAX_NEIGHBORS_COUNT] memory neighbors;
        Sector memory bestSector;
        (bestSector, neighbors) = _getReachableArea(_sector, _spaceshipEngine);

        // Пробуем найти планету
        if(!_canPlanetBeDiscovered(bestSector, neighbors)) return 0;

        // Определяем параметры найденной планеты
        return _launchSpaceship(msg.sender, _spaceshipRadar, _spaceshipPeople, bestSector, neighbors);
    }

    function _chargeRentingLaunchPrice(uint _launchPlanetId, address _planetOwner, uint _peopleNeeded, Sector _sector) internal {
        uint planetPopulation = galaxy_.getPlanetCurrentResources(_launchPlanetId)[0];

        require(_peopleNeeded <= planetPopulation, "Needed people more that existed");

        uint rentPrice = launchRenting_.getRentPrice(_launchPlanetId);

        require(rentPrice != 0, "Launching price should be initiated");

        uint256 rentAmount = rentPrice.mul(_peopleNeeded);

        require(msg.value >= rentAmount, "Not enough funds for renting");

        uint256 exceedPayment = msg.value.sub(rentAmount);

        uint256 rentingCut = _computeCut(rentAmount);

        address realPlanetOwner = _planetOwner;
        if(_planetOwner == address(saleAuction_)){
            (realPlanetOwner,,,,) = saleAuction_.getAuction(_launchPlanetId);
        }

        realPlanetOwner.transfer(rentAmount.sub(rentingCut));
        _logCommission(_sector.x, _sector.y, rentingCut);

        // return exceed amount
        msg.sender.transfer(exceedPayment);
    }

    function _computeCut(uint256 _price) internal view returns (uint256) {
        return _price * ownerCut / 10000;
    }

    function _buildSpaceship(
        uint _launchPlanetId,
        uint _spaceshipDraftId
    )
    internal {
        // Повышаем стоимость следующего звездолета того же типа (draft), но не в случае запуска с Земли
        if(_launchPlanetId == 0) return;

        uint[MAX_ID_LIST_LENGTH] memory emptyArray = [uint(0),0,0,0,0];
        emit SpaceshipBuilt(msg.sender, _spaceshipDraftId, emptyArray, emptyArray, _launchPlanetId);
    }

    function _getReachableArea(Sector _launchSector, uint256 _spaceshipEngine)
    internal
    returns(Sector _bestSector, Sector[MAX_NEIGHBORS_COUNT] _neighbors) {
        // Определяем зону поиска
        _neighbors = _getNeighboursReachable(_launchSector, _spaceshipEngine);

        // Находим наиболее разреженный сектор, т.е. с меньшим количеством уже найденых планет
        _bestSector = _neighbors[0];
        uint bestSectorIndex = 0;
        for (uint s = 1; s < _neighbors.length; s++) {
            if(_isNullSector(_neighbors[s])) { break; }
            if (
                galaxy_.getDiscoveredPlanetsDensity(_bestSector.x, _bestSector.y) <
                galaxy_.getDiscoveredPlanetsDensity(_neighbors[s].x, _neighbors[s].y))
            {
                _bestSector = _neighbors[s];
                bestSectorIndex = s;
            }
        }

        // Убираем bestSector из neighbors
        _neighbors[bestSectorIndex] = _neighbors[s - 1];
        _neighbors[s - 1] = _getNullSector();
    }

    function _canPlanetBeDiscovered(Sector _bestSector, Sector[MAX_NEIGHBORS_COUNT] _neighbors)
    internal
    returns (bool) {
        uint engineModifierFixX100 = balance_.getUIntValue(/* planetDiscovery_engineModifierFixX100 */ 42);
        uint engineModifierX100 = balance_.getUIntValue(/* planetDiscovery_engineModifierX100 */ 12);

        // Определяем базовую вероятность
        uint chanceToFind = _divisionWithRound(galaxy_.getDiscoveredPlanetsDensity(_bestSector.x, _bestSector.y)
            * engineModifierFixX100, 100);

        // Увеличиваем вероятность найти планету за счет наличия других секторов
        for (uint si = 0; si < _neighbors.length; si++) {
            Sector memory sec = _neighbors[si];
            if(_isNullSector(sec)) { break; }

            chanceToFind += _divisionWithRound(galaxy_.getDiscoveredPlanetsDensity(sec.x, sec.y)
                * engineModifierFixX100 * engineModifierX100, 10000);
        }

        return _randChance(chanceToFind);
    }

    function _launchSpaceship(
        address _owner,
        uint _spaceshipRadar,
        uint _spaceshipPeople,
        Sector _bestSector,
        Sector[MAX_NEIGHBORS_COUNT] _neighbors)
    internal
    returns(uint) {
        // Определяем редкость планеты исходя из мощности радара
        uint256 rarity = _getRarity(_spaceshipRadar);

        // Если нашли Черную дыру, а их уже достаточно, значит понижаем редкость
        if(rarity == 4 && galaxy_.countPlanetsByRarity(4) == balance_.getUIntValue(/* "galaxy_forth_rarity_planet_max_count" */ 38)){
            rarity = 3;
        }

        // Если нашли легендарную планету, но в этой группе секторов уже есть одна, то...
        if (rarity == 3 && !_canThirdRarityPlanetBeDiscovered(_bestSector))
        {
            // пытаемся найти другой сектор из neighbors
            Sector memory betterSector = _lookingForThirdRarityAvailableBetterSector(_neighbors);

            if(!_isNullSector(betterSector)) {
                _bestSector = betterSector;
            }
            // если не получается, то понижаем редкость
            else {
                rarity = 2;
            }
        }

        // Уменьшает количество колонистов (не все выжили)
        uint[2] memory peopleLossX100 = balance_.getUIntArray2Value(/* "planetDiscovery_peopleLossX100" */ 36);

        return galaxy_.createPlanet(
            _owner,
            rarity,
            _bestSector.x,
            _bestSector.y,
            _divisionWithRound(_spaceshipPeople * _randRange(peopleLossX100[0], peopleLossX100[1]), 100)
        );
    }

    function _canThirdRarityPlanetBeDiscovered(Sector _sector)
    internal view
    returns (bool) {
        uint groupIndex = balance_.getGroupId(_sector.x, _sector.y);
        return galaxy_.countPlanetsByRarityInGroup(groupIndex, 3) == 0;
    }

    function _lookingForThirdRarityAvailableBetterSector(Sector[MAX_NEIGHBORS_COUNT] neighbours)
    internal view
    returns (Sector) {
        Sector memory sector = _getNullSector();

        for (uint i = 0; i < neighbours.length; i++) {
            Sector memory neighbour = neighbours[i];

            if(_isNullSector(neighbour)) { break; }

            if (_canThirdRarityPlanetBeDiscovered(neighbour))
            {
                sector = neighbour;
                break;
            }
        }

        return sector;
    }

    function _getNeighboursReachable(Sector startSector, uint count)
    internal
    returns (Sector[MAX_NEIGHBORS_COUNT] sectors) {
        uint l = 0;

        if (count < 1 || count > MAX_NEIGHBORS_COUNT)
        {
            sectors[0] = _getNullSector();
            return sectors;
        }

        if (count == 1)
        {
            sectors[0] = startSector;
            sectors[1] = _getNullSector();
            return sectors;
        }

        uint radius = 0;

        if (count <= 9)
            radius = 1;
        else if (count <= 25)
            radius = 2;
        else if (count <= 49)
            radius = 3;
        else // if (count <= MAX_NEIGHBORS_COUNT)
            radius = 4;

        uint x1;
        uint x2;
        uint y1;
        uint y2;

        uint i;
        uint j;

        x1 = startSector.x > radius ? startSector.x - radius : 0;
        x2 = startSector.x + radius < SECTOR_X_MAX ? startSector.x + radius : SECTOR_X_MAX - 1;
        // todo: объединить циклы и убрать лишние переменные
        if (startSector.y  < radius) // Если пересекают левый край
        {
            y1 = 0;
            y2 = startSector.y + radius;
            for (i = x1; i <= x2; i++) {
                for (j = y1; j <= y2; j++) {
                    sectors[l] = Sector(i, j);
                    l++;
                }
            }

            y1 = SECTOR_Y_MAX + (startSector.y - radius);
            y2 = SECTOR_Y_MAX - 1;
            for (i = x1; i <= x2; i++) {
                for (j = y1; j <= y2; j++) {
                    sectors[l] = Sector(i, j);
                    l++;
                }
            }
        }
        else if (startSector.y + radius < SECTOR_Y_MAX) // Если поцентру
        {
            y1 = startSector.y - radius;
            y2 = startSector.y + radius;
            for (i = x1; i <= x2; i++) {
                for (j = y1; j <= y2; j++) {
                    sectors[l] = Sector(i, j);
                    l++;
                }
            }
        }
        else // Если пересекают правый край
        {
            y1 = startSector.y - radius;
            y2 = SECTOR_Y_MAX - 1;
            for (i = x1; i <= x2; i++) {
                for (j = y1; j <= y2; j++) {
                    sectors[l] = Sector(i, j);
                    l++;
                }

            }

            y1 = 0;
            y2 = startSector.y + radius - SECTOR_Y_MAX;
            for (i = x1; i <= x2; i++) {
                for (j = y1; j <= y2; j++) {
                    sectors[l] = Sector(i, j);
                    l++;
                }
            }
        }

        uint random;

        while (l > count) {
            l--;

            random = _randRange(0, l);

            if (random != l) {
                sectors[random] = sectors[l]; // последний элемент переносим на место извлечённого
            }
            sectors[l] = _getNullSector(); // а его место заполняем нулевым сектором
        }

        if (l < MAX_NEIGHBORS_COUNT) { // l = индекс последнего сектора + 1
            sectors[l] = _getNullSector(); // создаём терминирующий сектор, на случай если там сектор (0,0), т.е. не заполнен
        }

        return sectors;
    }

    function _getRarity (uint256 _spaceshipRadar)
    internal
    returns (uint256) {
        uint[3] memory balance_modulesRange = balance_.getUIntArray3Value(/* "spaceshipInvention_modulesRangeX100" */ 8);

        uint spaceshipRanksCount = balance_.getUIntValue(/* settings_ranks_count */ 40);
        uint[3] memory minRankParams = balance_.getRankParamsValue(0);
        uint[3] memory maxRankParams = balance_.getRankParamsValue(spaceshipRanksCount - 1);

        // Получаем параметры самого маломощного и самого можного радара
        uint minRadarX100 = minRankParams[0] * balance_modulesRange[0];
        uint maxRadarX100 = maxRankParams[0] * balance_modulesRange[2];

        // Нормализуем мощность радара к значению 0..100
        uint radarMultiplier = _divisionWithRound(100 * (100 * _spaceshipRadar - minRadarX100), (maxRadarX100 - minRadarX100));

        uint random = _randRange(0, 1000);

        // Получаем базовую вероятность
        uint[2] memory firstRarityChance = balance_.getUIntArray2Value(/* "planetDiscovery_chanceToGetFirstRarityX1000" */ 9);
        uint[2] memory secondRarityChance = balance_.getUIntArray2Value(/* "planetDiscovery_chanceToGetSecondRarityX1000" */ 10);
        uint[2] memory thirdRarityChance = balance_.getUIntArray2Value(/* "planetDiscovery_chanceToGetThirdRarityX1000" */ 11);
        uint[2] memory forthRarityChance = balance_.getUIntArray2Value(/* "planetDiscovery_chanceToGetForthRarityX1000" */ 37);

        // Увеличиваем вероятность на нормализованую мощность радара
        if (random < (_divisionWithRound(radarMultiplier * (forthRarityChance[1] - forthRarityChance[0]), 100) + forthRarityChance[0]))
            return 4;
        else if (random < (_divisionWithRound(radarMultiplier * (thirdRarityChance[1] - thirdRarityChance[0]), 100) + thirdRarityChance[0]))
            return 3;
        else if (random < (_divisionWithRound(radarMultiplier * (secondRarityChance[1] - secondRarityChance[0]), 100) + secondRarityChance[0]))
            return 2;
        else if (random < (_divisionWithRound(radarMultiplier * (firstRarityChance[1] - firstRarityChance[0]), 100) + firstRarityChance[0]))
            return 1;
        else
            return 0;
    }
}
