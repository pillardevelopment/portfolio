pragma solidity 0.4.24;

import "../Common/UniverseGalaxyConstant.sol";

contract UniverseMap is UniverseGalaxyConstant{

    uint internal constant MAX_NEIGHBORS_COUNT = 81;

    uint internal constant NULL_SECTOR_COORDS = 9999;

    struct Sector {
        uint x;
        uint y;
    }

    function _getSector(uint x, uint y) internal pure returns (Sector) {
        return Sector( { x: x, y: y });
    }

    function _getNullSector() internal pure returns (Sector){
        return Sector( { x: NULL_SECTOR_COORDS, y: NULL_SECTOR_COORDS });
    }

    function _isNullSector(Sector sector) internal pure returns (bool){
        return sector.x == NULL_SECTOR_COORDS && sector.y == NULL_SECTOR_COORDS;
    }

    function _getNeighbours(Sector s) internal pure returns (Sector[4]) {
        Sector[4] memory neighbours;
        uint x = s.x;
        uint y = s.y;
        if(y != 0) {
            neighbours[0] = _getSector(x, y - 1); // up
        } else {
            neighbours[0] = _getNullSector();
        }

        if(x == SECTOR_X_MAX - 1) {
            neighbours[1] = _getSector(0, y); // right
        } else {
            neighbours[1] = _getSector(x + 1, y); // right
        }

        if(y != SECTOR_Y_MAX - 1) {
            neighbours[2] = _getSector(x, y + 1); // down
        } else {
            neighbours[2] = _getNullSector();
        }

        if(x == 0) {
            neighbours[3] = _getSector(SECTOR_X_MAX - 1, y); // right
        } else {
            neighbours[3] = _getSector(x - 1, y); // right
        }

        return neighbours;
    }

    function _areSectorsEquals(Sector sec1, Sector sec2) internal pure returns (bool){
        return sec1.x == sec2.x && sec1.y == sec2.y;
    }

    function _containsSector(Sector[] _sectors, Sector _sector) internal pure returns (bool) {
        for (uint i = 0; i < _sectors.length; i++) {
            Sector memory current = _sectors[i];
            if(_areSectorsEquals(current, _sector)){
                return true;
            }
        }
        return false;
    }
}
