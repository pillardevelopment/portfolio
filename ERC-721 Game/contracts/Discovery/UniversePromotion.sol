pragma solidity 0.4.24;

import "./UniverseLaunch.sol";

contract UniversePromotion is UniverseLaunch {

    modifier whenSectorCoordinatesInTheRange(uint256 _sectorX, uint256 _sectorY) {
        require(
            _sectorX >= 0 && _sectorY >= 0 &&
            _sectorX < SECTOR_X_MAX && _sectorY < SECTOR_Y_MAX,
            "Sector coordinates should be in the range"
        );
        _;
    }

    function createPromoPlanet(uint256 _sectorX, uint256 _sectorY, uint256 _rarity, address _owner)
    external
    onlyWhitelisted
    whenNotPaused
    whenSectorCoordinatesInTheRange(_sectorX, _sectorY)
    returns (uint)
    {
        address planetOwner = _owner;

        if (planetOwner == address(0)) {
            planetOwner = msg.sender;
        }

        galaxy_.checkWhetherEnoughPromoPlanet();

        return galaxy_.createPlanet(planetOwner, _rarity, _sectorX, _sectorY,
            balance_.getUIntValue(/* settings_start_population_promo_planet */ 41));
    }

    function createPlanetForAuction(
        uint256 _sectorX,
        uint256 _sectorY
    )
    external
    onlyWhitelisted
    whenNotPaused
    whenSectorCoordinatesInTheRange(_sectorX, _sectorY)
    {
        uint earthId = 0;
        uint earthSpaceshipId = 0;
        uint[3] memory shipResourcesValues = balance_.getUIntArray3Value(/* "earth_shipParams_resources_m_values" */ 29);

        uint256 planetId = _launchExpedition(
            msg.sender,
            _getSector(_sectorX, _sectorY),
            earthId,
            earthSpaceshipId,
            balance_.getUIntValue(/* "earth_shipParams_radar" */ 26),
            balance_.getUIntValue(/* "earth_shipParams_engine" */ 27),
            shipResourcesValues[0]
        );
        // return if expedition has been failed
        if(planetId == 0) {
            return;
        }

        galaxy_.approve(saleAuction_, planetId);

        uint rarity;
        (rarity,,,,,) = galaxy_.getPlanet(planetId);
        uint256 averagePrice = _computeNextSalePrice(rarity);

        saleAuction_.createAuction(
            planetId,
            averagePrice * balance_.getUIntValue(/* "earth_auction_startPriceModifierX100" */ 31) / 100,
            averagePrice * balance_.getUIntValue(/* "earth_auction_endPriceModifierX100" */ 32) / 100,
            balance_.getUIntValue(/* "earth_auction_duration" */ 30) * 24 * 60 * 60,
            msg.sender
        );
    }

    function restartPlanetAuction(uint256 _planetId)
    external
    onlyWhitelisted
    whenNotPaused
    {
        if (balance_.autoClearAuction()) saleAuction_.clearOne(msg.sender, _planetId);

        galaxy_.approve(saleAuction_, _planetId);

        uint rarity;
        (rarity,,,,,) = galaxy_.getPlanet(_planetId);
        uint256 averagePrice = _computeNextSalePrice(rarity);

        saleAuction_.createAuction(
            _planetId,
            averagePrice * balance_.getUIntValue(/* "earth_auction_startPriceModifierX100" */ 31) / 100,
            averagePrice * balance_.getUIntValue(/* "earth_auction_endPriceModifierX100" */ 32) / 100,
            balance_.getUIntValue(/* "earth_auction_duration" */ 30) * 24 * 60 * 60,
            msg.sender
        );
    }

    function _computeNextSalePrice(uint256 _rarity) internal view returns (uint256) {
        uint256 avePrice = saleAuction_.averageExpansionSalePrice(_rarity);

        if(avePrice == 0) {
            uint[4] memory auctionDefaultPrices = balance_.getUIntArray4Value(/* "planets_auction_defaultPricesWEI" */ 19);
            avePrice = auctionDefaultPrices[_rarity];
        }

        // Sanity check to ensure we don't overflow arithmetic
        require(avePrice == uint256(uint128(avePrice)), "Price can't be bigger that 2 ** 128");

        //uint256 nextPrice = avePrice + (avePrice / 2);
        uint256 nextPrice = avePrice;

        uint256 minimalPrice = balance_.getUIntValue(/* "earth_auction_endPriceMinWEI" */ 33);

        // We never auction for less than starting price
        if (nextPrice < minimalPrice) {
            nextPrice = minimalPrice;
        }

        return nextPrice;
    }
}
