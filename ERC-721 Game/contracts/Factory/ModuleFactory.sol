pragma solidity ^0.4.24;

import "../Tools/Random.sol";
import "../Module/IERC-721_Game.sol";
import "../Access/TreasurerMigratable.sol";
import "../Access/WhitelistMigratable.sol";


contract ModuleFactory is TreasurerMigratable, Random, WhitelistMigratable {

    event NewBoxBought(address buyer, uint16 typeBox);
    event BoxGifted(address sender, uint16 typeBox);

    ERC-721_Game public erc_721_Game_;

    uint256[4] price;

    function initialize(address _moduleAddress) public isInitializer("ModuleFactory", "1.0.0") {
        require(_moduleAddress != address(0), "address incorrect");
        Ownable.initialize(msg.sender);

        transferTreasurer(owner);

        setModuleContract(_moduleAddress);

        price[0] = 10000000000000000;
        price[1] = 100000000000000000;
        price[2] = 1000000000000000000;
        price[3] = 10000000000000000000;
    }


    function setModuleContract(address _moduleAddress) public onlyOwner {
        erc_721_Game_ = IERC-721_Game(_moduleAddress);
    }


    function buyBox (uint16 _boxType) public payable {
        require(_boxType >= 0 && _boxType <= 3, "TypeBox cannot be more than 3");
        require(msg.value >= price[_boxType], "Insufficient equity");

        _issue(msg.sender, _boxType);

        emit NewBoxBought(msg.sender, _boxType);
    }


    function giftBox(uint8 _boxType, address _to) public onlyWhitelisted() {
        require(_to != address(0), "incorrect address, try again");
        require(_boxType >= 0 && _boxType <= 3, "TypeBox cannot be more than 3");

        _issue(_to, _boxType);

        emit BoxGifted(_to, _boxType);
    }


    function setBoxPrices(uint16 _type, uint256 _price) public onlyOwner {
        price[_type] = _price;
    }


    function getBoxPrices() public view returns(uint256, uint256, uint256, uint256) {
        return(price[0], price[1], price[2], price[3]);
    }


    function () public payable {
        revert();
    }


    function withdrawFactoryBalance() external onlyTreasurer {
        uint256 balance = address(this).balance;

        treasurer.transfer(balance);
    }


    function _issue(address _to, uint16 _type) internal view {
        uint16[3] memory modules;

        modules = _generateRarities( [600, 300, 95, 5] );
        _produceBox(_to, modules);
    }


    function _produceBox(address _to, uint16[3] _list ) internal view {
        for (uint i=0; i<3; i++) {
            erc_721_Game_.mintModule(_to, "module", uint8(_list[i]), _generateType());
        }
    }


    function _generateRarities ( uint16[4] memory _chances ) internal view returns ( uint16[3] ) {
        uint16[3] memory result;

        for (uint16 i=0; i<3; i++)  {
            uint256 chance = _randRange(1,1000);

            if (chance <= _chances[0]) {
                result[i] = 0;
            }
            if (chance <= _chances[1]) {
                result[i] = 1;
            }
            if (chance <= _chances[2]) {
                result[i] = 2;
            }
            if (chance <= _chances[3]) {
                result[i] = 3;
            }
        }
        return result;
    }


    function _generateType() internal view returns (uint8) {
        return uint8(_randRange(0, 10));
    }


    function _assureModuleWithRarity(uint16[3] memory _moduleList, uint16 _garant) internal pure returns (uint16[3]){
        if (!_hasModuleWithRarity(_moduleList, _garant)) {
            uint16 min_rarity_idx = _findMinRarity(_moduleList);
            _moduleList[min_rarity_idx] = _garant;
        }
        return _moduleList;
    }


    function _hasModuleWithRarity(uint16[3] memory _moduleList, uint16 _garant) internal pure returns (bool) { 
        for (uint16 i = 1; i < 3; i++) {
            if (_moduleList[i] >= _garant) {
                return true;
            } else
                return false;
        }
    }


    function _findMinRarity (uint16[3] memory _moduleList) internal pure returns (uint16) {
        uint16 result = 0;

        for (uint16 i = 1; i < 3; i++) {
            if (_moduleList[i] < _moduleList[result]) {
                result = i;
            }
        }
        return result;
    }


    uint256[50] private ______gap;
}