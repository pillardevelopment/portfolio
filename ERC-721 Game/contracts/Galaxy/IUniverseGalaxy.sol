pragma solidity 0.4.24;

import "openzeppelin-solidity/contracts/token/ERC721/ERC721Basic.sol";
import "../Common/UniverseGalaxyConstant.sol";

contract IUniverseGalaxy is ERC721Basic, UniverseGalaxyConstant{

    function getPlanet(uint256 _id) external view
    returns (
        uint256 rarity,
        uint256 discovered,
        uint256 sectorX,
        uint256 sectorY,
        uint256[MAX_ID_LIST_LENGTH] resourcesId,
        uint256[MAX_ID_LIST_LENGTH] resourcesVelocity
    );

    function createSaleAuction(uint256 _planetId, uint256 _startingPrice, uint256 _endingPrice, uint256 _duration) external;

    function findAvailableResource(address _owner, uint _rarity) external returns (int8);
    function getDiscoveredPlanetsDensity(uint sectorX, uint sectorY) external view returns (uint);

    function createPlanet(
        address _owner,
        uint256 _rarity,
        uint256 _sectorX,
        uint256 _sectorY,
        uint256 _startPopulation
    ) external returns(uint256);

    function spendResources(address _owner, uint[MAX_ID_LIST_LENGTH] _resourcesId, uint[MAX_ID_LIST_LENGTH] _resourcesNeeded) external;

    function spendResourceOnPlanet(address _owner, uint _planetId, uint _resourceId, uint _resourceValue) external;

    function spendKnowledge(address _owner, uint _spentKnowledge) external;

    function recountPlanetResourcesAndUserKnowledge(address _owner, uint256 _planetId) external;

    function countPlanetsByRarityInGroup(uint _groupIndex, uint _rarity) external view returns (uint);

    function countPlanetsByRarity(uint _rarity) external view returns (uint);

    function checkWhetherEnoughPromoPlanet() external;

    function getPlanetUpdatedResources(uint256 _id)
    external
    view
    returns (
        uint256 updated,
        uint256[MAX_ID_LIST_LENGTH] resourcesId,
        uint256[MAX_ID_LIST_LENGTH] resourcesUpdated
    );

    function getPlanetCurrentResources(uint _planetId) external view returns (uint[MAX_ID_LIST_LENGTH]);
}
