pragma solidity 0.4.24;

import "./../../archive/contracts/Auction/ISaleClockAuction.sol";
import "./UniverseGalaxyStore.sol";

contract UniverseAuction is UniverseGalaxyStore {

    ISaleClockAuction public saleAuction;

    function setSaleAuctionAddress(address _address) external onlyOwner {
        ISaleClockAuction candidateContract = ISaleClockAuction(_address);

        // NOTE: verify that a contract is what we expect - https://github.com/Lunyr/crowdsale-contracts/blob/cfadd15986c30521d8ba7d5b6f57b4fefcc7ac38/contracts/LunyrToken.sol#L117
        require(candidateContract.isSaleClockAuction(), "Incorrect address param");

        // Set the new contract address
        saleAuction = candidateContract;
    }

    function createSaleAuction(
        uint256 _planetId,
        uint256 _startingPrice,
        uint256 _endingPrice,
        uint256 _duration
    )
    external
    whenNotPaused
    {
        if (universeBalance.autoClearAuction()) saleAuction.clearOne(msg.sender, _planetId);
        // Auction contract checks input sizes
        // If planet is already on any auction, this will throw
        // because it will be owned by the auction contract.
        require(ownerOf(_planetId) == msg.sender, "Not owner");

        approve(saleAuction, _planetId);
        // Sale auction throws if inputs are invalid and clears
        // transfer approval after escrowing the planet.
        saleAuction.createAuction(
            _planetId,
            _startingPrice,
            _endingPrice,
            _duration,
            msg.sender
        );
    }

    function withdrawAuctionBalances() external onlyTeam {
        saleAuction.withdrawBalance();
    }
}
