pragma solidity 0.4.24;

import "./UniverseGalaxyState.sol";

contract UniverseGalaxy is UniverseGalaxyState {

    uint256 public constant PROMO_PLANETS_LIMIT = 10000;

    uint256 public promoCreatedCount;

    constructor() public {
        paused = true;
        transferTreasurer(owner);
    }

    function initialize(address _earthOwner) external onlyOwner {
        require(planets.length == 0, "Earth was created");

        uint[2] memory earthSector = universeBalance.getUIntArray2Value(/* "earth_planet_sector" */ 20);

        uint[3] memory earthResourcesId = universeBalance.getUIntArray3Value(/* "earth_planet_resources_m_keys" */ 21);
        uint[3] memory earthResourcesVelocity = universeBalance.getUIntArray3Value(/* "earth_planet_resources_m_values" */ 22);
        uint[3] memory earthResourcesUpdated = universeBalance.getUIntArray3Value(/* "earth_planet_resourcesUpdated_m_values" */ 24);

        Planet memory earth = Planet({
            rarity: 3,
            discovered: uint256(now),
            updated: uint256(now),
            sectorX: earthSector[0],
            sectorY: earthSector[1],
            resourcesId: [earthResourcesId[0], earthResourcesId[1], earthResourcesId[2], 0, 0],
            resourcesVelocity: [earthResourcesVelocity[0], earthResourcesVelocity[1], earthResourcesVelocity[2], 0, 0],
            resourcesUpdated: [earthResourcesUpdated[0], earthResourcesUpdated[1], earthResourcesUpdated[2], 0, 0]
            });

        _savePlanet(_earthOwner, earth);
    }

    function checkWhetherEnoughPromoPlanet()
    external
    onlyWhitelisted()
    {
        promoCreatedCount++;

        require( promoCreatedCount < PROMO_PLANETS_LIMIT, "Promo planet limit is reached" );
    }

    function() external payable onlyWhitelisted() {
    }

    function unpause() public onlyOwner whenPaused {
        require(saleAuction != address(0), "SaleClock contract should be defined");
        require(universeBalance != address(0), "Balance contract should be defined");

        // Actually unpause the contract.
        super.unpause();
    }

    function withdrawBalance() external onlyTreasurer {
        uint256 balance = address(this).balance;

        treasurer.transfer(balance);
    }
}
