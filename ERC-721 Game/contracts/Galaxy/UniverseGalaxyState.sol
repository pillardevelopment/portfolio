pragma solidity 0.4.24;

import "./UniverseAuction.sol";

contract UniverseGalaxyState is UniverseAuction {

    uint internal constant SECONDS_IN_DAY = 60 * 60 * 24;

    mapping (address => uint) public ownerToKnowledge;
    mapping (address => uint) public lastKnowledgeSpentDateByOwner;

    function getPlanetUpdatedResources(uint256 _id)
    external
    view
    returns (
        uint256 updated,
        uint256[MAX_ID_LIST_LENGTH] resourcesId,
        uint256[MAX_ID_LIST_LENGTH] resourcesUpdated
    ) {
        Planet memory pl = _getPlanet(_id);

        updated = pl.updated;
        resourcesId = pl.resourcesId;
        resourcesUpdated = pl.resourcesUpdated;
    }

    function spendResourceOnPlanet(
        address _owner,
        uint _planetId,
        uint _resourcePlanetIndex,
        uint _resourceValue
    )
    external
    onlyWhitelisted()
    {
        require(_owner != address(0), "Owner param should be defined");
        require(_resourceValue > 0, "ResourceValue param should be bigger that zero");

        Planet memory planet = _getPlanet(_planetId);
        planet = _recountPlanetStateAndUpdateUserKnowledge(_owner, planet);

        require(planet.resourcesUpdated[_resourcePlanetIndex] >= _resourceValue, "Resource current should be bigger that ResourceValue");

        planet.resourcesUpdated[_resourcePlanetIndex] -= _resourceValue;
        _updatePlanetStateHash(_planetId, planet);
    }

    function spendResources(
        address _owner,
        uint[MAX_ID_LIST_LENGTH] _resourcesId,
        uint[MAX_ID_LIST_LENGTH] _resourcesNeeded
    ) external onlyWhitelisted() {
        uint ownedPlanetsCount = _getOwnedTokensCount(_owner);

        for (uint j = 0; j < _resourcesId.length; j++) { // 0-4
            uint resourceId = _resourcesId[j];
            uint resourceNeeded = _resourcesNeeded[j];

            if (resourceNeeded == 0) { continue; }

            for (uint i = 0; i < ownedPlanetsCount; i++) { // 0-n
                if (resourceNeeded == 0) { break; }

                uint planetId = _getOwnedTokensByIndex(_owner, i);
                Planet memory planet = _getPlanet(planetId);

                uint foundResourceIndex = 9999;

                for (uint k = 0; k < planet.resourcesId.length; k++) { //0-4
                    if (resourceId == planet.resourcesId[k]) {
                        foundResourceIndex = k;
                        break;
                    }
                }

                if(foundResourceIndex == 9999) {continue;}

                planet = _recountPlanetStateAndUpdateUserKnowledge(_owner, planet);
                if (planet.resourcesUpdated[foundResourceIndex] > 0) {
                    if (planet.resourcesUpdated[foundResourceIndex] >= resourceNeeded) {
                        planet.resourcesUpdated[foundResourceIndex] -= resourceNeeded;
                        resourceNeeded = 0;
                    } else {
                        resourceNeeded -= planet.resourcesUpdated[foundResourceIndex];
                        planet.resourcesUpdated[foundResourceIndex] = 0;
                    }
                }
                _updatePlanetStateHash(planetId, planet);

            }

            if (resourceNeeded > 0) {
                revert("NotEnoughResources");
            }
        }
    }

    function spendKnowledge(address _owner, uint _spentKnowledge) external onlyWhitelisted() {
        if (ownerToKnowledge[_owner] < _spentKnowledge) {
            uint balanceVelocity = universeBalance.getUIntValue(/* "settings_time_velocity" */ 34);

            uint spentKnowledge = _spentKnowledge * SECONDS_IN_DAY; // защита от потерь при округлении

            uint knowledge = ownerToKnowledge[_owner] * SECONDS_IN_DAY;

            uint ownedPlanetsCount = _getOwnedTokensCount(_owner);

            bool enoughKnowledge = false;

            for (uint i = 0; i < ownedPlanetsCount; i++) {
                Planet memory planet = _getPlanet( _getOwnedTokensByIndex(_owner, i) );

                uint interval = (_now() - _getLastKnowledgeUpdateForPlanet(_owner, planet)) * balanceVelocity;
                knowledge += (planet.resourcesUpdated[0] + _divisionWithRound(planet.resourcesVelocity[0] * interval, 2 * SECONDS_IN_DAY))
                    * universeBalance.getUIntValue(/* "planets_knowledgePerPeoplePerDay" */17)
                    * interval;

                if (knowledge >= spentKnowledge) {
                    enoughKnowledge = true;
                    break;
                }
            }

            if(!enoughKnowledge) {
                revert("NotEnoughKnowledge");
            }
        }

        ownerToKnowledge[_owner] = 0;
        lastKnowledgeSpentDateByOwner[_owner] = _now();
    }

    // Only for test purpose
    function getCurrentKnowledgeOfOwner(address _owner) external view returns(uint) {
        uint balanceVelocity = universeBalance.getUIntValue(/* "settings_time_velocity" */ 34);

        uint knowledge = ownerToKnowledge[_owner] * SECONDS_IN_DAY;

        uint ownedPlanetsCount = _getOwnedTokensCount(_owner);

        for (uint i = 0; i < ownedPlanetsCount; i++) {
            Planet memory planet = _getPlanet( _getOwnedTokensByIndex(_owner, i) );

            uint interval = (_now() - _getLastKnowledgeUpdateForPlanet(_owner, planet)) * balanceVelocity;
            knowledge += (planet.resourcesUpdated[0] + _divisionWithRound(planet.resourcesVelocity[0] * interval, 2 * SECONDS_IN_DAY))
                * universeBalance.getUIntValue(/* "planets_knowledgePerPeoplePerDay" */17)
                * interval;
        }

        return _divisionWithRound(knowledge, SECONDS_IN_DAY);
    }

    function recountPlanetResourcesAndUserKnowledge(address _owner, uint256 _planetId) external onlyWhitelisted() {
        Planet memory planet = _getPlanet(_planetId);
        planet = _recountPlanetStateAndUpdateUserKnowledge(_owner, planet);
        _updatePlanetStateHash(_planetId, planet);
    }

    function _updatePlanetStateHash(uint256 _planetID, Planet memory _planet) internal {
        _planet.updated = _now();

        uint256 planetState = _convertPlanetToPlanetStateHash(_planet);
        planetStates[_planetID] = planetState;
    }

    function _getLastKnowledgeUpdateForPlanet(address _owner, Planet memory _planet) internal view returns (uint256) {
        return ((_planet.updated > lastKnowledgeSpentDateByOwner[_owner]) ? _planet.updated : lastKnowledgeSpentDateByOwner[_owner]);
    }

    function _recountPlanetStateAndUpdateUserKnowledge(address _owner, Planet memory _planet) internal returns (Planet) {
        uint balanceVelocity = universeBalance.getUIntValue(/* "settings_time_velocity" */ 34);

        // update knowledge
        uint intervalForKnowledge = (_now() - _getLastKnowledgeUpdateForPlanet(_owner, _planet)) * balanceVelocity;
        uint knowledge = (_planet.resourcesUpdated[0] + _divisionWithRound(_planet.resourcesVelocity[0] * intervalForKnowledge, 2 * SECONDS_IN_DAY))
            * universeBalance.getUIntValue(/* "planets_knowledgePerPeoplePerDay" */ 17)
            * intervalForKnowledge;

        ownerToKnowledge[_owner] += _divisionWithRound(knowledge, SECONDS_IN_DAY);


        // update resources
        uint interval = (_now() - _planet.updated) * balanceVelocity;

        uint resourcesMultiplierMAX = universeBalance.getUIntValue(/* "planets_resourcesMaxModifier" */ 18);

        // начал j с 0, чтобы и на людей ограничение распространялось. -1 вроде был лишним, там строго меньше сравнение
        for (uint j = 0; j < _planet.resourcesVelocity.length; j++) {
            if (_planet.resourcesVelocity[j] == 0) { continue; }

            _planet.resourcesUpdated[j] += _divisionWithRound(_planet.resourcesVelocity[j] * interval, SECONDS_IN_DAY);

            uint maxResourceAmount = _planet.resourcesVelocity[j] * resourcesMultiplierMAX;
            if (_planet.resourcesUpdated[j] > maxResourceAmount) {
                _planet.resourcesUpdated[j] = maxResourceAmount;
            }
        }

        return _planet;
    }

    function getPlanetCurrentResources(uint _planetId) external view returns (uint[MAX_ID_LIST_LENGTH]) {
        uint balanceVelocity = universeBalance.getUIntValue(/* "settings_time_velocity" */ 34);

        Planet memory planet = _getPlanet(_planetId);

        uint interval = (_now() - planet.updated) * balanceVelocity;

        uint[MAX_ID_LIST_LENGTH] memory velocities = planet.resourcesVelocity;

        uint resourcesMultiplierMAX = universeBalance.getUIntValue(/* "planets_resourcesMaxModifier" */ 18);

        // начал j с 0, чтобы и на людей ограничение распространялось. -1 вроде был лишним, там строго меньше сравнение
        for (uint j = 0; j < velocities.length; j++) {
            if (velocities[j] == 0) { continue; }

            planet.resourcesUpdated[j] += _divisionWithRound(planet.resourcesVelocity[j] * interval, SECONDS_IN_DAY);

            uint maxResourceAmount = planet.resourcesVelocity[j] * resourcesMultiplierMAX;
            if (planet.resourcesUpdated[j] > maxResourceAmount) {
                planet.resourcesUpdated[j] = maxResourceAmount;
            }
        }

        return planet.resourcesUpdated;
    }
}
