pragma solidity 0.4.24;

import "./../Common/Random.sol";
import "./../Access/AccessControl.sol";
import "./../Settings/IUniverseBalance.sol";
import "../Common/MathTools.sol";
import "openzeppelin-solidity/contracts/token/ERC721/ERC721Token.sol";
import "../Common/ArrayArchiveTools.sol";
import "../PlanetExploration/IUniversePlanetExploration.sol";
import "./IUniverseGalaxy.sol";
import "../Common/WhitelistV190.sol";


contract UniverseGalaxyStore is IUniverseGalaxy, ERC721Token, WhitelistV190, AccessControl, Random, MathTools, ArrayArchiveTools {
    /*** EVENTS ***/
    event PlanetCreated(
        address indexed owner,
        uint256 indexed planetId,
        uint256 sectorX,
        uint256 sectorY,
        uint256 rarity,
        uint256[MAX_ID_LIST_LENGTH] resourcesId,
        uint256[MAX_ID_LIST_LENGTH] resourcesVelocity,
        uint256 startPopulation
    );

    /*** DATA TYPES ***/

    struct Planet {
        uint256 rarity;
        uint256 discovered;
        uint256 updated;
        uint256 sectorX;
        uint256 sectorY;
        uint[MAX_ID_LIST_LENGTH] resourcesId;
        uint[MAX_ID_LIST_LENGTH] resourcesVelocity;
        uint[MAX_ID_LIST_LENGTH] resourcesUpdated;
    }

    /*** STORAGE ***/

    //    struct Planet {
    //        uint48 discovered;
    //        uint40 resourcesId;
    //        uint40 resourcesVelocity;
    //        uint8 sectorX;
    //        uint8 sectorY;
    //        uint8 rarity;
    //    }
    uint256[] public planets;

    //    struct PlanetState {
    //        uint48 updated;
    //        uint40 resourcesId;
    //        uint80 resourcesUpdated;
    //    }
    mapping (uint256 => uint256) planetStates;

    // x => (y => discovered_planet_count)
    mapping (uint => mapping ( uint => uint )) discoveredPlanetsCountMap;

    // group index => rarity => discovered planet count
    mapping (uint => mapping (uint => uint)) planetCountByRarityInGroups;

    // rarity => discovered planet count in galaxy
    mapping (uint => uint) planetCountByRarity;

    IUniverseBalance public universeBalance;
    IUniversePlanetExploration public universePlanetExploration;

    constructor() ERC721Token("0xUniverse", "PLANET")
    public { }

    function _getPlanet(uint256 _id)
    internal view
    returns(Planet memory _planet)
    {
        uint256 planet = planets[_id];
        uint256 planetState = planetStates[_id];

        _planet.discovered = uint256(uint48(planet));
        _planet.resourcesId = _splitUint40ToArray(uint40(planet >> 48));
        _planet.resourcesVelocity = _splitUint40ToArray(uint40(planet >> 88));
        _planet.sectorX = uint256(uint8(planet >> 128));
        _planet.sectorY = uint256(uint8(planet >> 136));
        _planet.rarity = uint256(uint8(planet >> 144));

        _planet.updated = uint256(uint48(planetState));
        _planet.resourcesUpdated = _splitUint80ToArray(uint80(planetState >> 88));
    }

    function _convertPlanetToPlanetHash(Planet memory _planet)
    internal
    pure
    returns(uint256 _planetHash)
    {
        _planetHash = _planet.discovered;
        _planetHash |= _mergeArrayToUint40(_planet.resourcesId) << 48;
        _planetHash |= _mergeArrayToUint40(_planet.resourcesVelocity) << 88;
        _planetHash |= _planet.sectorX << 128;
        _planetHash |= _planet.sectorY << 136;
        _planetHash |= uint256(_planet.rarity) << 144;
    }

    function _convertPlanetToPlanetStateHash(Planet memory _planet)
    internal
    pure
    returns(uint256 _planetStateHash)
    {
        _planetStateHash = _planet.updated;
        _planetStateHash |= _mergeArrayToUint40(_planet.resourcesId) << 48;
        _planetStateHash |= _mergeArrayToUint80(_planet.resourcesUpdated) << 88;
    }

    // ВНИМАНИЕ: На самом деле это не Плотность, а Разряженность, но править уже поздно
    function getDiscoveredPlanetsDensity(uint sectorX, uint sectorY) external view returns (uint) {
        uint discoveredPlanetsCount = discoveredPlanetsCountMap[sectorX][sectorY];
        // жёсткая проверка на количество планет в секторе и защита от переполнения переменной
        if (discoveredPlanetsCount >= PLANETS_COUNT_PER_SECTOR) {
            return 0;
        }
        return 100 - (discoveredPlanetsCount * 100) / PLANETS_COUNT_PER_SECTOR;
    }

    function countPlanetsByRarityInGroup(uint _groupIndex, uint _rarity) external view returns (uint){
        return planetCountByRarityInGroups[_groupIndex][_rarity];
    }

    function countPlanetsByRarity(uint _rarity) external view returns (uint){
        return planetCountByRarity[_rarity];
    }

    function setUniverseBalanceAddress(address _address) external onlyOwner {
        IUniverseBalance candidateContract = IUniverseBalance(_address);

        // NOTE: verify that a contract is what we expect
        // https://github.com/Lunyr/crowdsale-contracts/blob/cfadd15986c30521d8ba7d5b6f57b4fefcc7ac38/contracts/LunyrToken.sol#L117
        require(candidateContract.isUniverseBalance(), "Incorrect address param");

        // Set the new contract address
        universeBalance = candidateContract;
    }

    function setUniversePlanetExplorationAddress(address _address) external onlyOwner {
        IUniversePlanetExploration candidateContract = IUniversePlanetExploration(_address);

        // NOTE: verify that a contract is what we expect
        // https://github.com/Lunyr/crowdsale-contracts/blob/cfadd15986c30521d8ba7d5b6f57b4fefcc7ac38/contracts/LunyrToken.sol#L117
        require(candidateContract.isUniversePlanetExploration(), "Incorrect address param");

        // Set the new contract address
        universePlanetExploration = candidateContract;
    }

    function getPlanet(uint256 _id)
    external
    view
    returns (
        uint256 rarity,
        uint256 discovered,
        uint256 sectorX,
        uint256 sectorY,
        uint256[MAX_ID_LIST_LENGTH] resourcesId,
        uint256[MAX_ID_LIST_LENGTH] resourcesVelocity
    ) {
        Planet memory pl = _getPlanet(_id);

        rarity = pl.rarity;
        discovered = pl.discovered;
        sectorX = pl.sectorX;
        sectorY = pl.sectorY;
        resourcesId = pl.resourcesId;
        resourcesVelocity = pl.resourcesVelocity;
    }

    function _getOwnedTokensCount(address _owner) internal view returns (uint256){
        return ownedTokens[_owner].length;
    }

    function _getOwnedTokensByIndex(address _owner, uint256 _ownerTokenIndex) internal view returns (uint256){
        return ownedTokens[_owner][_ownerTokenIndex];
    }

    function findAvailableResource(address _owner, uint _rarity) external returns (int8) {
        uint ownedPlanetsCount = _getOwnedTokensCount(_owner);

        uint[] memory resourceList = new uint[](ownedPlanetsCount * MAX_ID_LIST_LENGTH);

        uint[2] memory resourcesOrderByRarity = universeBalance.getResourcesQuantityByRarity(_rarity);
        uint firstResourceId = resourcesOrderByRarity[0];
        uint lastResourceId = resourcesOrderByRarity[0] + resourcesOrderByRarity[1] - 1;

        uint maxResourceListElement = 0;
        for (uint i = 0; i < ownedPlanetsCount; i++) {
            Planet memory planet = _getPlanet( _getOwnedTokensByIndex(_owner, i) );

            for (uint k = 1; k < planet.resourcesId.length; k++) {
                uint resourceId = planet.resourcesId[k];
                if(resourceId == 0) break;

                if(resourceId >= firstResourceId && resourceId <= lastResourceId) {
                    resourceList[maxResourceListElement] = resourceId; // замена resourceList.push(j);
                    maxResourceListElement++;
                }
            }
        }

        if (maxResourceListElement > 0) { // выбираем из них один случайный
            return int8(resourceList[_randRange(0, maxResourceListElement - 1)]);
        } else {
            return -1;
        }
    }

    function createPlanet(
        address _owner,
        uint256 _rarity,
        uint256 _sectorX,
        uint256 _sectorY,
        uint256 _startPopulation
    )
    external
    onlyWhitelisted()
    returns (uint256)
    {
        Planet memory planet = _createPlanetWithRandomResources(_rarity, _sectorX, _sectorY, _startPopulation);
        return _savePlanet(_owner, planet);
    }

    function _savePlanet(
        address _owner,
        Planet _planet
    )
    internal
    returns (uint)
    {
        uint256 planet = _convertPlanetToPlanetHash(_planet);
        uint256 planetState = _convertPlanetToPlanetStateHash(_planet);

        uint256 newPlanetId = planets.push(planet) - 1;
        planetStates[newPlanetId] = planetState;

        require(newPlanetId < PLANETS_COUNT, "No more planets");

        emit PlanetCreated(
            _owner,
            newPlanetId,
            _planet.sectorX,
            _planet.sectorY,
            _planet.rarity,
            _planet.resourcesId,
            _planet.resourcesVelocity,
            _planet.resourcesUpdated[0]
        );

        discoveredPlanetsCountMap[_planet.sectorX][_planet.sectorY] += 1;

        if (_planet.rarity == 3) {
            uint groupIndex = universeBalance.getGroupId(_planet.sectorX, _planet.sectorY);
            planetCountByRarityInGroups[groupIndex][3] += 1;
        }

        if (_planet.rarity == 4) {
            planetCountByRarity[4] += 1;
        }

        _mint(_owner, newPlanetId);

        return newPlanetId;
    }

    function _createPlanetWithRandomResources(uint _rarity, uint _sectorX, uint _sectorY, uint _startPopulation)
    internal
    returns (Planet memory _planet)
    {
        uint[MAX_ID_LIST_LENGTH] memory resourcesId;
        uint[MAX_ID_LIST_LENGTH] memory resourcesVelocity;
        (resourcesId, resourcesVelocity) = universePlanetExploration.explorePlanet(_rarity);

        uint[MAX_ID_LIST_LENGTH] memory resourcesUpdated;
        resourcesUpdated[0] = _startPopulation;

        _planet = Planet({
            rarity: _rarity,
            discovered: uint256(now),
            updated: uint256(now),
            sectorX: _sectorX,
            sectorY: _sectorY,
            resourcesId: resourcesId,
            resourcesVelocity: resourcesVelocity,
            resourcesUpdated: resourcesUpdated
            });
    }
}
