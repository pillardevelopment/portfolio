pragma solidity 0.4.24;

contract IUniverseLaunchRenting {
    event RentPriceChanged(uint256 indexed planetId, uint256 price);

    function isUniverseLaunchRenting() public pure returns(bool);
    function setRentPrice(uint256 _planetId, uint256 _price) public;
    function getRentPrice(uint256 _planetId) public view returns(uint256);

}
