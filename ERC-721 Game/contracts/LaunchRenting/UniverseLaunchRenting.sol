pragma solidity 0.4.24;

import "zos-lib/contracts/migrations/Migratable.sol";

import "../Galaxy/IUniverseGalaxy.sol";
import "../LaunchRenting/IUniverseLaunchRenting.sol";
import "../Registry/IUniverseRegistry.sol";

contract UniverseLaunchRenting is IUniverseLaunchRenting, Migratable {
    event RentPriceChanged(uint256 indexed planetId, uint256 price);

    mapping (uint256 => uint256) planetLaunchingRentPrices;

    IUniverseRegistry registry_;

    function initialize(address _registryAddress) isInitializer('UniverseLaunchRenting','1.0.0') public {
        require(_registryAddress != address(0), "Registry contract address should be defined");
        registry_ = IUniverseRegistry(_registryAddress);
    }

    function isUniverseLaunchRenting() public pure returns(bool) {
        return true;
    }

    function setRentPrice(uint256 _planetId, uint256 _price) public {
        require(msg.sender == IUniverseGalaxy(registry_.getGalaxy()).ownerOf(_planetId));

        planetLaunchingRentPrices[_planetId] = _price;
        emit RentPriceChanged(_planetId, _price);
    }

    function getRentPrice(uint256 _planetId) public view returns(uint256) {
        return planetLaunchingRentPrices[_planetId];
    }

    uint256[50] private ______gap;
}
