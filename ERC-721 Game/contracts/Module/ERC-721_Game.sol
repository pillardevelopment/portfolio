pragma solidity ^0.4.24;

import "./ModuleAuction.sol";

contract ERC-721_Game is ModuleAuction {

    function init() public isInitializer("ERC-721_Game", "1.0.0") {
        ModuleStore.initialize();
    }

    function() public payable onlyWhitelisted() { }

    function withdrawBalance() external onlyTreasurer {
        uint256 balance = address(this).balance;
        treasurer.transfer(balance);
    }

    uint256[50] private ______gap;
}