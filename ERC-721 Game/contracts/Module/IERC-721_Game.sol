pragma solidity ^0.4.24;

import "openzeppelin-zos/contracts/token/ERC721/ERC721.sol";

contract IERC-721_Game is ERC721 {
    function mintModule(address _to, string _moduleName, uint256 _rarity, uint256 _kind) external returns(uint256);

    function getModule(uint256 _modId) public view returns (uint256 produced, uint256 rarity, uint256 kind);
}
