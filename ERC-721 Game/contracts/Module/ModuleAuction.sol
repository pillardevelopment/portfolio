pragma solidity 0.4.24;

import "../ModuleSaleAuction/IModuleSaleAuction.sol";
import "./ModuleStore.sol";

contract ModuleAuction is ModuleStore {

    IModuleSaleAuction public saleAuction;

    function setModuleSaleAuction(address _address) external onlyOwner {
        IModuleSaleAuction candidateContract = IModuleSaleAuction(_address);

        // NOTE: verify that a contract is what we expect - https://github.com/Lunyr/crowdsale-contracts/blob/cfadd15986c30521d8ba7d5b6f57b4fefcc7ac38/contracts/LunyrToken.sol#L117
        require(candidateContract.isModuleSaleAuction(), "Incorrect address param");

        // Set the new contract address
        saleAuction = candidateContract;
    }

    function createSaleAuction(
        uint256 _moduleId,
        uint256 _startingPrice,
        uint256 _endingPrice,
        uint256 _duration
    )
    external
    whenNotPaused
    {

        // Auction contract checks input sizes
        // If module is already on any auction, this will throw
        // because it will be owned by the auction contract.
        require(ownerOf(_moduleId) == msg.sender, "Not owner");

        approve(saleAuction, _moduleId);
        // Sale auction throws if inputs are invalid and clears
        // transfer approval after escrowing the module.
        saleAuction.createAuction(
            _moduleId,
            _startingPrice,
            _endingPrice,
            _duration,
            msg.sender
        );
    }

    function withdrawAuctionBalances() external onlyOwner {
        saleAuction.withdrawBalance();
    }
}