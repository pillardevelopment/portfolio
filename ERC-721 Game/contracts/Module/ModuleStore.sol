pragma solidity ^0.4.24;

import "../Tools/Random.sol";
import "./../Access/AccessControl.sol";
import "openzeppelin-zos/contracts/token/ERC721/ERC721Token.sol";
import "./IERC-721_Game.sol";
import "../Access/WhitelistMigratable.sol";

contract ModuleStore is ERC-721_Game, ERC721Token, WhitelistMigratable, AccessControl, Random {

    /*** EVENTS ***/
    event ModuleCreated(
        address owner,
        uint256 id,
        uint256 rarity,
        uint256 kind);


    /*** DATA TYPES ***/
    struct ERC-721_GameModule {
        uint256 produced;
        uint256 rarity;
        uint256 kind;
    }


    /*** STORAGE ***/

    //    struct ERC-721_GameModule {
    //        uint48 produced;
    //        uint8 rarity;
    //        uint8 kind;
    //    }

    uint256[] public modules;


    function initialize() public isInitializer("ModuleStore", "1.0.0") {
        Pausable.initialize(msg.sender);

        ERC721Token.initialize("ERC-721_Game", "721");

        paused = false;
        transferTreasurer(owner);
    }


    function getModule(uint256 _modId) public view returns (uint256 produced, uint256 rarity, uint256 kind) {
        ERC-721_Game memory bm = _getModule(_modId);

        produced = bm.produced;
        rarity = bm.rarity;
        kind = bm.kind;
    }


    function mintModule(address _to, string _moduleName, uint256 _rarity, uint256 _kind)
        external whenNotPaused onlyWhitelisted()
        returns(uint256)
        {
            require(_rarity <= 3, "Value cannot be greater than 3.");
            require(_kind <= 10, "Value cannot be greater than 10");

            ERC-721_Game memory _bm = _createModule(_rarity, _kind);
            uint256 module = _convertModulesToModulesHash(_bm);

            uint256 newModuleID = modules.push(module)-1;

            emit ModuleCreated(_to, newModuleID, _rarity, _kind);

            _mint(_to, newModuleID);

            _setTokenURI(newModuleID, _moduleName);

            return newModuleID;
        }


    function _getModule(uint256 _id) internal view returns(ERC-721_Game memory _bm)
    {
        uint256 module = modules[_id];

        _bm.produced = uint256(uint48(module));
        _bm.rarity = uint256(uint8(module>>48));
        _bm.kind = uint256(uint8(module>>56));

    }


    function _convertModulesToModulesHash(ERC-721_Game memory _ERC-721_Game)
    internal pure returns(uint256 _moduleHash)
    {
        _moduleHash = _ERC-721_Game.produced;
        _moduleHash |= _ERC-721_Game.rarity<<48;
        _moduleHash |= _ERC-721_Game.kind<<56;
    }


    function _createModule(uint256 _rarity, uint256 _kind)
        internal view returns (ERC-721_Game memory _module)
    {
        _module = ERC-721_Game({
            rarity: _rarity,
            produced: uint256(now),
            kind: _kind
            });
    }


    function () public payable {
        revert("Contract cannot be funded");
    }
}