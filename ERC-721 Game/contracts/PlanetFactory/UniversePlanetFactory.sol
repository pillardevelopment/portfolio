/*****************
**    1) bundle 0 - $25 -  4 Common and 2 Rare planets
**    2) bundle 1 -  $50 - 6 Common, 4 Rare and 1 Epic planet
**    3) bundle 2 - $100 - 12 Common, 8 Rare and 2 Epic planets
*****************/

pragma solidity ^0.4.24;

import "../Access/ExchangeRateControl.sol";
import "../Access/TreasurerMigratable.sol";
import "../Access/WhitelistMigratable.sol";
import "../Discovery/IUniverseDiscovery.sol";
import "../Registry/IUniverseRegistryV1.sol";
import "../Common/Random.sol";
import "../Settings/IUniverseBalance.sol";
import "../Common/UniverseGalaxyConstant.sol";

contract UniversePlanetFactory is UniverseGalaxyConstant, TreasurerMigratable, WhitelistMigratable, ExchangeRateControl, Random {

    /*** EVENTS ***/
    event NewPlanetBundleBought(address buyer, uint256 bundleType, uint256 bundlePrice);
    event PlanetBundleGifted( address recipient, uint256 bundleType);

    IUniverseDiscovery public discoveryContact;
    IUniverseRegistryV1 public registry;

    function initialize(address _universeRegistry,
                        address _promoter) public isInitializer("PlanetFactory", "1.0.0") {
        require(_universeRegistry != address(0), "address incorrect");
        require(_promoter != address(0), "address incorrect");

        registry = IUniverseRegistryV1(_universeRegistry);
        discoveryContact = IUniverseDiscovery(registry.getDiscovery());

        Ownable.initialize(msg.sender);
        transferTreasurer(_promoter);

        setPrices(0, 2500);
        setPrices(1, 5000);
        setPrices(2, 10000);
    }

    function getPlanetBundlePriceUSD(uint256 _bundleType) public view returns(uint256) {
        return _calculateEtherPrice(_bundleType);
    }

    function buyPlanetBundle(uint256 _bundleType) public payable {
        uint256 price = _calculateEtherPrice(_bundleType);
        require(_checkIfEnoughAndSendExcessBack(msg.value, price, msg.sender), "Insufficient amount");

        _issue(msg.sender, _bundleType);
        emit NewPlanetBundleBought(msg.sender, _bundleType, price);
    }

    function giftPlanetBundle(uint256 _bundleType, address _to) public onlyWhitelisted {
        _issue(_to, _bundleType);
        emit PlanetBundleGifted(_to, _bundleType);
    }

    function () public payable {
        revert();
    }

    function withdrawFactoryBalance() external onlyTreasurer {
        uint256 balance = address(this).balance;

        treasurer.transfer(balance);
    }

    function _checkIfEnoughAndSendExcessBack(uint256 _value, uint256 _price, address _bayer) internal returns (bool) {
        if(_value < _price) return false;

        if(_value > _price) {
            uint256 excess = _value.sub(_price);
            _bayer.transfer(excess);
        }
        return true;
    }

    function _issue(address _to, uint256 _bundleType) internal {
        IUniverseBalance balance = IUniverseBalance(registry.getBalance());

//      balance.getUIntArray3Value(/* planet_factory_common_planets_per_bundle */ 44;
//      balance.getUIntArray3Value(/* planet_factory_rare_planets_per_bundle */ 45);
//      balance.getUIntArray3Value(/* planet_factory_epic_planets_per_bundle */ 46);
        for(uint i = 0; i < 3; i++) {
            _produceBundle(
                _to,
                balance.getUIntArray3Value(/* planet_factory_RARITY_planets_per_bundle */ 44 + i)[_bundleType],
                i);
        }
    }

    function _produceBundle(address _to, uint256 _planetList, uint256 _rarity) internal {
        uint256[] memory _coordinateX = _generateSectors(_planetList, SECTOR_X_MAX - 1);
        uint256[] memory _coordinateY = _generateSectors(_planetList, SECTOR_Y_MAX - 1);

        for(uint i = 0; i < _planetList; i++) {
            discoveryContact.createPromoPlanet(_coordinateX[i], _coordinateY[i], _rarity, _to);
        }
    }

    function _generateSectors(uint256 _numbers, uint256 _end) internal returns(uint256[]) {
        uint256[] memory coordinates = new uint[](_numbers);

        for(uint i = 0; i < _numbers; i++) {
            coordinates[i] = _randRange(0, _end);
        }

        return coordinates;
    }

    uint256[50] private ______gap;
}
