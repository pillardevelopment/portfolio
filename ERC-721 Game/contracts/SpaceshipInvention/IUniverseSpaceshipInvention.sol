pragma solidity 0.4.24;

contract IUniverseSpaceshipInvention {
    function inventSpaceshipDraft() external;
    function inventSpaceshipDraftOptimized(bytes _signature) external;
}