pragma solidity 0.4.24;

import "openzeppelin-zos/contracts/lifecycle/Pausable.sol";

import "./../Common/Random.sol";
import "../Common/MathTools.sol";
import "../Registry/IUniverseRegistry.sol";
import "../SaleAuction/ISaleAuction.sol";
import "../Settings/IUniverseBalance.sol";
import "../Galaxy/IUniverseGalaxy.sol";
import "../SpaceshipStore/IUniverseSpaceshipStoreV1.sol";
import "../LaunchRenting/IUniverseLaunchRenting.sol";
import "../Common/SignatureVerifier.sol";
import "../Access/WhitelistMigratable.sol";

contract UniverseSpaceshipInventionV2 is UniverseDiscoveryConstant, Random, Pausable, MathTools, SignatureVerifier, WhitelistMigratable {
    uint256 internal constant MAX_FLAT_LIST_LENGTH = 100;

    event SpaceshipDraftInvented(
        address indexed owner,
        uint256 spaceshipDraftId,
        uint radarAmount,
        uint engineAmount,
        uint peopleAmount,
        uint rank,
        uint[MAX_ID_LIST_LENGTH] resourcesId,
        uint[MAX_ID_LIST_LENGTH] resourcesNeeded
    );

    IUniverseRegistry public registry_;
    ISaleAuction public saleAuction_;
    IUniverseBalance public balance_;
    IUniverseGalaxy public galaxy_;
    IUniverseSpaceshipStoreV1 public spaceshipStore_;

    function initialize(address _registryAddress) public isInitializer("UniverseSpaceshipInvention", "1.0.0") {
        Pausable.initialize(msg.sender);

        require(_registryAddress != address(0), "Registry contract address should be defined");
        registry_ = IUniverseRegistry(_registryAddress);

        paused = true;
    }

    function unpause() public onlyOwner whenPaused {
        require(galaxy_ != address(0), "Planet contract should be defined");
        require(saleAuction_ != address(0), "SaleAuction contract should be defined");
        require(balance_ != address(0), "Balance contract should be defined");
        require(spaceshipStore_ != address(0), "UniverseSpaceshipStore contract should be defined");

        // Actually unpause the contract.
        super.unpause();
    }

    function cacheRegistry() onlyOwner whenPaused external {
        saleAuction_ = ISaleAuction(registry_.getSaleAuction());
        balance_ = IUniverseBalance(registry_.getBalance());
        galaxy_ = IUniverseGalaxy(registry_.getGalaxy());
        spaceshipStore_ = IUniverseSpaceshipStoreV1(registry_.getSpaceshipStore());
    }

    function createEarthSpaceship(address _earthOwner) external onlyOwner {
        require(spaceshipStore_.getDraftsCountByUser(_earthOwner) == 0, "Earth spaceship draft was created");

        uint[3] memory shipResourcesKeys = balance_.getUIntArray3Value(/* "earth_shipParams_resources_m_keys" */ 28);
        uint[3] memory shipResourcesValues = balance_.getUIntArray3Value(/* "earth_shipParams_resources_m_values" */ 29);

        _saveSpaceshipDraft(
            _earthOwner,
            balance_.getUIntValue(/* "earth_shipParams_radar" */ 26),
            balance_.getUIntValue(/* "earth_shipParams_engine" */ 27),
            shipResourcesValues[0],
            balance_.getUIntValue(/* "earth_shipParams_rank" */ 25),
            [shipResourcesKeys[1], shipResourcesKeys[2], 0, 0, 0],
            [shipResourcesValues[1], shipResourcesValues[2], 0, 0, 0]
        );
    }

    function inventSpaceshipDraft() external whenNotPaused {
        address inventor = tx.origin; // launch as user

        //TODO: make whitelisted the onlyOtherGameContracts method in the SaleAuction
        //uint planetLimitation = balance_.getUIntValue(/* "settings_planet_limitation" */ 39);
        //if (balance_.autoClearAuction()) saleAuction_.clearAll(inventor, planetLimitation);

        galaxy_.spendKnowledge(inventor, _knowledgeForNextDiscovery(inventor));

        _makeSpaceshipDraft(inventor);
    }

    function inventSpaceshipDraftOptimized(bytes _signature) external whenNotPaused {
        address inventor = tx.origin; // launch as user

        uint discoveries = spaceshipStore_.getDraftsCountByUser(inventor);

        //TODO: make whitelisted the onlyOtherGameContracts method in the SaleAuction
        //uint planetLimitation = balance_.getUIntValue(/* "settings_planet_limitation" */ 39);
        //if (balance_.autoClearAuction()) saleAuction_.clearAll(inventor, planetLimitation);

        // This recreates the message that was signed on the client.
        bytes32 message = prefixed(keccak256(inventor, discoveries + 1, address(this)));

        require(whitelist[recoverSigner(message, _signature)], "The signature is an incorrect");

        galaxy_.spendKnowledge(inventor, 0);

        _makeSpaceshipDraft(inventor);
    }

    function _makeSpaceshipDraft(address inventor) internal {
        uint newRank = _calculateNewSpaceshipRank(inventor);

        uint radarAmount;
        uint engineAmount;
        uint peopleAmount;

        (radarAmount, engineAmount, peopleAmount) = _calculateNewSpaceshipModules(newRank);

        uint[MAX_ID_LIST_LENGTH] memory resourcesId;
        uint[MAX_ID_LIST_LENGTH] memory resourcesAmount;

        (resourcesId, resourcesAmount) = _toIndexedList( _calculateResourcesForSpaceshipDraft(inventor, newRank) );

        _saveSpaceshipDraft(
            inventor,
            radarAmount,
            engineAmount,
            peopleAmount,
            newRank,
            resourcesId,
            resourcesAmount);
    }

    function _saveSpaceshipDraft(
        address _owner,
        uint _radarAmount,
        uint _engineAmount,
        uint _peopleAmount,
        uint _rank,
        uint[5] _resourcesId,
        uint[5] _resourcesNeeded
    )
    internal
    returns (uint)
    {
        uint256 newSpaceshipDraftId = spaceshipStore_.createDraft(
            _owner,
            _radarAmount,
            _engineAmount,
            _peopleAmount,
            _rank,
            _resourcesId,
            _resourcesNeeded
        );

        emit SpaceshipDraftInvented(
            _owner,
            newSpaceshipDraftId,
            _radarAmount,
            _engineAmount,
            _peopleAmount,
            _rank,
            _resourcesId,
            _resourcesNeeded
        );

        return newSpaceshipDraftId;
    }

    function _calculateNewSpaceshipRank(address _owner) internal returns (uint) {
        uint spaceshipRanksCount = balance_.getUIntValue(/* settings_ranks_count */ 40);

        // считаем сколько черетежей кораблей каждого ранга у пользователя
        uint[MAX_RANKS_COUNT] memory ownerRanksArray = spaceshipStore_.getDraftsCountByRanks(_owner);

        uint chance;
        uint newRank = 0;
        uint maxChanceToGetNewRankX100 = balance_.getUIntValue(/* "spaceshipInvention_maxChanceToGetNewRankX100" */ 3);
        uint countPerRank = balance_.getUIntValue(/* "spaceshipInvention_countPerRank" */ 7);
        for (newRank = 0 ; newRank < spaceshipRanksCount; newRank++) {
            // Определяем ранг. Чем больше уже кораблей открыто, тем больше вероятность получить более высокий ранг

            chance = ownerRanksArray[newRank] * 100 / countPerRank;
            if (chance > maxChanceToGetNewRankX100) {
                chance = maxChanceToGetNewRankX100;
            }

            if (chance < _randRange(0, 100)) {
                break; // ранг следующего чертежа корабля равен newRank
            }
        }

        if (newRank >= spaceshipRanksCount) {
            newRank = spaceshipRanksCount - 1;
        }

        return newRank;
    }

    function _calculateNewSpaceshipModules(uint _newRank) internal returns (uint radarAmount, uint engineAmount, uint peopleAmount) {
        // radarAmount 0, engineAmount 1, peopleAmount 2
        uint[3] memory newRankParams = balance_.getRankParamsValue(_newRank);

        uint[3] memory balance_modulesRange = balance_.getUIntArray3Value(/* "spaceshipInvention_modulesRangeX100" */ 8);

        uint peopleModuleLevel = _randRange(0, balance_modulesRange.length - 1);
        peopleAmount = _divisionWithRound(newRankParams[2] * balance_modulesRange[peopleModuleLevel], 100); // Определяем размер жилого модуля

        uint radarModuleLevel = _randRange(0, balance_modulesRange.length - 1);
        radarAmount = _divisionWithRound(newRankParams[0] * balance_modulesRange[radarModuleLevel], 100); // Определяем мощность радара

        uint engineModuleLevel = _randRange(0, balance_modulesRange.length - 1);
        engineAmount = _divisionWithRound(newRankParams[1] * balance_modulesRange[engineModuleLevel], 100); // Определяем мощность двигателей
    }

    //event DEBUG(string name, int8 value);
    function _calculateResourcesForSpaceshipDraft(address _owner, uint _newRank)
    internal
    returns (uint[MAX_FLAT_LIST_LENGTH] resourcesAmount)
    {
        uint[4] memory newRankResourcesCountByRarity = balance_.getRankResourcesCountByRarity(_newRank);

        uint random;
        uint[20] memory randElements;
        int firstResource = -2;
        for (uint j = 0; j <= 3; j++) {//для шаблона корабля нужно максимум 4 редкости ресурсов
            if (newRankResourcesCountByRarity[j] == 0) continue; // newRankResourcesCountByRarity[j] - количество видов ресурсов заданной ценности

            uint[2] memory resourcesOrderByRarity = balance_.getResourcesQuantityByRarity(j);
            for (uint k = 0; k < resourcesOrderByRarity[1]; k++) { // инициализируем изменяемый массив с ресурсами нужной редкости
                randElements[k] = resourcesOrderByRarity[0] + k;
            }

            for (uint i = 0; i < newRankResourcesCountByRarity[j]; i++) { // набираем указанное количество ресурсов нужной редкости
                // условие срабатывает только один раз на последней итерации первой существующей редкости, после этого firstResource меняется
                if ((i == (newRankResourcesCountByRarity[j] - 1)) && firstResource == -2) {
                    firstResource = galaxy_.findAvailableResource(_owner, j);
                    //emit DEBUG('firstResource', int8(firstResource));
                    if (firstResource > 0) {
                        if (resourcesAmount[uint(firstResource)] == 0) {
                            resourcesAmount[uint(firstResource)] = _calcResourcesAmount(_newRank);
                            continue;
                        }
                    }
                }

                // выбираем случайное число от 0 до последнего числа в списке:
                random = _randRange(0, (resourcesOrderByRarity[1] - 1) - i); // последнее число на каждой итерации всё меньше и меньше

                resourcesAmount[randElements[random]] = _calcResourcesAmount(_newRank);

                randElements[random] = randElements[resourcesOrderByRarity[1] - 1 - i]; // последний элемент переносим на место извлечённого
            }
        }
    }

    function _calcResourcesAmount(uint _newRank) internal returns (uint) {
        uint resBase = balance_.getUIntValue(/* "spaceshipInvention_resBase" */ 4);
        uint resModifierX100 = balance_.getUIntValue(/* "spaceshipInvention_resModifierX100" */ 5);
        uint resRandomizerX100 = balance_.getUIntValue(/* "spaceshipInvention_resRandomizerX100" */ 6);

        return (resModifierX100 ** _newRank) * resBase * (100 + _randRange(0, 2 * resRandomizerX100) - resRandomizerX100) / 100 / (100 ** _newRank);
    }

    function _knowledgeForNextDiscovery(address _owner) internal view returns (uint) {
        uint discoveries = spaceshipStore_.getDraftsCountByUser(_owner);

        // умножаем на 1000, чтобы избавиться от грубых потерь на округлении
        uint knowledge = balance_.getUIntValue(/* "spaceshipInvention_knowledgeForInventionBase" */ 1) * 1000;
        uint knowledgeForInventionModifierX100 = balance_.getUIntValue(/* "spaceshipInvention_knowledgeForInventionModifierX100" */ 2);
        for (uint i = 0; i < discoveries; i++) {
            knowledge = _divisionWithRound(knowledge * knowledgeForInventionModifierX100, 100);
        }

        return _divisionWithRound(knowledge, 1000);
    }

    function _toIndexedList(uint[MAX_FLAT_LIST_LENGTH] _list) internal pure returns (uint[MAX_ID_LIST_LENGTH] _resourcesId, uint[MAX_ID_LIST_LENGTH] _resourcesValue){
        uint position = 0;
        for (uint i = 0; i < _list.length; i++) {
            uint value = _list[i];
            if(value == 0) {
                continue;
            }
            _resourcesId[position] = i;
            _resourcesValue[position] = value;
            position += 1;
        }
    }

    uint256[50] private ______gap;
}
