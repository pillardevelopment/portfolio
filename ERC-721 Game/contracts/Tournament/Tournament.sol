pragma solidity ^0.4.24;

import "../Access/ExchangeRateControl.sol";
import "../Access/TreasurerMigratable.sol";
import "../Access/WhitelistMigratable.sol";

contract Tournament is TreasurerMigratable,WhitelistMigratable, ExchangeRateControl {

    /*** EVENTS ***/

    event NewTicketBought(address buyer, uint16 tournamentType, uint256 ticketPrice);

    mapping (address => mapping (uint256 => uint256)) public ticketStorage;

    function initialize() public isInitializer("0xWarriors Tournament", "1.0.0") {
        Ownable.initialize(msg.sender);
        transferTreasurer(owner);
        setPrices(0, 500);
    }

    function buyTournamentTicket(uint16 _tournamentType) public payable {
        uint256 price = _calculateEtherPrice(_tournamentType);
        require(_checkIfEnoughAndSendExcessBack(msg.value, price, msg.sender));
        ticketStorage[msg.sender][_tournamentType]+=1;

        emit NewTicketBought(msg.sender, _tournamentType, price);
    }

    function getTournamentTicketPrice(uint16 _tournamentType)public view returns(uint256) {
        return usdPrices[_tournamentType];
    }

    function getTicketsCount(address _owner, uint16 _tournamentType) public view returns(uint256) {
        return ticketStorage[_owner][_tournamentType];
    }

    function withdrawBalance() external onlyTreasurer {
        uint256 balance = address(this).balance;

        treasurer.transfer(balance);
    }

    function _checkIfEnoughAndSendExcessBack(uint256 _value, uint256 _price, address _bayer) internal returns (bool) {
        if(_value < _price) return false;

        if(_value > _price) {
            uint256 excess = _value.sub(_price);
            _bayer.transfer(excess);
        }
        return true;
    }

    function () public payable {
        revert();
    }
uint256[50] private ______gap;
}