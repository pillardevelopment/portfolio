pragma solidity ^0.4.24;

import "../Tools/Random.sol";
import "../Warrior/IWarrior.sol";
import "../GameContract/IOxGamesToken.sol";
import "../Access/ExchangeRateControl.sol";
import "../Access/TreasurerMigratable.sol";
import "../Access/WhitelistMigratable.sol";

contract WarriorFactory is TreasurerMigratable, Random, WhitelistMigratable , ExchangeRateControl {

    /*** EVENTS ***/
    event NewWarriorBought(address buyer, uint256 warriorPrice);
    event WarriorGifted( address recipient);

    IWarrior public warrior_;
    IOxGamesToken public gameToken_;

    function initialize(address _warriorAddress) public isInitializer("Warrior Factory", "1.0.0") {
        require(_warriorAddress != address(0), "address incorrect");
        Ownable.initialize(msg.sender);

        transferTreasurer(owner);
        setWarriorContract(_warriorAddress);
        setPrices(0, 50000);
    }

    function setWarriorContract(address _warriorAddress) public onlyOwner {
        warrior_ = IWarrior(_warriorAddress);
    }

    function setToken(address _tokenAddress) public onlyOwner {
        gameToken_ = IOxGamesToken(_tokenAddress);
    }

    function buyChestWithWarrior() public payable {
        uint256 _price = _calculateEtherPrice(0);
        require(_checkIfEnoughAndSendExcessBack(msg.value, _price, msg.sender), "Insufficient amount");

        _issue(msg.sender);
        emit NewWarriorBought(msg.sender, _price);
    }

    function buyChestWithWarriorAtTokens() public {

        uint256 multipliedHundredPercents = (discountStore_.getPercentMultiplier()).mul(100);
        uint256 multipliedHundredPercentsWithoutDiscount = (multipliedHundredPercents).sub(discountStore_.getDiscount());
        uint256 _price = (_calculateTokenPrice(0)).mul(multipliedHundredPercentsWithoutDiscount).div(multipliedHundredPercents);

        require(gameToken_.balanceOf(msg.sender) >= _price, "Insufficient amount");
        gameToken_.approveWhitelisted(msg.sender, this, _price);
        gameToken_.transferFrom(msg.sender, this, _price);

        _issue(msg.sender);
        emit NewWarriorBought(msg.sender, _price);
    }

    function giftWarrior(address _to) public onlyWhitelisted {
        require(_to != address(0), "incorrect address, try again");
        _issue(_to);
        emit WarriorGifted(_to);
    }

    function getWarriorPricesForEther(uint16 _boxType) public view returns(uint256) {
        return _calculateEtherPrice(_boxType);
    }

    function getWarriorPricesForTokens(uint16 _boxType) public view returns(uint256) {
        return _calculateTokenPrice(_boxType);
    }

    function () public payable {
        revert();
    }

    function withdrawFactoryBalance() external onlyTreasurer {
        uint256 balance = address(this).balance;

        treasurer.transfer(balance);
    }

    function withdrawFactoryBalanceFromToken() external onlyTreasurer {
        uint256 balance =  gameToken_.balanceOf(this);

        gameToken_.transfer(treasurer, balance);
    }

    function _checkIfEnoughAndSendExcessBack(uint256 _value, uint256 _price, address _bayer) internal returns (bool) {
        if(_value < _price) return false;

        if(_value > _price) {
            uint256 excess = _value.sub(_price);
            _bayer.transfer(excess);
        }
        return true;
    }

    function _issue(address _to) internal view {
        warrior_.mintWarrior(_to, _generateType());
    }

    function _generateType() internal view returns (uint256) {
        return uint256(_randRange(0, 3));
    }

    uint256[50] private ______gap;
}
