const Module = artifacts.require("./Module/ERC-721_Game.sol");
const ModuleProxy = artifacts.require("./Module/ERC-721_GameProxy.sol");

require('dotenv').config();
const delay = require('delay');

const paused = parseInt( process.env.DELAY_MS || "60000" );

const wait = async (param) => { console.log("Delay " + paused); await delay(paused); return param;};
const logReceipt = (receipt, name) => console.log(name + " :: success :: " + receipt.tx);

module.exports = function(deployer, network, accounts) {
  deployer.then(async () => {
    await wait();

    const CONTRACT_ADMIN_ACCOUNT = accounts[0];
    console.log('CONTRACT_ADMIN_ACCOUNT :: ',CONTRACT_ADMIN_ACCOUNT);
    const PROXY_ADMIN_ACCOUNT = accounts[1];
    console.log('PROXY_ADMIN_ACCOUNT :: ',PROXY_ADMIN_ACCOUNT);

    await wait(await deployer.deploy(Module));
    await wait(await deployer.deploy(ModuleProxy, Module.address, {from: PROXY_ADMIN_ACCOUNT}));

    let module = await Module.at(ModuleProxy.address);
    await wait(logReceipt(await module.init( {from: CONTRACT_ADMIN_ACCOUNT} ),
      'module.initialize'));
  });
};