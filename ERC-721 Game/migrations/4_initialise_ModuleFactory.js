const Module = artifacts.require("./Module/ERC-721_Game.sol");
const ModuleProxy = artifacts.require("./Module/ERC-721_GameProxy.sol");
const ModuleFactory = artifacts.require("./Factory/ModuleFactory.sol");
const ModuleFactoryProxy = artifacts.require("./Factory/ModuleFactoryProxy.sol");

require('dotenv').config();
const delay = require('delay');

const paused = parseInt( process.env.DELAY_MS || "60000" );

const wait = async (param) => { console.log("Delay " + paused); await delay(paused); return param;};
const logReceipt = (receipt, name) => console.log(name + " :: success :: " + receipt.tx);

module.exports = function(deployer) {
  deployer.then(async () => {
    await wait();

    const module = await Module.at(ModuleProxy.address);
    const moduleFactory = await ModuleFactory.at(ModuleFactoryProxy.address);

    await wait(logReceipt(await moduleFactory.setModuleContract(module.address),
      "moduleFactory setModuleContract " + module.address));

    await wait(logReceipt(await module.addAddressToWhitelist(moduleFactory.address),
      "module addAddressToWhitelist " + moduleFactory.address));
  });
};
