const Module = artifacts.require("./Module/ERC-721_Game.sol");
const ModuleProxy = artifacts.require("./Module/ERC-721_GameProxy.sol");
const ModuleSaleAuction = artifacts.require("./ModuleSaleAuction/ModuleSaleAuction.sol");
const ModuleSaleAuctionProxy = artifacts.require("./ModuleSaleAuction/ModuleSaleAuctionProxy.sol");

require('dotenv').config();
const delay = require('delay');

const paused = parseInt( process.env.DELAY_MS || "60000" );

const wait = async (param) => { console.log("Delay " + paused); await delay(paused); return param;};
const logReceipt = (receipt, name) => console.log(name + " :: success :: " + receipt.tx);

module.exports = function(deployer) {
  deployer.then(async () => {
    await wait();

    const module = await Module.at(ModuleProxy.address);
    const moduleSaleAuction = await ModuleSaleAuction.at(ModuleSaleAuctionProxy.address);

    await wait(logReceipt(await module.setModuleSaleAuction(moduleSaleAuction.address),
      "module setModuleSaleAuction " + moduleSaleAuction.address));

    await wait(logReceipt(await moduleSaleAuction.unpause(),
      "moduleSaleAuction unpause"));
  });
};