const ArconaMarketplaceContract = artifacts.require("./Archive/ArconaMarketplaceContract.sol");
const ArconaToken = artifacts.require("./Archive/SimpleTokenForTest.sol");

require('dotenv').config();
const delay = require('delay');

const paused = parseInt( process.env.DELAY_MS || "5" );

const testProfitAddress = process.env.BENEFIT_ADDRESS; // TODO only MainNet

const wait = async (param) => {console.log("Delay " + paused); await delay(paused); return param;};

module.exports = function(deployer) {
    deployer.then(async () => {
        await wait();

        await wait(await deployer.deploy(ArconaMarketplaceContract, ArconaToken.address, testProfitAddress));
    });
};