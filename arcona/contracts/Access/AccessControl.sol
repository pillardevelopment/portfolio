pragma solidity ^0.4.23;

import "openzeppelin-zos/contracts/lifecycle/Pausable.sol";
import "./TreasurerMigratable.sol";


contract AccessControl is Pausable, TreasurerMigratable {

    event AddAdmin(address indexed admin);
    event DelAdmin(address indexed admin);

    mapping(address => bool) admins;

    modifier onlyAdmin() {
        require(isAdmin(msg.sender));
        _;
    }

    function addAdmin(address _adminAddress) external onlyOwner {
        require(_adminAddress != address(0));
        admins[_adminAddress] = true;
        emit AddAdmin(_adminAddress);
    }

    function delAdmin(address _adminAddress) external onlyOwner {
        require(admins[_adminAddress]);
        admins[_adminAddress] = false;
        emit DelAdmin(_adminAddress);
    }

    function isAdmin(address _adminAddress) public view returns (bool) {
        return admins[_adminAddress];
    }
}