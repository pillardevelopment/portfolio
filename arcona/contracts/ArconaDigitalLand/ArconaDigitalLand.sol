pragma solidity ^0.4.24;

import "./ArconaDigitalLandStore.sol";

contract ArconaDigitalLand is ArconaDigitalLandStore {

    function init() public isInitializer("Arcona Digital Land", "1.0.0") {
        ArconaDigitalLandStore.initialize();
    }

    function() public payable onlyWhitelisted() { }

    /**
   * @dev Allows to withdraw funds from the contract.
   * Usage of this method only treasurer
   */
    function withdrawBalance() external onlyTreasurer {
        uint256 balance = address(this).balance;
        treasurer.transfer(balance);
    }

    uint256[50] private ______gap;
}