pragma solidity ^0.4.24;

import "../Tools/Convertation.sol";
import "./../Access/AccessControl.sol";
import "openzeppelin-zos/contracts/token/ERC721/ERC721Token.sol";
import "./IArconaDigitalLand.sol";
import "../Access/WhitelistMigratable.sol";

contract ArconaDigitalLandStore is IArconaDigitalLand, ERC721Token, WhitelistMigratable, AccessControl, Convertation {

    /*** EVENTS ***/
    event ArconaLandCreated(address owner, uint256 id);
    event ArconaLandModified(address owner, uint256 param, uint256 newvalue);

    /*** DATA TYPES ***/
    struct Land {
        uint256 gisId;
        uint256 manorMember;
        uint256 blocked;
    }

    /*** STORAGE ***/
    //    struct Land {
    //        uint192 gisId;
    //        uint8 manorMember;
    //        uint8 blocked;
    //    }

    uint256[] public landStorage;

    string public rootUrl_;

    function initialize() public isInitializer("ArconaLandStore", "1.0.0") {
        Pausable.initialize(msg.sender);

        ERC721Token.initialize("Arcona Digital Land", "ARDL");

        paused = false;
        transferTreasurer(owner);
        rootUrl_ = "https://marketplace.arcona.io/earth/";
    }

        /**
      * @dev Allows to get the parameters of land token by identifier.
      * @param _landId The land's unique identifier.
      */
    function getLand(uint256 _landId) public view returns ( uint256 _gisId,
                                                            uint256 _manorMember,
                                                            bool _blocked) {
        Land memory _landStorage = _getLand(_landId);
        _gisId = _landStorage.gisId;
        _manorMember = _landStorage.manorMember;
        if (_landStorage.blocked == 0) {
            _blocked = false;
        }
        if (_landStorage.blocked == 1) {
            _blocked = true;
        }
    }

    /**
    * @dev Allows to create a new land token a whitelist member.
    * Usage of this method only WhiteList's member
    * @param _to The address to transfer to.
    * @param _gisId The unique GIS identifier.
    * @param _manorMember The number of the Manor.
    */
    function mintLand ( address _to,
                        uint256 _gisId,
                        uint256 _manorMember) external whenNotPaused onlyWhitelisted() returns(uint256) {

        Land memory _landStorage = _createLand(_gisId, _manorMember);
        uint256 land = _convertLandToHash(_landStorage);
        uint256 newID = landStorage.push(land)-1;
        emit ArconaLandCreated(_to, newID);
        _mint(_to, newID);

        return newID;
    }

    /**
    * @dev Allows to modify the state of land token.
    * Usage of this method only WhiteList's member
    * Requires the _landId is exist
    * @param _landId The land's unique identifier.
    * @param _paramCode Special code to change a specific parameter.
    * @param _newValue New value of parameter.
    */
    function modifyLand(uint256 _landId, uint256 _paramCode, uint256 _newValue) external whenNotPaused onlyWhitelisted( ) {
        require(exists(_landId));

        _modifyParams(_landId, _paramCode, _newValue);

        emit ArconaLandModified(msg.sender, _paramCode, _newValue);
    }

    /**
    * @dev Allows to modify root URL's directory.
    * Usage of this method only owner
    * @param _newRootUrl New root directory.
    */
    function updateRootURL(string _newRootUrl) public onlyOwner {
        rootUrl_ = _newRootUrl;
    }

    /**
    * @dev Allows to get the full URL path to land token by identifier.
    * Requires the _landId is exist
    * @param _landId The land's unique identifier.
    */
    function tokenURI(uint256 _landId) public view returns (string) {
        require(exists(_landId));
        return strConcat(rootUrl_, uint2str(_landId));
    }


    function transferFrom(address _sender, address _spender, uint256 _landId)  public {
        Land memory _landStorage = _getLand(_landId);
        require(_landStorage.blocked == 0, "This token is blocking");
        require(_landStorage.manorMember == 0, "This token is blocking");
        super.transferFrom(_sender, _spender,  _landId);
    }


    function approveOurContracts(uint256 _tokenId) external onlyWhitelisted() {
        tokenApprovals[_tokenId] = msg.sender;
    }


    /**
    * @dev A simple non fungible token standard that allows batching tokens
    * into lots and settling p2p atomic transfers in one transaction.
    * @param _to The land's unique identifiers.
    * @param tokenIndices The land's unique identifiers.
    */
    function transferMany(address _to, uint256[] tokenIndices) public  {
        for(uint i = 0; i < tokenIndices.length; i++) {
            require(_to != address(0));

            uint256 index = uint(tokenIndices[i]);

           // canTransfer(tokenIndices[i]);
            clearApproval(msg.sender, tokenIndices[i]);
            removeTokenFrom(msg.sender, tokenIndices[i]);
            addTokenTo(_to, tokenIndices[i]);

            emit Transfer(msg.sender, _to, tokenIndices[i]);
        }
    }



    function () public payable {
        revert("Contract cannot be funded");
    }


    function _getLand (uint256 _landId) internal view returns(Land memory _landStorage) {

        uint256 land = landStorage[_landId];

        _landStorage.gisId = uint256(uint192(land));
        _landStorage.manorMember = uint256(uint8(land>>192));
        _landStorage.blocked = uint256(uint8(land>>200));
    }

    function _convertLandToHash(Land memory _land) internal pure returns(uint256 _landHash) {

        _landHash = _land.gisId;
        _landHash |= _land.manorMember<<192;
        _landHash |= _land.blocked<<200;
    }

    function _createLand(uint256 _gisId, uint256 _manorMember)
        internal pure returns (Land memory _land) {

        _land = Land({
            gisId: _gisId,
            manorMember: _manorMember,
            blocked: 0
            });
    }

    function _modifyParams (uint256 _landId, uint256 _paramCode, uint256 _newValue) internal  {
        require(_paramCode < 3 && _paramCode != 0);
        Land memory _land = _getLand(_landId);
        if (_paramCode == 1) {
            _land.manorMember = _newValue;
        }
        if (_paramCode == 2) {
            require(_newValue < 2);
            _land.blocked = _newValue;
        }
        landStorage[_landId] =  _convertLandToHash(_land);
    }
}