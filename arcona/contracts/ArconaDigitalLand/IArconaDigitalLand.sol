pragma solidity ^0.4.24;

import "openzeppelin-zos/contracts/token/ERC721/ERC721.sol";

contract IArconaDigitalLand is ERC721 {
    function mintLand(address _to, uint256 _gisId, uint256 _manorMember) external returns(uint256);
    function getLand(uint256 _landId) public view returns (uint256 _gisId, uint256 _manorMember, bool _blocked);
    function modifyLand(uint256 _tokenId, uint256 _paramCode, uint256 _newValue) external;
    function approveOurContracts(uint256 _tokenId) external;
    function transferMany(address _to, uint256[] tokenIndices) public;
}