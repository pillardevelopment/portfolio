pragma solidity ^0.4.24;

import "../Access/TreasurerMigratable.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-zos/contracts/token/ERC721/ERC721.sol";
import "../ArconaDigitalLand/IArconaDigitalLand.sol";

contract ArconaDigitalLandRent is TreasurerMigratable {

	struct LeaseContract{
		address tenant;
		uint256 price;
		uint256 duration;
//		bool vacant;
		bool active;
	}

	mapping (uint256 => mapping(address => LeaseContract)) internal rentStorage;

	ERC20 public token_;

	IArconaDigitalLand public landToken_;

	function initialize(address _landAddress, address _tokenAddress) public isInitializer("ArconaDigitalLandRent", "1.0.0") {
		require(_landAddress != address(0), "address incorrect");
		require(_tokenAddress != address(0), "address incorrect");

		Ownable.initialize(msg.sender);
		transferTreasurer(owner);

		landToken_ = IArconaDigitalLand(landToken_);
		token_ = ERC20(token_);
	}

	function setLandContract(address _landAddress) public onlyOwner {
		landToken_ = IArconaDigitalLand(_landAddress);
	}

	function setTokenContract(address _tokenAddress) public onlyOwner {
		token_ = ERC20(_tokenAddress);
	}

	/**
    * @dev Create slot for rent
    * @param _landLord address
    * @param _landId uint256
    * @param _monthPrice uint256
    * @param _duration uint256
    */
	function createRent(address _landLord, uint256 _landId, uint256 _monthPrice, uint256 _duration) public onlyOwner {
		require(landToken_.exists(_landId));
		require(msg.sender == landToken_.ownerOf(_landId) || msg.sender == rentStorage[_landId][_landLord].tenant);

		require(_landLord != address(0), " cannot be created for this address");
		require(_monthPrice > 0 && _duration > 0);

		if (msg.sender == rentStorage[_landId][_landLord].tenant) {
			require(rentStorage[_landId][landToken_.ownerOf(_landId)].duration >= now+_duration);
		}
		
		rentStorage[_landId][_landLord] = _createRent(_duration, _monthPrice, _landLord);
	}

	// pre -approve to token owner
	function bidRent(uint256 _landId, address _landLord) public {
		require(rentStorage[_landId][_landLord].active);
		require(rentStorage[_landId][_landLord].tenant == address(0));
		token_.transferFrom(msg.sender, _landLord, rentStorage[_landId][_landLord].price);
		rentStorage[_landId][_landLord].tenant = msg.sender;

		rentStorage[_landId][_landLord].duration += now;
	}

	function finalizeRent(uint256 _landId, address _landLord) public {
		require(msg.sender == landToken_.ownerOf(_landId) || msg.sender == rentStorage[_landId][_landLord].tenant);

		require(rentStorage[_landId][_landLord].duration < now);
		rentStorage[_landId][_landLord].active = false;
	}

	function getRent(address _landLord, uint256 _landId) public view returns(   address _tenant,
																				uint256 _price,
																				uint256 _duration,
																				bool _active) {
		_tenant = rentStorage[_landId][_landLord].tenant;
		_price = rentStorage[_landId][_landLord].price;
		_duration = rentStorage[_landId][_landLord].duration;
		_active = rentStorage[_landId][_landLord].active;
	}

	function updatePrice(address _landLord, uint256 _landId, uint256 newPrice) public {
		require(msg.sender == landToken_.ownerOf(_landId) || msg.sender == rentStorage[_landId][_landLord].tenant);
		require(!rentStorage[_landId][_landLord].active);
		rentStorage[_landId][_landLord].price = newPrice;
	}


	function updateDuration(address _landLord, uint256 _landId, uint256 newDuration) public{
		require(msg.sender == landToken_.ownerOf(_landId) || msg.sender == rentStorage[_landId][_landLord].tenant);
		require(!rentStorage[_landId][_landLord].active);
		rentStorage[_landId][_landLord].duration = newDuration;
	}


	function _createRent(uint256 _dur, uint256 _price, address _from) internal view returns(LeaseContract memory _lease) {

		_lease = LeaseContract({
			tenant: address(0),
			price: _price,
			duration: _dur,
			active: true
			});
	}

	uint256[50] private ______gap;
}
