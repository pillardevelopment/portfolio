pragma solidity ^0.4.24;

import "./ArconaManorStore.sol";

contract ArconaManor is ArconaManorStore {

    function init() public isInitializer("Arcona Manor ", "1.0.0") {
        ArconaManorStore.initialize();
    }

    function() public payable onlyWhitelisted() { }

    /**
   * @dev Allows to withdraw funds from the contract.
   * Usage of this method only treasurer
   */
    function withdrawBalance() external onlyTreasurer {
        uint256 balance = address(this).balance;
        treasurer.transfer(balance);
    }

    uint256[50] private ______gap;
}