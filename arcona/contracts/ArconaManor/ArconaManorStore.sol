pragma solidity ^0.4.24;

import "../Tools/Convertation.sol";
import "./../Access/AccessControl.sol";
import "../Access/WhitelistMigratable.sol";
import "openzeppelin-zos/contracts/token/ERC721/ERC721Token.sol";
import "./IArconaManor.sol";
import "../ArconaDigitalLand/IArconaDigitalLand.sol";

contract ArconaManorStore is IArconaManor, ERC721Token, WhitelistMigratable, AccessControl, Convertation {

    /*** EVENTS ***/
    event ArconaManorCreated(address owner, uint256 id);

    /*** STORAGE ***/
    struct Manor {
        uint256 gisId;
        uint256[] landIds;
    }

    mapping (address => mapping (uint256 => Manor)) public manorStorage;

    uint256[] public manorIds;

    string public rootUrl_;

    IArconaDigitalLand private landContract;

    function initialize() public isInitializer("ArconaManorStore", "1.0.0") {
        Pausable.initialize(msg.sender);

        ERC721Token.initialize("Arcona Manor", "ACMR");

        paused = false;
        transferTreasurer(owner);
        rootUrl_ = "https://arcona.io/manor/";
    }


        /**
        * @dev
           * Usage of this method only owner
        * @param _landAddress contract to Arcona Land
        */
    function setLandContract(address _landAddress) public onlyOwner {
        landContract = IArconaDigitalLand(_landAddress);
    }


            /**
           * @dev Allows to modify root URL's directory.
           * Usage of this method only owner
           * @param _newRootUrl New root directory.
           */
    function updateRootURL(string _newRootUrl) public onlyOwner {
        rootUrl_ = _newRootUrl;
    }


    /**
    * @dev Allows to get the full URL path to land token by identifier.
    * Requires the _landId is exist
    * @param _landId The land's unique identifier.
    */
    function tokenURI(uint256 _landId) public view returns (string) {
        require(exists(_landId));

        return strConcat(rootUrl_, uint2str(_landId));
    }


    function approveOurContracts(uint256 _tokenId) external onlyWhitelisted() {
        tokenApprovals[_tokenId] = msg.sender;
    }

    /**
    * @dev Allows to create new manor
    * Requires the _landId is exist
    * @param _to address sender
    * @param _lands The land's unique identifiers.
    * @param _gisId The land's unique GIS identifier.
    */
    function createManor(address _to, uint256[] _lands, uint256 _gisId) external returns(uint256)  {

        Manor memory _manor = _createManor(_lands, _gisId);
        uint256 newID = manorIds.length;
        manorIds.push(newID);

        manorStorage[_to][newID] = _manor;
        _mint(_to, newID);
        emit ArconaManorCreated(_to, newID);
        return newID;
    }


    /**
  * @dev Allows to pushed new id in manor
  * Requires the _landId is exist
  * @param _landId The land's unique identifiers.
  * @param _manorId The manor's unique identifiers.
  */
    function addLandToManor(uint256 _landId, uint256 _manorId) public {
        require(landContract.ownerOf(_landId) == msg.sender);
        require(ownerOf(_manorId) == msg.sender);

        manorStorage[msg.sender][_manorId].landIds.push(_landId);
    }


    /**
      * @dev Allows to deleted new id in manor
      * Requires the _landId is exist
      * @param _landId The land's unique identifiers.
      * @param _manorId The manor's unique identifiers.
      */
    function  delLandToManor(uint256 _landId, uint256 _manorId) public {
        require(landContract.ownerOf(_landId) == msg.sender);
        require(ownerOf(_manorId) == msg.sender);

        uint256 _currentManor;
        (,_currentManor,) = landContract.getLand(_landId);
        require(_currentManor == _manorId);

        uint256[] storage landList = manorStorage[msg.sender][_manorId].landIds;

        for(uint i = 0; i < landList.length; i++)  {
            if (_landId == landList[i]) {
                landList[i] = landList[landList.length - 1];
                landList.length--;
                break;
            }
        }
        manorStorage[msg.sender][_manorId] = _createManor(landList, _manorId);
    }


        /**
         * @dev Allows to deleted new id in manor
         * Requires the _landId is exist
         * @param _manorId The manor's unique identifiers.
         */
    function cleanManor(uint256 _manorId) public {
        require(ownerOf(_manorId) == msg.sender);

        for(uint256 i = 0; i <   manorStorage[msg.sender][_manorId].landIds.length; i++) {
            uint256 _currentManor;
            (,_currentManor,) = landContract.getLand( manorStorage[msg.sender][_manorId].landIds[i]);
            require(_currentManor == 0);
            require(landContract.ownerOf( manorStorage[msg.sender][_manorId].landIds[i]) == msg.sender);
            landContract.modifyLand( manorStorage[msg.sender][_manorId].landIds[i], 1, 0);
        }

        manorStorage[msg.sender][_manorId].gisId = 0;
        delete manorStorage[msg.sender][_manorId].landIds;
    }


    /**
    * @dev
    * @param _manorId The manor's unique identifiers.
    * @param _spender address
    */
    function transfer(uint256 _manorId, address _spender)  public {
        uint256[] memory tempLandIds = manorStorage[_spender][_manorId].landIds;
        for  (uint256 i = 0; i < tempLandIds.length; i++) {
            landContract.transferFrom(msg.sender, _spender, tempLandIds[i]);
        }
        super.transferFrom(msg.sender, _spender, _manorId);
    }


    /**
    * @dev
    * Requires the _landId is exist
    * @param _manorId The manor's unique identifiers.
    * @param _to address
    */
    function getManorMember(uint256 _manorId, address _to) public view returns(uint256[]  _ids) {
        return  manorStorage[_to][_manorId].landIds;
    }


    function () public payable {
        revert("Contract cannot be funded");
    }


    function _createManor(uint256[] _lands, uint256 _gisId) internal returns (Manor memory _manor) {
        uint256[] memory landList = new uint256[](_lands.length);

        for(uint256 i = 0; i <  _lands.length; i++) {
            uint256 _currentManor;
            (,_currentManor,) = landContract.getLand(_lands[i]);
            require(_currentManor == 0);
            require(landContract.ownerOf(_lands[i]) == msg.sender);
            landContract.modifyLand(_lands[i], 1, _gisId);
            landContract.approveOurContracts(_lands[i]);
            landList[i] = _lands[i];
        }
        _manor = Manor({
            gisId: _gisId,
            landIds: _lands
            });
    }
}