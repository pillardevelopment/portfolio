pragma solidity ^0.4.24;

contract IArconaManor {

    function createManor(address _to, uint256[] _lands, uint256 _gisID) external returns(uint256);

    function addLandToManor(uint256 _landId, uint256 _manorId) public;

    function delLandToManor(uint256 _landId, uint256 _manorId) public;

    function tokenURI(uint256 _landId) public view returns (string);

    function cleanManor(uint256 _manorId) public;

    function getManorMember(uint256 _manorId, address _to) public view returns(uint256[]  _ids);

    function approveOurContracts(uint256 _tokenId) external;
}
