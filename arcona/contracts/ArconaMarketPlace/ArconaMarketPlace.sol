pragma solidity ^0.4.24;

import  "./AuctionBase.sol";

contract ArconaMarketPlace is AuctionBase {

    function init(address _land, address _manor) isInitializer("ArconaMarketplace", "1.0.0")  public {
        AuctionBase.initialize(_land, _manor);
    }

    /**
      * @dev  Allows to create land
      * @param _gisId The land's unique identifier.
      */
    function createAndBuyLand(uint256 _gisId) public returns(uint256) {
        uint256 _tokenId = landContract.mintLand(this, _gisId, 0); // токен до выкупа будет на балансе контракта;
        _createAuction(this, _tokenId, 0, 0, 0);
        emit CreateAnBought(msg.sender, _gisId, _tokenId);
        return _tokenId;
    }


    /**
      * @dev  Allows to create land
      * @param _gisId The land's unique identifier.
      */
    function createAndBuyManor(uint256 _gisId, uint256[] _gisLandId) public  {

        uint256[]  _landIs;
         for (uint256 i = 0; i < _gisLandId.length; i++) {
             uint256 _tokenId = landContract.mintLand(this, _gisLandId[i], 0);
             _landIs.push(_tokenId);
        }

        uint256 manorId = manorContract.createManor(this, _landIs, _gisId);
        _createAuction(this, _tokenId, 0, 0, 1);
        emit CreateAnBought(msg.sender, _gisId, _tokenId);
    }


    /**
   * @dev  Allows to create auction to land
   * @param _landId The land's unique identifier.
   * @param _startPrice  starting price to auction
   * @param _duration minimum duration to auction
   */
    function createLandAuction(uint256 _landId, uint256 _startPrice, uint256 _duration) public {
        require(_duration >= minDuration && _duration <= maxDuration);
        require(_owns(msg.sender, _landId));

        landContract.approveOurContracts(_landId);
        _createAuction(msg.sender, _landId, _startPrice, _duration, 0);
    }


    /**
   * @dev  Allows to create auction to manor
   * @param _manorId The manor's unique identifier.
   * @param _startPrice  starting price to auction
   * @param _duration minimum duration to auction
   */
    function createManorAuction(uint256 _manorId, uint256 _startPrice, uint256 _duration) public {
        require(_duration >= minDuration && _duration <= maxDuration);
        require(_owns(msg.sender, _manorId));

        manorContract.approveOurContracts(_manorId);
        _createAuction(msg.sender, _manorId, _startPrice, _duration, 1);
    }


    /**
   * @dev  Allows to create auction to land
   * @param _landId The land's unique identifier.
    */
    function cancelLandAuction(uint256 _landId)  external {
        Auction storage auction = landAuctions[_landId];
        require(_isOnAuction(auction));
        address seller = auction.owner;
        require(msg.sender == seller);
        _cancelAuction(_landId, seller, 0);

        _deleteSellerToLandId(seller, _landId);
    }


    /**
   * @dev  Allows to create auction to land
   * @param _manorId The manor's unique identifier.
    */
    function cancelManorAuction(uint256 _manorId)  external {
        Auction storage auction = manorAuctions[_manorId];
        require(_isOnAuction(auction));
        address seller = auction.owner;
        require(msg.sender == seller);
        _cancelAuction(_manorId, seller, 1);

        _deleteSellerToManorId(seller, _manorId);
    }


    /**
    * @dev  Allows to create auction to land
    * @param _winner new owner to land
    * @param _landId The land's unique identifier.
    * @param _finalPrice finalize price
    * @param _executeTime time to buyout to token
     */
    function setLandAuctionWinner(address _winner, uint256 _landId, uint256 _finalPrice, uint256 _executeTime) onlyAdmin public {
        require(now > landAuctions[_landId].stopTime);
        //require(landAuctions[_landId].winner == address(0));
        require(_finalPrice >= landAuctions[_landId].startPrice);

        landAuctions[_landId].winner = _winner;
        landAuctions[_landId].finalPrice = uint128(_finalPrice);
        if (_executeTime > 0) {
            landAuctions[_landId].executeTime = uint64( now + (_executeTime * 1 minutes));
        }
        emit WinnerLandDetermined(_winner, _landId, _finalPrice, _executeTime);
    }

    /**
  * @dev  Allows to create auction to manor
  * @param _winner new owner to manor
  * @param _manorId The manor's unique identifier.
  * @param _finalPrice finalize price
  * @param _executeTime time to buyout to token
   */
    function setManorAuctionWinner(address _winner, uint256 _manorId, uint256 _finalPrice, uint256 _executeTime) onlyAdmin public {
        require(now > manorAuctions[_manorId].stopTime);
        //require(manorAuctions[_landId].winner == address(0));
        require(_finalPrice >= manorAuctions[_manorId].startPrice);

        manorAuctions[_manorId].winner = _winner;
        manorAuctions[_manorId].finalPrice = uint128(_finalPrice);
        if (_executeTime > 0) {
            manorAuctions[_manorId].executeTime = uint64( now + (_executeTime * 1 minutes));
        }
        emit WinnerManorDetermined(_winner, _manorId, _finalPrice, _executeTime);
    }


    /**
     * @dev  Allows to transfer to land
     * @param _landId The land's unique identifier.
     */
    function buyBackLandToken(uint256 _landId) public {
        Auction storage auction = landAuctions[_landId];
        require(_isOnAuction(auction));
        require(now <= landAuctions[_landId].executeTime);
        require(msg.sender == landAuctions[_landId].winner);

        uint256 fullPrice = landAuctions[_landId].finalPrice;
        require(arconaToken_.transferFrom(msg.sender, this, fullPrice));

       // if (!inWhiteList(landAuctions[_landId].owner)) {
       //     uint256 fee = valueFromPercent(fullPrice, auctionFee);
      //      fullPrice = fullPrice.sub(fee).sub(gasInTokens);
      //  }
        arconaToken_.transfer(landAuctions[_landId].owner, fullPrice);

        _transfer(address(this), msg.sender, _landId, 0);

        _deleteSellerToLandId(msg.sender, _landId);
        emit LandReceived(_landId, msg.sender);
    }


    /**
     * @dev  Allows to transfer to manor
     * @param _manorId The manor's unique identifier.
     */
    function buyBackManorToken(uint256 _manorId) public {
        Auction storage auction = manorAuctions[_manorId];
        require(_isOnAuction(auction));
        require(now <= manorAuctions[_manorId].executeTime);
        require(msg.sender == manorAuctions[_manorId].winner);

        uint256 fullPrice = manorAuctions[_manorId].finalPrice;
        require(arconaToken_.transferFrom(msg.sender, this, fullPrice));

        // if (!inWhiteList(manorAuctions[_landId].owner)) {
        //     uint256 fee = valueFromPercent(fullPrice, auctionFee);
        //      fullPrice = fullPrice.sub(fee).sub(gasInTokens);
        //  }
        arconaToken_.transfer(manorAuctions[_manorId].owner, fullPrice);

        _transfer(address(this), msg.sender, _manorId, 1);

        _deleteSellerToManorId(msg.sender, _manorId);
        emit ManorReceived(_manorId, msg.sender);
    }


    /**
     * @dev Returns auction info for an NFT on auction.
     * @param _landId - ID of NFT on auction.
     */
    function getLandAuction(uint256 _landId)  external view returns (   address owner,
                                                                        address winner,
                                                                        uint128 startPrice,
                                                                        uint128 finalPrice,
                                                                        uint64 startedAt,
                                                                        uint64 stopTime,
                                                                        uint64 executeTime) {

        Auction storage auction = landAuctions[_landId];
           require(_isOnAuction(auction));
            return (auction.owner,
                    auction.winner,
                    auction.startPrice,
                    auction.finalPrice,
                    auction.startedAt,
                    auction.stopTime,
                    auction.executeTime
        );
    }

    /// @dev Returns auction info for an NFT on auction.
    /// @param _manorId - ID of NFT on auction.
    function getManorAuction(uint256 _manorId)  external view returns ( address owner,
                                                                        address winner,
                                                                        uint128 startPrice,
                                                                        uint128 finalPrice,
                                                                        uint64 startedAt,
                                                                        uint64 stopTime,
                                                                        uint64 executeTime) {

        Auction storage auction = manorAuctions[_manorId];
            require(_isOnAuction(auction));
                return (auction.owner,
                        auction.winner,
                        auction.startPrice,
                        auction.finalPrice,
                        auction.startedAt,
                        auction.stopTime,
                        auction.executeTime
        );
    }

    function withdrawBalance() external onlyTreasurer {
        uint256 balance = address(this).balance;

        treasurer.transfer(balance);
    }

    function _deleteSellerToLandId(address _seller, uint256 _landId) internal {
        uint256[] storage tokenIds = tokenIds = sellerToLandIds[_seller];

        for(uint i = 0; i < tokenIds.length; i++) {
            if (tokenIds[i] == _landId) {

                tokenIds[i] = tokenIds[tokenIds.length - 1];
                tokenIds.length--;
                break;
            }
        }
    }

    function _deleteSellerToManorId(address _seller, uint256 _manorId) internal {
        uint256[] storage tokenIds = tokenIds = sellerToManorIds[_seller];

        for(uint i = 0; i < tokenIds.length; i++) {
            if (tokenIds[i] == _manorId) {

                tokenIds[i] = tokenIds[tokenIds.length - 1];
                tokenIds.length--;
                break;
            }
        }
    }
    uint256[50] private ______gap;
}
