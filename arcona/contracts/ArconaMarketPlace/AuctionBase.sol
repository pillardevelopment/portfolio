pragma solidity ^0.4.24;

import "./AuctionStorage.sol";
import "../Access/AccessControl.sol";
import "../Access/WhitelistMigratable.sol";

contract AuctionBase  is AuctionStorage, AccessControl, WhitelistMigratable {

    function initialize(address _nftAddress1, address _nftAddress2) isInitializer("BaseAuction", "1.0.0")  public {
        Pausable.initialize(msg.sender);

        ERC721 candidateContract1 = ERC721(_nftAddress1);
        ERC721 candidateContract2 = ERC721(_nftAddress2);

        landContract = IArconaDigitalLand(_nftAddress1);
        manorContract = IArconaManor(_nftAddress2);

        nonFungibleContractLand = candidateContract1;
        nonFungibleContractManor = candidateContract2;
        transferTreasurer(owner);

        defaultExecuteTime = 24 hours;
        minDuration = 1;
        maxDuration = 20160;
        auctionFee  =  300;
    }

    /**
      * @dev Allows to set ERC20 token
      * Usage of this method only owner
      * @param _token ARCONA ERC20 token
      */
    function setTokenContract(address _token) public onlyOwner {
        arconaToken_ = ERC20(_token);
    }

    /**
    * @dev  Allows to set set Default Execute Time
    * Usage of this method only owner
    * @param _hours - duration to  execute time
    */
    function setDefaultExecuteTime(uint256 _hours) onlyAdmin public {
        defaultExecuteTime = _hours * 1 hours;
    }

    /**
    * @dev  Allows to set Auction Fee
    * Usage of this method   onlyAdmin
    * @param _fee  owner cut
    */
    function setAuctionFee(uint256 _fee) onlyAdmin public {
        auctionFee = _fee;
    }

    /**
    * @dev  Allows to set min duration
    * Usage of this method only onlyAdmin
    * @param  _minDuration minimum duration
    */
    function setMinDuration(uint256 _minDuration) onlyAdmin public {
        minDuration = _minDuration;
    }

    /**
    * @dev  Allows to set max duration
    * Usage of this method only onlyAdmin
    * @param _maxDuration  maximum duration
    */
    function setMaxDuration(uint256 _maxDuration) onlyAdmin public {
        maxDuration = _maxDuration;
    }


    /**
    * @dev  Allows to get
    * @param _owner owner to auctions
    function ownerAuctionCount(address _owner) public view returns (uint256) {
        return ownedAuctions[_owner].length;
    }
*/

    /**
    * @dev  Allows to withdraw ERC 20 Arcona tokens
    * Usage of this method only Admins
    */
    function withdrawTokens() onlyAdmin public {
        require(arconaToken_.balanceOf(this) > 0);
        arconaToken_.transfer(treasurer, arconaToken_.balanceOf(this));
    }

    /**
    * @dev  Remove this contract from blockchain
    * Usage of this method only Owner
     */
    function destruct() onlyOwner public {
        selfdestruct(owner);
    }

    function _createAuction(address _seller, uint256 _id, uint256 _startPrice, uint256 _duration, uint256 _modifier) internal {
        Auction memory auction = Auction(
            _seller,
            address(0),
            uint128(_startPrice),
            uint128(0),
            uint64(now),
            uint64( now + (_duration * 1 minutes)),
            uint64( now + (_duration * 1 minutes) + defaultExecuteTime)
        );

        _transfer(_seller, this, _id, _modifier);

        _addAuction(_id, auction, _modifier);
    }

    function _owns(address _claimant, uint256 _id) internal view returns (bool) {
        return (nonFungibleContractLand.ownerOf(_id) == _claimant || nonFungibleContractManor.ownerOf(_id) == _claimant);
    }

    function _transfer(address _owner, address _receiver, uint256 _id, uint256 _mod) internal {
        if (_mod == 0) {
            nonFungibleContractLand.transferFrom(_owner, _receiver, _id);
        }
        if (_mod == 1) {
            nonFungibleContractManor.transferFrom(_owner, _receiver, _id);
        }
    }

    function _addAuction(uint256 _id, Auction _auction, uint256 _modifier) internal {
        if (_modifier == 0) {
            sellerToLandIds[_auction.owner].push(_id);
            landAuctions[_id] = _auction;
            emit AuctionLandCreated( _id,  _auction.owner, uint256(_auction.startPrice), uint256(_auction.stopTime));
        }
        else {
            sellerToManorIds[_auction.owner].push(_id);
            manorAuctions[_id] = _auction;
            emit AuctionManorCreated( _id,  _auction.owner, uint256(_auction.startPrice), uint256(_auction.stopTime));
        }
    }

    function _cancelAuction(uint256 _id, address _seller, uint256 _mod) internal {
        _transfer(address(this), _seller, _id, _mod);

        if(_mod == 0) {
            _removeLandAuction(_id);
            emit AuctionLandCancelled(_id);
        }
        else {
            _removeManorAuction(_id);
            emit AuctionManorCancelled(_id);
        }
    }

    function _removeLandAuction(uint256 _landId) internal {
        delete landAuctions[_landId];
    }

    function _removeManorAuction(uint256 _manorId) internal {
        delete manorAuctions[_manorId];
    }

    function _computeCut(uint256 _price) internal view returns (uint256) {
        return _price * auctionFee / 10000;
    }

    function _isOnAuction(Auction storage _auction) internal view returns (bool) {
        return (_auction.startedAt > 0);
    }
}
