pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-zos/contracts/token/ERC721/ERC721.sol";
import "../ArconaDigitalLand/IArconaDigitalLand.sol";
import "../ArconaManor/IArconaManor.sol";

contract AuctionStorage {

    /***********  EVENTS ***********/
    event AuctionLandCreated( uint256 landId, address  owner, uint256 startPrice, uint256 stopTime);
    event AuctionLandCancelled(uint landId);
    event WinnerLandDetermined(address winner, uint landId, uint finalPrice, uint executeTime);
    event LandReceived(uint256 tokend, address winner);

    event AuctionManorCreated( uint256 manorId, address  owner, uint256 startPrice, uint256 stopTime);
    event AuctionManorCancelled(uint manorId);
    event WinnerManorDetermined(address winner, uint manorId, uint finalPrice, uint executeTime);
    event ManorReceived(uint256 tokend, address winner);

    event CreateAnBought(address buyer, uint256 gisId, uint256 tokenId);

    ERC20 public arconaToken_;
    ERC721 public nonFungibleContractLand;
    ERC721 public nonFungibleContractManor;

    IArconaDigitalLand public landContract;
    IArconaManor public manorContract;

    /***********  STORAGE ***********/
    mapping (address => uint256[]) sellerToLandIds;
    mapping (address => uint256[]) sellerToManorIds;

    mapping(uint256 => Auction) public landAuctions;
    mapping(uint256 => Auction) public manorAuctions;

    uint256 public defaultExecuteTime;
    uint256 public minDuration;
    uint256 public maxDuration;
    uint256 public auctionFee; //3% Cut owner takes on each auction, measured in basis points (1/100 of a percent).

    struct Auction {
        address owner;
        address winner;
        uint128 startPrice;
        uint128 finalPrice;
        uint64 startedAt;
        uint64 stopTime;
        uint64 executeTime;
    }
}
