pragma solidity ^0.4.24;

contract IArconaMarketPlace {

    function createAuction(uint256 _landId, uint256 _startPrice, uint256 _duration) public;

    function cancelAuction(uint256 _landId)  external;

    function setWinner(address _winner, uint256 _landId, uint256 _finalPrice, uint256 _executeTime)  public;

    function buyBackLandToken(uint256 _landId) public;

    function getAuction(uint256 _landId)  external view returns  (
                                                                                            address owner,
                                                                                            address winner,
                                                                                            uint128 startPrice,
                                                                                            uint128 finalPrice,
                                                                                            uint64 startedAt,
                                                                                            uint64 stopTime,
                                                                                            uint64 executeTime);

    function clearAll(address _seller, uint _moduleLimitation)  external;

    function clearOne(address _seller, uint _landId) external;
}
