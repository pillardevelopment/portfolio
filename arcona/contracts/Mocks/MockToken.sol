pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol";
import "openzeppelin-solidity/contracts/token/ERC20/DetailedERC20.sol";

contract MockToken is StandardToken, DetailedERC20 {

    uint8 constant DECIMALS = 18;
    uint256 constant _initialSupply = 1000000;

    constructor() DetailedERC20("Arcona Mock token", "AMT", DECIMALS) public {
        totalSupply_ = _initialSupply * 10 ** uint256(DECIMALS);
        balances[msg.sender] =  totalSupply_;
        emit Transfer(this, msg.sender, totalSupply_);
    }
}
