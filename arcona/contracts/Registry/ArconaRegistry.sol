pragma solidity ^0.4.24;

import "openzeppelin-zos/contracts/lifecycle/Pausable.sol";
import "./IArconaRegistry.sol";

contract ArconaRegistry is IArconaRegistry, Pausable {
    address landToken_;
    address arconaDigitalLand_;
    address arconaAuction_;
    address arconaManor_;
    address arconaLandRent_;


    function initialize() public isInitializer("ArconaRegistry", "1.0.0") {
        Ownable.initialize(msg.sender);
    }

    function getLandToken() view external returns(address) {
        return landToken_;
    }

    function getArconaDigitalLand() view external returns(address) {
        return arconaDigitalLand_;
    }

    function getArconaManor() view external returns(address) {
        return arconaManor_;
    }

    function getArconaAuction_() view external returns(address) {
        return arconaAuction_;
    }

    function getArconaLandRent() view external returns(address) {
        return arconaLandRent_;
    }

    function setLandToken(address _address) onlyOwner whenNotPaused public {
        require(_address != address(0), "landToken contract address should be defined");
        landToken_ = _address;
    }

    function setArconaDigitalLand(address _address) onlyOwner whenNotPaused public {
        require(_address != address(0), "arconaDigitalLand contract address should be defined");
        arconaDigitalLand_ = _address;
    }

    function setArconaManor(address _address) onlyOwner whenNotPaused  public {
        require(_address != address(0), "arconaManor contract address should be defined");
        arconaManor_ = _address;
    }

    function setArconaAuction_(address _address)  onlyOwner whenNotPaused public {
        require(_address != address(0), "arconaAuction contract address should be defined");
        arconaAuction_ = _address;
    }

    function setArrconaLandRent(address _address) onlyOwner whenNotPaused public {
        require(_address != address(0), "arconaLandRent contract address should be defined");
        arconaLandRent_ = _address;
    }
}