pragma solidity ^0.4.24;

contract IArconaRegistry {

    function getLandToken() view external returns(address);

    function getArconaDigitalLand() view external returns(address);

    function getArconaManor() view external returns(address);

    function getArconaAuction_() view external returns(address);

    function getArconaLandRent() view external returns(address);
}
