const ArconaManor = artifacts.require("./ArconaManor/ArconaManor.sol");
const ArconaManorProxy = artifacts.require("./ArconaManor/ArconaManorProxy.sol");

require('dotenv').config();
const delay = require('delay');

const paused = parseInt( process.env.DELAY_MS || "20000" );

const wait = async (param) => { console.log("Delay " + paused); await delay(paused); return param;};
const logReceipt = (receipt, name) => console.log(name + " :: success :: " + receipt.tx);

module.exports = function(deployer, network, accounts) {
  deployer.then(async () => {
    await wait();

    const CONTRACT_ADMIN_ACCOUNT = accounts[0];
    console.log('CONTRACT_ADMIN_ACCOUNT :: ',CONTRACT_ADMIN_ACCOUNT);
    const PROXY_ADMIN_ACCOUNT = accounts[1];
    console.log('PROXY_ADMIN_ACCOUNT :: ',PROXY_ADMIN_ACCOUNT);

    await wait(await deployer.deploy(ArconaManor));
    await wait(await deployer.deploy(ArconaManorProxy, ArconaManor.address, {from: PROXY_ADMIN_ACCOUNT}));

    let manor = await ArconaManor.at(ArconaManorProxy.address);
    await wait(logReceipt(await manor.init( {from: CONTRACT_ADMIN_ACCOUNT} ),
        'manor.initialize'));
  });
};