const ArconaMarketPlace = artifacts.require("./ArconaMarketPlace/ArconaMarketPlace.sol");
const ArconaMarketPlaceProxy = artifacts.require("./ArconaMarketPlace/ArconaMarketPlaceProxy.sol");
const ArconaDigitalLand = artifacts.require("./ArconaDigitalLand/ArconaDigitalLand.sol");
const ArconaDigitalLandProxy = artifacts.require("./ArconaDigitalLand/ArconaDigitalLandProxy.sol");
const ArconaManor = artifacts.require("./ArconaManor/ArconaManor.sol");
const ArconaManorProxy = artifacts.require("./ArconaManor/ArconaManorProxy.sol");

require('dotenv').config();
const delay = require('delay/index');
const paused = parseInt( process.env.DELAY_MS || "20000" );

const wait = async (param) => { console.log("Delay " + paused); await delay(paused); return param;};
const logReceipt = (receipt, name) => console.log(name + " :: success :: " + receipt.tx);

module.exports = function(deployer, network, accounts) {
  deployer.then(async () => {
    await wait();

    const CONTRACT_ADMIN_ACCOUNT = accounts[0];
    console.log('CONTRACT_ADMIN_ACCOUNT :: ',CONTRACT_ADMIN_ACCOUNT);
    const PROXY_ADMIN_ACCOUNT = accounts[1];
    console.log('PROXY_ADMIN_ACCOUNT :: ',PROXY_ADMIN_ACCOUNT);

    const land = await ArconaDigitalLand.at(ArconaDigitalLandProxy.address);
    const manor = await ArconaManor.at(ArconaManorProxy.address);

    await wait(await deployer.deploy(ArconaMarketPlace));
    await wait(await deployer.deploy(ArconaMarketPlaceProxy, ArconaMarketPlace.address, {from: PROXY_ADMIN_ACCOUNT}));

    let marketPlace = await ArconaMarketPlace.at(ArconaMarketPlaceProxy.address);

    await wait(logReceipt(await marketPlace.init(land.address, manor.address, {from: CONTRACT_ADMIN_ACCOUNT} ),
        'marketPlace.init ' + land.address + ' ' + manor.address));
  });
};
