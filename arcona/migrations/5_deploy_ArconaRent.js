const ArconaRent = artifacts.require("./ArconaDigitalLandRent/ArconaDigitalLandRent.sol");
const ArconaRentProxy = artifacts.require("./ArconaDigitalLandRent/ArconaDigitalLandRentProxy.sol");
const ArconaDigitalLandProxy = artifacts.require("./ArconaDigitalLand/ArconaDigitalLandProxy.sol");

const tokenContract = process.env.TOKEN_CONTRACT;

if(!tokenContract){
    throw("TOKEN_CONTRACT is not configured in .env!");
}

require('dotenv').config();
const delay = require('delay');

const paused = parseInt( process.env.DELAY_MS || "20000" );

const wait = async (param) => { console.log("Delay " + paused); await delay(paused); return param;};
const logReceipt = (receipt, name) => console.log(name + " :: success :: " + receipt.tx);

module.exports = function(deployer, network, accounts) {
    deployer.then(async () => {
        await wait();

        const CONTRACT_ADMIN_ACCOUNT = accounts[0];
        console.log('CONTRACT_ADMIN_ACCOUNT :: ',CONTRACT_ADMIN_ACCOUNT);
        const PROXY_ADMIN_ACCOUNT = accounts[1];
        console.log('PROXY_ADMIN_ACCOUNT :: ',PROXY_ADMIN_ACCOUNT);

        await wait(await deployer.deploy(ArconaRent));
        await wait(await deployer.deploy(ArconaRentProxy, ArconaRent.address, {from: PROXY_ADMIN_ACCOUNT}));

        let rentContract = await ArconaRent.at(ArconaRentProxy.address);
        await wait(logReceipt(await rentContract.initialize( ArconaDigitalLandProxy.address, tokenContract,  {from: CONTRACT_ADMIN_ACCOUNT} ),
            'rentContract.initialize'));
    });
};