const ArconaRegistry = artifacts.require("./ArconaRegistry/ArconaRegistry.sol");
const ArconaRegistryProxy = artifacts.require("./ArconaRegistry/ArconaRegistryProxy.sol");


require('dotenv').config();
const delay = require('delay');

const paused = parseInt( process.env.DELAY_MS || "20000" );

const wait = async (param) => { console.log("Delay " + paused); await delay(paused); return param;};
const logReceipt = (receipt, name) => console.log(name + " :: success :: " + receipt.tx);

module.exports = function(deployer, network, accounts) {
    deployer.then(async () => {
        await wait();

        const CONTRACT_ADMIN_ACCOUNT = accounts[0];
        console.log('CONTRACT_ADMIN_ACCOUNT :: ',CONTRACT_ADMIN_ACCOUNT);
        const PROXY_ADMIN_ACCOUNT = accounts[1];
        console.log('PROXY_ADMIN_ACCOUNT :: ',PROXY_ADMIN_ACCOUNT);

        await wait(await deployer.deploy(ArconaRegistry));
        await wait(await deployer.deploy(ArconaRegistryProxy, ArconaRegistry.address, {from: PROXY_ADMIN_ACCOUNT}));

        let registryContract = await ArconaRegistry.at(ArconaRegistryProxy.address);
        await wait(logReceipt(await registryContract.initialize({from: CONTRACT_ADMIN_ACCOUNT} ),
            'registryContract.initialize'));
    });
};