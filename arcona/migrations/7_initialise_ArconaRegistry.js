const ArconaRegistry = artifacts.require("./ArconaRegistry/ArconaRegistry.sol");
const ArconaRegistryProxy = artifacts.require("./ArconaRegistry/ArconaRegistryProxy.sol");

const ArconaDigitalLand = artifacts.require("./ArconaDigitalLand/ArconaDigitalLand.sol");
const ArconaDigitalLandProxy = artifacts.require("./ArconaDigitalLand/ArconaDigitalLandProxy.sol");
const ArconaManor = artifacts.require("./ArconaManor/ArconaManor.sol");
const ArconaManorProxy = artifacts.require("./ArconaManor/ArconaManorProxy.sol");
const ArconaAuction = artifacts.require("./ArconaMarketPlace/ArconaMarketPlace.sol");
const ArconaAuctionProxy = artifacts.require("./ArconaMarketPlace/ArconaMarketPlaceProxy.sol");
const ArconaRent = artifacts.require("./ArconaDigitalLandRent/ArconaDigitalLandRent.sol");
const ArconaRentProxy = artifacts.require("./ArconaDigitalLandRent/ArconaDigitalLandRentProxy.sol");

const tokenContract = process.env.TOKEN_CONTRACT;;

if(!tokenContract){
  throw("TOKEN_CONTRACT is not configured in .env!");
}

require('dotenv').config();
const delay = require('delay');

const paused = parseInt( process.env.DELAY_MS || "20000" );

const wait = async (param) => { console.log("Delay " + paused); await delay(paused); return param;};
const logReceipt = (receipt, name) => console.log(name + " :: success :: " + receipt.tx);

module.exports = function(deployer) {
  deployer.then(async () => {
    await wait();

    const registryContract = await ArconaRegistry.at(ArconaRegistryProxy.address);
    const landContract = await ArconaDigitalLand.at(ArconaDigitalLandProxy.address);
    const manorContract = await ArconaManor.at(ArconaManorProxy.address);
    const auction = await ArconaAuction.at(ArconaAuctionProxy.address);
    const rentContract = await ArconaRent.at(ArconaRentProxy.address);

    await wait(logReceipt(await registryContract.setLandToken(tokenContract),
        "registryContract setLandToken " + tokenContract));

    await wait(logReceipt(await registryContract.setArconaDigitalLand(landContract.address),
        "registryContract setArconaDigitalLand " + landContract.address));

    await wait(logReceipt(await registryContract.setArconaManor(manorContract.address),
        "registryContract setArconaManor " + manorContract.address));

    await wait(logReceipt(await registryContract.setArconaAuction_(auction.address),
        "registryContract setArconaAuction_ " + auction.address));

    await wait(logReceipt(await registryContract.setArrconaLandRent(rentContract.address),
        "registryContract setArrconaLandRent " + rentContract.address));
  });
};
