const ArconaMarketPlace = artifacts.require("./ArconaMarketPlace/ArconaMarketPlace.sol");
const ArconaMarketPlaceProxy = artifacts.require("./ArconaMarketPlace/ArconaMarketPlaceProxy.sol");

const ArconaDigitalLand = artifacts.require("./ArconaDigitalLand/ArconaDigitalLand.sol");
const ArconaDigitalLandProxy = artifacts.require("./ArconaDigitalLand/ArconaDigitalLandProxy.sol");

const ArconaManor = artifacts.require("./ArconaManor/ArconaManor.sol");
const ArconaManorProxy = artifacts.require("./ArconaManor/ArconaManorProxy.sol");

require('dotenv').config();
const delay = require('delay');

const paused = parseInt( process.env.DELAY_MS || "60000" );

const wait = async (param) => { console.log("Delay " + paused); await delay(paused); return param;};
const logReceipt = (receipt, name) => console.log(name + " :: success :: " + receipt.tx);

const tokenContract = process.env.TOKEN_CONTRACT;

if(!tokenContract){
  throw("TOKEN_CONTRACT is not configured in .env!");
}

module.exports = function(deployer) {
  deployer.then(async () => {
    await wait();

    const land = await ArconaDigitalLand.at(ArconaDigitalLandProxy.address);
    const manor = await ArconaManor.at(ArconaManorProxy.address);

    const marketPlace = await ArconaMarketPlace.at(ArconaMarketPlaceProxy.address);

    await wait(logReceipt(await land.addAddressToWhitelist(marketPlace.address),
        "land addAddressToWhitelist " + marketPlace.address));

    await wait(logReceipt(await manor.addAddressToWhitelist(marketPlace.address),
      "manor addAddressToWhitelist " + marketPlace.address));

    await wait(logReceipt(await marketPlace.setTokenContract(tokenContract),
      "marketPlace setTokenContract " + tokenContract));
  });
};
