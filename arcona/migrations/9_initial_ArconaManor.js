const ArconaManor = artifacts.require("./ArconaManor/ArconaManor.sol");
const ArconaManorProxy = artifacts.require("./ArconaManor/ArconaManorProxy.sol");
const ArconaDigitalLand = artifacts.require("./ArconaDigitalLand/ArconaDigitalLand.sol");
const ArconaDigitalLandProxy = artifacts.require("./ArconaDigitalLand/ArconaDigitalLandProxy.sol");

require('dotenv').config();
const delay = require('delay');

const paused = parseInt( process.env.DELAY_MS || "20000" );

const wait = async (param) => { console.log("Delay " + paused); await delay(paused); return param;};
const logReceipt = (receipt, name) => console.log(name + " :: success :: " + receipt.tx);

module.exports = function(deployer) {
    deployer.then(async () => {
        await wait();

        const manor = await ArconaManor.at(ArconaManorProxy.address);
        const land = await ArconaDigitalLand.at(ArconaDigitalLandProxy.address);

        await wait(logReceipt(await manor.setLandContract(land.address),
            "manor setLandContract " + land.address));

        await wait(logReceipt(await land.addAddressToWhitelist(manor.address),
            "land addAddressToWhitelist " + manor.address));
    });
};
