// test/.ArconaDigitalRent.Test.js
const ArconaDigitalLand = artifacts.require("ArconaDigitalLand");
const ArconaDigitalLandProxy = artifacts.require("ArconaDigitalLandProxy");

const ArconaRent = artifacts.require("ArconaDigitalLandRent");
const ArconaRentProxy = artifacts.require("ArconaDigitalLandRentProxy");

const ArconaToken = artifacts.require("./Mocks/MockToken.sol");

const ETHER = 10**18;
const TOKEN = 10**18;

contract("ArconaDigital Rent", accounts => {

    const [firstAccount, secondAccount, thirdAccount, fourthAccount, fifthAccount, proxyAccount] = accounts;

    let land;
    let rentContract;
    let token;

    beforeEach(async () => {
        token = await ArconaToken.new();

        land = await ArconaDigitalLand.new();
        const landProxy  = await ArconaDigitalLandProxy.new(land.address, {from: proxyAccount});
        land = await ArconaDigitalLand.at(landProxy.address);
        await land.init();

        rentContract = await ArconaRent.new();
        const rentContractProxy = await ArconaRentProxy.new(rentContract.address, {from: proxyAccount});
        rentContract = await ArconaRent.at(rentContractProxy.address);
        await rentContract.initialize(landProxy.address, token.address);

        await rentContract.setTokenContract(token.address, { from: firstAccount });
        await rentContract.setLandContract(land.address, { from: firstAccount });

        await land.addAddressToWhitelist(firstAccount, {from: firstAccount});
        await land.addAddressToWhitelist(rentContract.address, {from: firstAccount});

        await token.transfer(secondAccount, 10000*TOKEN, {from: firstAccount});
        await token.transfer(thirdAccount, 10000*TOKEN, {from: firstAccount});
        await token.transfer(fourthAccount, 10000*TOKEN, {from: firstAccount});
        await token.transfer(fifthAccount, 10000*TOKEN, {from: firstAccount});

        assert.equal(await token.balanceOf.call(secondAccount), 10000*TOKEN);
        assert.equal(await token.balanceOf.call(thirdAccount), 10000*TOKEN);
        assert.equal(await token.balanceOf.call(fourthAccount), 10000*TOKEN);
        assert.equal(await token.balanceOf.call(fifthAccount), 10000*TOKEN);
        assert.equal(await token.totalSupply.call(), 1000000*TOKEN);
    });

    it("#1 should rented required land", async () => {
        assert.equal(await land.owner.call(), firstAccount);

        await land.addAddressToWhitelist(firstAccount);
        await land.mintLand(secondAccount, 53649020651777508, 0,  { from: firstAccount });
        await land.mintLand(thirdAccount, 982132106742,  0, { from: firstAccount });

        assert.equal(await land.balanceOf(secondAccount), 1);
        assert.equal(await land.balanceOf(thirdAccount), 1);
        assert.equal(await land.totalSupply.call(), 2);

        await rentContract.createRent(secondAccount, 0, 2*TOKEN, 9000,  { from: secondAccount });
        await token.approve(rentContract.address, 2*TOKEN, {from: thirdAccount});
        await rentContract.thirdAccount(0, secondAccount, {from: thirdAccount});
    });

});
