// test/.ArconaLand.Test.js
const ArconaDigitalLand = artifacts.require("ArconaDigitalLand");
const ArconaDigitalLandProxy = artifacts.require("ArconaDigitalLandProxy");

const ETHER = 10**18;

contract("Arcona Digital Land Test", accounts => {

    const [firstAccount, secondAccount, thirdaccount, fourthaccount, fifthaccount, proxyAccount] = accounts;

    let land;

    beforeEach(async () => {
        land = await ArconaDigitalLand.new();
        const landProxy  = await ArconaDigitalLandProxy.new(land.address, {from: proxyAccount});
        land = await ArconaDigitalLand.at(landProxy.address);
        await land.init();
    });

    it("#1 should initialize correctly", async () => {
        assert.equal(await land.owner.call(), firstAccount);
        assert.equal(await land.treasurer.call(), firstAccount);
    });

    it("#2 should deposit and withdraw funds correctly", async () => {
        await land.addAddressToWhitelist(firstAccount);
        await land.addAddressToWhitelist(secondAccount);
        await land.addAddressToWhitelist(thirdaccount);
        await land.addAddressToWhitelist(fourthaccount);

        await land.sendTransaction({from: firstAccount, value: 2 * ETHER});
        await land.sendTransaction({from: secondAccount, value: 1 * ETHER});
        await land.sendTransaction({from: thirdaccount, value: 10 * ETHER});
        await land.sendTransaction({from: fourthaccount, value: 25 * ETHER});
        await land.withdrawBalance();
    });

    it("#3 does not allow non-whitelist's members to deposit funds", async () => {
        try {
            await land.sendTransaction({from: thirdaccount, value: 2 * ETHER});
            assert.fail();
        } catch (err) {
            assert.ok(/revert/.test(err.message));
        }
    });

    it("#4 does not allow non-owners to withdraw funds",  async () => {
        await land.addAddressToWhitelist(secondAccount);
        await land.sendTransaction({from: secondAccount, value: 2 * ETHER});
        try {
            await land.withdrawBalance({ from: secondAccount });
            assert.fail();
        } catch (err) {
            assert.ok(/revert/.test(err.message));
        }
    });

    it("#5 does not allow non-owners to call pause",  async () => {
        try {
            await land.pause({ from: fifthaccount });
            assert.fail();
        } catch (err) {
            assert.ok(/revert/.test(err.message));
        }
    });

    it("#6 should minted required land", async () => {
        assert.equal(await land.owner.call(), firstAccount);

        await land.addAddressToWhitelist(firstAccount);
        await land.mintLand(firstAccount, 53649020651777508, 0,  { from: firstAccount });
        await land.mintLand(firstAccount, 982132106742,  2, { from: firstAccount });

        assert.equal(await land.balanceOf(firstAccount), 2);
        assert.equal(await land.totalSupply.call(), 2);
    });

    it("#7 does not minted during pause", async () => {
        await land.addAddressToWhitelist(firstAccount);
        try {
            await land.mintLand(firstAccount, 68230681623,  0, { from: fourthaccount });
            assert.fail();
        } catch (err) {
            assert.ok(/revert/.test(err.message));
        }
    });

    it("#8 does not allow non-owner and non-whitelisted", async () => {
        try {
            await land.mintLand(firstAccount, 3333333333, 0, { from: firstAccount });
            assert.fail();
        } catch (err) {
            assert.ok(/revert/.test(err.message));
        }
    });

    it("#9 should modified required land", async () => {
        assert.equal(await land.owner.call(), firstAccount);

        await land.addAddressToWhitelist(firstAccount);
        await land.mintLand(firstAccount, 0, 0,  { from: firstAccount });

        assert.equal(await land.balanceOf(firstAccount), 1);
        assert.equal(await land.totalSupply.call(), 1);

        await land.modifyLand(0, 1, 4, { from: firstAccount });
        await land.modifyLand(0, 2, 1, { from: firstAccount });

        try {
            await land.modifyLand(0, 3, 2, { from: firstAccount });
            assert.fail();
        } catch (err) {
            assert.ok(/revert/.test(err.message));
        }
    });
});