// test/.ArconaManor.Test.js
const ArconaDigitalLand = artifacts.require("ArconaDigitalLand");
const ArconaDigitalLandProxy = artifacts.require("ArconaDigitalLandProxy");
const ArconaManor = artifacts.require("ArconaManor");
const ArconaManorProxy = artifacts.require("ArconaManorProxy");

const ETHER = 10**18;

contract("Arcona ManorTest", accounts => {

    const [firstAccount, secondAccount, thirdaccount, fourthaccount, fifthaccount, proxyAccount] = accounts;

    let land;
    let manor;

    beforeEach(async () => {
        land = await ArconaDigitalLand.new();
        const landProxy  = await ArconaDigitalLandProxy.new(land.address, {from: proxyAccount});
        land = await ArconaDigitalLand.at(landProxy.address);
        await land.init();

        manor = await ArconaManor.new();
        const manorProxy  = await ArconaManorProxy.new(manor.address, {from: proxyAccount});
        manor = await ArconaManor.at(manorProxy.address);
        await manor.init();

        await manor.setLandContract(land.address, {from: firstAccount});
        await land.addAddressToWhitelist(manor.address, {from: firstAccount});
    });

    it("#1 should initialize correctly", async () => {
        assert.equal(await manor.owner.call(), firstAccount);
        assert.equal(await manor.treasurer.call(), firstAccount);
    });

    it("#2 should deposit and withdraw funds correctly", async () => {
        await manor.addAddressToWhitelist(firstAccount);
        await manor.addAddressToWhitelist(secondAccount);
        await manor.addAddressToWhitelist(thirdaccount);
        await manor.addAddressToWhitelist(fourthaccount);

        await manor.sendTransaction({from: firstAccount, value: 2 * ETHER});
        await manor.sendTransaction({from: secondAccount, value: 1 * ETHER});
        await manor.sendTransaction({from: thirdaccount, value: 10 * ETHER});
        await manor.sendTransaction({from: fourthaccount, value: 25 * ETHER});
        await manor.withdrawBalance();
    });

    it("#3 does not allow non-whitelist's members to deposit funds", async () => {
        try {
            await manor.sendTransaction({from: thirdaccount, value: 2 * ETHER});
            assert.fail();
        } catch (err) {
            assert.ok(/revert/.test(err.message));
        }
    });

    it("#4 does not allow non-owners to withdraw funds",  async () => {
        await manor.addAddressToWhitelist(secondAccount);
        await manor.sendTransaction({from: secondAccount, value: 2 * ETHER});
        try {
            await manor.withdrawBalance({ from: secondAccount });
            assert.fail();
        } catch (err) {
            assert.ok(/revert/.test(err.message));
        }
    });

    it("#5 does not allow non-owners to call pause",  async () => {
        try {
            await manor.pause({ from: fifthaccount });
            assert.fail();
        } catch (err) {
            assert.ok(/revert/.test(err.message));
        }
    });

    it("#6 should minted required manor", async () => {
        assert.equal(await manor.owner.call(), firstAccount);

        await manor.addAddressToWhitelist(firstAccount);
        await land.addAddressToWhitelist(firstAccount);

        await land.mintLand(firstAccount, 53649020651777508, 0, { from: firstAccount });
        await land.mintLand(firstAccount, 982132106742, 0, { from: firstAccount });
        await land.mintLand(firstAccount, 34343, 0, { from: firstAccount });
        await land.mintLand(firstAccount, 23466098, 0, { from: firstAccount });
        let ids = [0, 1, 2, 3];
        await manor.createManor(firstAccount, ids, 9001232, { from: firstAccount });

        assert.equal(await manor.balanceOf(firstAccount), 1);
        assert.equal(await manor.totalSupply.call(), 1);
    });

    it("#7 does not minted during pause", async () => {
        await manor.addAddressToWhitelist(firstAccount);
        await land.addAddressToWhitelist(firstAccount);

        await land.mintLand(firstAccount, 53649020651777508, 0, { from: firstAccount });
        await land.mintLand(firstAccount, 982132106742, 0, { from: firstAccount });
        await land.mintLand(firstAccount, 34343, 0, { from: firstAccount });
        await land.mintLand(firstAccount, 23466098, 0, { from: firstAccount });

        try {
            await manor.createManor(firstAccount, [0, 1, 2, 3], 9001232, { from: fourthaccount });
            assert.fail();
        } catch (err) {
            assert.ok(/revert/.test(err.message));
        }
    });
});