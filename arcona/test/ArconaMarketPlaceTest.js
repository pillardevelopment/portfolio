// test/.ArconaMarketPlaceTest.Test.js
const ArconaDigitalLand = artifacts.require("ArconaDigitalLand");
const ArconaDigitalLandProxy = artifacts.require("ArconaDigitalLandProxy");

const ArconaMarketPlace = artifacts.require("ArconaMarketPlace");
const ArconaMarketPlaceProxy = artifacts.require("ArconaMarketPlaceProxy");

const ArconaManor = artifacts.require("ArconaManor");
const ArconaManorProxy = artifacts.require("ArconaManorProxy");

const ArconaToken = artifacts.require("./Mocks/MockToken.sol");

const ETHER = 10**18;
const TOKEN = 10**18;

contract("Arcona MarketPlace", accounts => {

    const [firstAccount, secondAccount, thirdAccount, fourthAccount, fifthAccount, proxyAccount] = accounts;

    let land;
    let manor;
    let marketplace;
    let token;

    beforeEach(async () => {
        token = await ArconaToken.new();

        land = await ArconaDigitalLand.new();
        const landProxy  = await ArconaDigitalLandProxy.new(land.address, {from: proxyAccount});
        land = await ArconaDigitalLand.at(landProxy.address);
        await land.init();

        manor = await ArconaManor.new();
        const manorProxy  = await ArconaManorProxy.new(manor.address, {from: proxyAccount});
        manor = await ArconaManor.at(manorProxy.address);
        await manor.init();

        marketplace = await ArconaMarketPlace.new();
        const marketplaceProxy = await ArconaMarketPlaceProxy.new(marketplace.address, {from: proxyAccount});
        marketplace = await ArconaMarketPlace.at(marketplaceProxy.address);
        await marketplace.initialize(landProxy.address, manorProxy.address);

        await land.addAddressToWhitelist(land.address, {from: firstAccount});
        await land.addAddressToWhitelist(firstAccount, {from: firstAccount});

        await marketplace.setTokenContract(token.address, { from: firstAccount });

        await manor.setLandContract(land.address, {from: firstAccount});
        await land.addAddressToWhitelist(manor.address, {from: firstAccount});

        await land.addAddressToWhitelist(marketplace.address, {from: firstAccount});
        await manor.addAddressToWhitelist(marketplace.address, {from: firstAccount});

        await token.transfer(secondAccount, 10000*TOKEN, {from: firstAccount});
        await token.transfer(thirdAccount, 10000*TOKEN, {from: firstAccount});
        await token.transfer(fourthAccount, 10000*TOKEN, {from: firstAccount});
        await token.transfer(fifthAccount, 10000*TOKEN, {from: firstAccount});

        assert.equal(await token.balanceOf.call(secondAccount), 10000*TOKEN);
        assert.equal(await token.balanceOf.call(thirdAccount), 10000*TOKEN);
        assert.equal(await token.balanceOf.call(fourthAccount), 10000*TOKEN);
        assert.equal(await token.balanceOf.call(fifthAccount), 10000*TOKEN);
        assert.equal(await token.totalSupply.call(), 1000000*TOKEN);
    });

    it("#1 should initialize correctly", async () => {
        assert.equal(await marketplace.owner.call(), firstAccount);
        assert.equal(await marketplace.treasurer.call(), firstAccount);
        assert.equal(await marketplace.arconaToken_.call(), token.address);
        assert.equal(await marketplace.landContract.call(), land.address);
        assert.equal(await marketplace.manorContract.call(), manor.address);

        assert.equal(web3.toBigNumber(await marketplace.minDuration.call()).toString(), 1);
        assert.equal(web3.toBigNumber(await marketplace.maxDuration.call()).toString(), 20160);
        assert.equal(web3.toBigNumber(await marketplace.defaultExecuteTime.call()).toString(), 86400);
        assert.equal(web3.toBigNumber(await marketplace.auctionFee.call()).toString(), 300);
    });


    it("#2 Create land auction", async () => {
        assert.equal(await land.owner.call(), firstAccount);

        await land.addAddressToWhitelist(firstAccount);
        await land.mintLand(secondAccount, 53649020651777508, 0, { from: firstAccount });
        await land.mintLand(thirdAccount, 982132106742, 2, { from: firstAccount });

        assert.equal(web3.toBigNumber(await land.balanceOf(secondAccount)).toString(), 1);
        assert.equal(web3.toBigNumber(await land.balanceOf(thirdAccount)).toString(), 1);
        assert.equal(web3.toBigNumber(await land.totalSupply.call()).toString(), 2);

        await marketplace.createLandAuction(0, 100,  200, { from: secondAccount });
    });


    it("#3 Create manor auction", async () => {
        assert.equal(await land.owner.call(), firstAccount);

        await manor.addAddressToWhitelist(firstAccount);
        await land.addAddressToWhitelist(firstAccount);

        await land.mintLand(secondAccount, 53649020651777508, 0, { from: firstAccount });
        await land.mintLand(secondAccount, 982132106742, 0, { from: firstAccount });
        await land.mintLand(secondAccount, 34343, 0, { from: firstAccount });
        await land.mintLand(secondAccount, 23466098, 0, { from: firstAccount });
        let ids = [0, 1, 2, 3];
        await manor.createManor(secondAccount, ids, 9001232, { from: secondAccount });

        assert.equal(web3.toBigNumber(await manor.balanceOf(secondAccount)).toString(), 1);
        assert.equal(web3.toBigNumber(await manor.totalSupply.call()).toString(), 1);

        await marketplace.createManorAuction(0, 1000,  200, { from: secondAccount });
    });


    it("#4 Create and Buy land", async () => {
        await marketplace.createAndBuyLand(53649020651777508, { from: fourthAccount });

        assert.equal(web3.toBigNumber(await land.balanceOf(marketplace.address)).toString(), 1);
        assert.equal(web3.toBigNumber(await land.totalSupply.call()).toString(), 1);
    });


    it("#5 Create and Buy manor", async () => {
        let gisIds = [78934, 1709382, 222, 30000000];

        await marketplace.createAndBuyManor(542352,  gisIds, { from: fifthAccount });
        assert.equal(web3.toBigNumber(await manor.balanceOf(marketplace.address)).toString(), 1);
        assert.equal(web3.toBigNumber(await manor.totalSupply.call()).toString(), 1);
    });
});
