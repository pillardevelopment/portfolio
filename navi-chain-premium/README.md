# NaviAuction eosio demo APP


- [TO LOCAL START](#to-start)
- [DETAILED GUIDE](#detailed-guide)
- [MANUAL USE TO FUNCTIONS](#manual-use-to-functions)
- [FRONTEND](#frontend)
- [VIEW CURRENT STATUS](#View_current_status)



## To start
```sh
./quick_start.sh
```

The above command will execute the following in sequence:

1. `first_time_setup.sh`
2. `start_eosio_docker.sh`
3. `start_frontend.sh`

#### stop

press `cmdl+c` on your keyboard, and execute:

```sh
docker stop eosio_notechain_container
```

## Detailed guide

In this section we will describe in detail each script used to run the NoteChain environment in details.

### Initial setup

```sh
./first_time_setup.sh
```

Executing the above shell script verifies that docker and node.js are installed. It then downloads the `eosio/eos-dev` docker image (which contains a full version of the eosio blockchain), removes any previous instances of this docker container and installs node packages for the frontend react app.

### Initialise and start blockchain and DApp

After the initialisation, two terminal windows are required, both opened in the repository directory

- The **first terminal window** is for **blockchain** process.
- The **second terminal window** is for **frontend** react app.

**running the blockchain**

For the first (blockchain) terminal window, running
```sh
./start_eosio_docker.sh
```
will:

- Start the eosio blockchain
- Create smart contract owner account,
- Deploy smart contract
- Pre-create 7 user accounts with hard coded keys.

The log of blockchain will be displayed on your screen. eosio is now running and starts producing blocks.

**running the DApp**

For the second (frontend) terminal window, running
```sh
./start_frontend.sh
```
will open a browser session connecting to http://localhost:3000/ showing the react app.

### Stopping blockchain or DApp

**stopping the blockchain**

In the first (blockchain) terminal window, press `cmd+c` on your keyboard, the log will stop printing. And then execute:
```sh
docker stop eosio_notechain_container
```

This action will take a few seconds. The blockchain will be stopped.

**stopping the DApp**

In the second (frontend) terminal window, press `cmd+c` on your keyboard. The frontend react app will be stopped.

### Restarting blockchain or DApp

**restarting the blockchain**

In the first (blockchain) terminal window, execute this command:
```sh
./start_eosio_docker.sh
```

The blockchain will be resumed automatically and the log will be outputted to the terminal.

**restarting the DApp**

In the second (frontend) terminal window, you can restart the frontend react app by executing:
```sh
./start_frontend.sh
```

### Reset blockchain data

First, you need to stop the blockchain (as above) and then execute:
```sh
./first_time_setup.sh
```


## Manual use to functions

##### NaviAddress:

Creation:

```sh
cleos push action naviaddress create '[ "naviaddress", "NAVI"]' -p naviaddress@active
cleos get table naviaddress naviaddress token
cleos get table naviaddress [account_name user] accounts
cleos get table naviaddress NAVI stat
cleos push action naviaddress issue '["tester", "1 NAVI", ["N", "357", "-", "0221"], "NAVI", "create N357-0221"]' -p naviaddress@active
```
Transfer:
```sh
cleos push action naviaddress transfer '["naviaddress", "tester", "0", "transfer humber 0"]' -p naviaddress@active
```

Add metaData:
```sh
cleos push action naviaddress addmeta '["tester", "0", "N357-0221", "0"]' -p tester@active
```

Balance:
```sh
cleos get currency balance naviaddress [account_name user] "NAVI"
```


##### NaviAuction

Create:

```sh
cleos push action naviauction create '[ "tester", "create 0 auction", "0", "1", "4", "600" ]' -p tester@active
cleos get table naviauction naviauction auction
```
Bid:

```sh
cleos push action naviauction bid '["tester", "2", "100"]' -p tester2@active
```



## FrontEnd React APP

```sh
cd frontend
npm i
npm start

./srs/pages/index.js - DAPP

Dapp use:
Local:            http://localhost:3000/
On Your Network:  http://192.168.1.3:3000/


```



Local Configuration(line 20):
```js
const network = {
    blockchain:'eos',
    chainId:'',
    host:'localhost',
    port: 8888,
    protocol:'http'
    };
```


Jungle Configuration:
```js
const network = {
    blockchain:'eos',
    chainId:'038f4b0fc8ff18a4f0842a8f0564611f6e96e8535901dd45e43ac8691a1c4dca',
    host:'jungle.eosio.cr',
    port:443,
    protocol:'https'
    };
```


MainNet Configuration:
```js
const network = {
    blockchain:'eos',
    chainId:'aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906',
    host:'nodes.get-scatter.com',
    port:443,
    protocol:'https'
    };
```


Enpoints(line 43):
```js
const endpoint = "http://localhost:8888";  // LocalHost
const endpoint = "https://jungle.eosio.cr:443";  // Jungle
const endpoint = "https://nodes.get-scatter.com:443"; // MainNet
```

Events for call functions(line 81 - 409):
```js
 async handleFormEventCreate(event) {
        event.preventDefault();

        let account = event.target.account.value;
        let note = event.target.note.value;
        let naviaddress = event.target.naviaddress.value;
        let currentbid = event.target.currentbid.value;
        let buyout = event.target.buyout.value;
        let duration = event.target.duration.value;
        let actionName = "";
        let actionData = {};

        switch (event.type) {
            case "submit":
                actionName = "create";
                actionData = {
                    _owner: account,
                    _note: note,
                    _index: naviaddress,
                    _minbid: currentbid,
                    _buynow: buyout,
                    _duration: duration,
                };
                break;
            default:
                return;
        }

        const scatter_account = scatter.identity.accounts.find(x => x.blockchain === 'eos');
        const opts = { authorization:[`${scatter_account.name}@${scatter_account.authority}`], requiredFields:{} };
        const rpc = new Rpc.JsonRpc(endpoint);
        const signatureProvider = new SignatureProvider([opts]);
        const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });
        try {
            const result = await api.transact({
                actions: [{
                    account: "naviauction",
                    name: actionName,
                    authorization: [{
                        actor: account,
                        permission: 'active',
                    }],
                    data: actionData,
                }]
            }, {
                blocksBehind: 3,
                expireSeconds: 30,
            });

            console.log(result);
            this.getTable();
        } catch (e) {
            console.log('Caught exception: ' + e);
        }
    }
```

Table to output the created auctions(line 413 - 426)
```js
 getTable() {
        const rpc = new Rpc.JsonRpc(endpoint);
        rpc.get_table_rows({
            "json": true,
            "code": "naviauction",   // contract who owns the table
            "scope": "naviauction",  // scope of the table
            "table": "auction",    // name of the table as specified by the contract abi
            "limit": 500,
        }).then(result => this.setState({ noteTable: result.rows }));
    }
```
![](frontend/img/table.png)

## View current status
```sh
http://192.168.1.3:3000/
```

![](frontend/img/main.png)

```sh
http://127.0.0.1:8888/v1/chain/get_info
{
  "server_version": "1e9ca55c",
  "chain_id": "cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f",
  "head_block_num": 108066,
  "last_irreversible_block_num": 108065,
  "last_irreversible_block_id": "0001a621ed517e13734460c3ee88af144540f68b2d42861ab15b9d8813e2c241",
  "head_block_id": "0001a622a247d844dc26d2c5c9736c82ed96aa48399fd1654e90304490e72bfd",
  "head_block_time": "2018-11-29T14:06:50.000",
  "head_block_producer": "eosio",
  "virtual_block_cpu_limit": 200000000,
  "virtual_block_net_limit": 1048576000,
  "block_cpu_limit": 199900,
  "block_net_limit": 1048576,
  "server_version_string": "v1.3.2"
}
```