// @require_auth     --  Verify specified account exists in the set of provided auths.
// @account_name     --  typedef uint64_t account_name( <= 13 chars EOSIO address)
// @eosio_assert     -- Aborts processing of this action and unwinds all pending changes.
// @sha256           -- Hashes data using sha256 and stores result in memory pointed to by hash
// @transaction_size -- Gets the size of the currently executing transaction
// @ "get"_index     -- Defines EOSIO Multi Index Table

 /**
 * @file - navi_Address
 * @copyright NaviaddressCorp
 */
 #include "naviaddress.hpp"
namespace eosio {
    using std::string;
    using eosio::asset;
    using std::map;


        /*
         * функция отвечает за создание nfr токена
         * @param issuer - owner
         * @param sym - передает кооличество (???????)
         * @throws require_auth - выбросит исключение если _self не может подписывать транзакцию (issuer != _self)
         */
        // @abi action
    void navi::create( account_name issuer, string sym ) {
        require_auth( _self );

        eosio_assert( is_account( issuer ), "issuer account does not exist");
        asset supply(0, string_to_symbol(0, sym.c_str()));

        auto symbol = supply.symbol;
        eosio_assert( symbol.is_valid(), "invalid symbol name" );
        eosio_assert( supply.is_valid(), "invalid supply");
        currency_index currency_table( _self, symbol.name() );
        auto existing_currency = currency_table.find( symbol.name() ); //
        eosio_assert( existing_currency == currency_table.end(), "token with symbol already exists" );

        currency_table.emplace( _self, [&]( auto& currency ) {
            currency.supply = supply;
            currency.issuer = issuer;
        });
    }


        /*
         * метод выпуска navi_aaddress
         * @param to - account получателя
         * @param quantity - количество
         * @param uris - вектор для хранения URI токена
         * @param navi.address - вектор строк адреса (пример в хидере)
         * @param name - название -   TODO может тут и класть хеш???
         * @param memo - сопроводительное сообщение получателю
         */
        // @abi action
    void navi::issue( account_name to,
                     asset quantity,
                     vector<string> naviaddress,
                     string name,
                     string memo) {
        eosio_assert( is_account( to ), "to account does not exist");
        symbol_type symbol = quantity.symbol;
        eosio_assert( symbol.is_valid(), "invalid symbol name" );
        eosio_assert( symbol.precision() == 0, "quantity must be a whole number" );
        eosio_assert( memo.size() <= 256, "memo has more than 256 bytes" );

        eosio_assert( name.size() <= 32, "name has more than 32 bytes" );
        eosio_assert( name.size() > 0, "name is empty" );

        auto symbol_name = symbol.name();
        currency_index currency_table( _self, symbol_name );
        auto existing_currency = currency_table.find( symbol_name );
        eosio_assert( existing_currency != currency_table.end(), "token with symbol does not exist, create token before issue" );
        const auto& st = *existing_currency; //

        require_auth( st.issuer );
        eosio_assert( quantity.is_valid(), "invalid quantity" );
        eosio_assert( quantity.amount > 0, "must issue positive quantity of NFTs" );
        eosio_assert( symbol == st.supply.symbol, "symbol precision mismatch" );
        _add_supply( quantity );
            auto _hash = _vctr2str(naviaddress);
            _mint( to, st.issuer, asset{1, symbol}, _hash, name);
        _add_balance( to, quantity, st.issuer );
    }


        /* nft transfer
         * @param from - отправитель
         * @param to - получатель
         * @param id - уникальный id токена
         * @param memo - сопроводительное сообщение получателю
         */
        // @abi action
    void navi::transfer( account_name from,
                        account_name to,
                        id_type      id,
                        string       memo ) {
        eosio_assert( from != to, "cannot transfer to self" );   // проверка авторизации аккаунта отправителя
        require_auth( from );
        eosio_assert( is_account( to ), "to account does not exist");
        eosio_assert( memo.size() <= 256, "memo has more than 256 bytes" );
        auto sender_token = tokens.find( id );
        eosio_assert( sender_token != tokens.end(), "token with specified ID does not exist" );
        eosio_assert( sender_token->owner == from, "sender does not own token with specified ID");
        const auto& st = *sender_token;
        require_recipient( from );
        require_recipient( to );
        tokens.modify( st, from, [&]( auto& token ) {
            token.owner = to;
        });
        _sub_balance( from, st.value );
        _add_balance( to, st.value, from );
    }


        /* определение плательщика RAM
         * @param payer - принимает адрес нового плательщика RAM
         * @param id - id nf токена
         */
        // @abi action
    void navi::setrampayer(account_name payer, id_type id) {
        require_auth(payer);

        auto payer_token = tokens.find( id );
        eosio_assert( payer_token != tokens.end(), "token with specified ID does not exist" );

        eosio_assert( payer_token->owner == payer, "payer does not own token with specified ID");

        const auto& st = *payer_token;

        require_recipient( payer );
        tokens.erase(payer_token);
        tokens.emplace(payer, [&](auto& token) {
            token.id = st.id;
            token.owner = st.owner;
            token.value = st.value;
            token.name = st.name;
        });
        _sub_balance( payer, st.value );
        _add_balance( payer, st.value, payer );
    }


        /* Сжигание nf токена
         * @param owner - владелец аккаунта
         * @param token_id - id токена
         */
        // @abi action
    void navi::burn( account_name owner, id_type token_id ) {
        require_auth( owner );

        auto burn_token = tokens.find( token_id );
        eosio_assert( burn_token != tokens.end(), "token with id does not exist" );
        eosio_assert( burn_token->owner == owner, "token not owned by account" );
        asset burnt_supply = burn_token->value;

        tokens.erase( burn_token );
        _sub_balance( owner, burnt_supply );
        _sub_supply( burnt_supply );
    }


        /*
         * @param owner
         * @param id
         * @param ipfs
         * @param size
         * */
         // @abi action
    void navi::addmeta(account_name owner, uint64_t id, const string ipfs, uint8_t size) {
        auto _token = tokens.find( id );
            tokens.modify(_token, owner, [&](auto& _updtoken) {
                string tmp_str = _generateID(ipfs); // TODO тут необхолимо передавать вектор который возвращает число id
                _updtoken.ipfs_hash = tmp_str;
                _updtoken.ipfs_size = size;
            });
            size_t cur_size = transaction_size();
            eosio::print("currency size ", cur_size);
            eosio::print(" metadata#", id, " updated");
    }


        /* Mint non fungible tokens
              * @param owner - владелец
              * @param ram_payer - плательщик памяти
              * @param value - структура типа asset - (количество через пробел символ string)
              * @param uri - id
              * @param name - наименование
              */
    void navi::_mint( account_name owner,
                      account_name ram_payer,
                      asset value,
                      string _navi,
                      string name) {
        tokens.emplace( ram_payer, [&]( auto& _token ) {
            _token.id = tokens.available_primary_key();
            _token.naviaddress = _navi;
            _token.owner = owner;
            _token.value = value;
            _token.name = name;
            _token.ipfs_hash = "null";
            _token.ipfs_size = 0;
        });
    }


                /* private функция  - не идет в -g ABI увеличение баланса
                * @param owner - account_name владельца
                * @param value - структура {quantity, symbol}
                */
        void navi::_add_balance( account_name owner, asset value, account_name ram_payer ) {
            account_index to_accounts( _self, owner );
            auto to = to_accounts.find( value.symbol.name() );
            if( to == to_accounts.end() ) {
                to_accounts.emplace( ram_payer, [&]( auto& a ){
                    a.balance = value;
                });
            } else {
                to_accounts.modify( to, 0, [&]( auto& a ) {
                    a.balance += value;
                });
            }
        }


        /* private функция  - не идет в -g ABI уменьшения баланса
        * @param owner - account_name владельца
        * @param value - структура {quantity, symbol}
        */
        void navi::_sub_balance( account_name owner, asset value ) {
            account_index from_acnts( _self, owner );

            const auto& from = from_acnts.get( value.symbol.name(), "no balance object found" );
            eosio_assert( from.balance.amount >= value.amount, "overdrawn balance" );

            if( from.balance.amount == value.amount ) {
                from_acnts.erase( from );
            } else {
                from_acnts.modify( from, owner, [&]( auto& a ) {
                    a.balance -= value;
                });
            }
        }


        /* private функция  - не идет в -g ABI увеличение общего выпуска
        * @param quantity - структура {quantity, symbol}
        */
        void navi::_add_supply( asset quantity ) {
            auto symbol_name = quantity.symbol.name();
            currency_index currency_table( _self, symbol_name );
            auto current_currency = currency_table.find( symbol_name );
            currency_table.modify( current_currency, 0, [&]( auto& currency ) {
                currency.supply += quantity;
            });
        }


        /* private функция  - не идет в -g ABI уменьшение общего выпуска
         * @param quantity - структура {quantity, symbol}
         */
        void navi::_sub_supply( asset quantity ) {
            auto symbol_name = quantity.symbol.name();
            currency_index currency_table( _self, symbol_name );
            auto current_currency = currency_table.find( symbol_name );
            currency_table.modify( current_currency, 0, [&]( auto& currency ) {
                currency.supply -= quantity;
            });
        }


        /* private функция - для конкатенации элементов строкового вектора в строку
         * @code - Convert stl::_vector_type to stl::string_type
         * @param _vctr - stl::vector_type(include <vector> lib)
         */
            string navi::_vctr2str(vector<string>_vctr) {
                string _str;
                for (auto s : _vctr) {
                    _str+=s;
                }
                return _str;
            }


            /*  convert checksum256 2 string
             * @param c
             * */
            string navi::_checksum256_to_string(checksum256 c) {
                char hexstr[64];
                for (int i=0; i<32; i++) sprintf(hexstr+i*2, "%02x", c.hash[i]);
                string c_str = string(hexstr);
                return c_str;
            }


        /*  The sha256 method need a char * parameter
         * @param _straddr
         * */
        string navi::_generateID(string _straddr) { // todo должен принимать вектор строк vector<string>_stradd
            checksum256 calc_hash;

            sha256((char *)&_straddr, sizeof(char), &calc_hash);

            string _str = _checksum256_to_string(calc_hash);
            return  _str;
        }
    EOSIO_ABI( navi, (create)(issue)(transfer)(setrampayer)(burn)(addmeta))

} /// namespace eosio
