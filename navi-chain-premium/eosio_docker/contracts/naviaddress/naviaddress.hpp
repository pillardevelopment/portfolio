 /**
  *  @file
  *  @copyright defined in eos/LICENSE.txt
 */
#pragma once

#include <eosiolib/eosio.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/crypto.h>
#include <eosiolib/transaction.hpp>

 namespace eosiosystem {
    class system_contract;
}

namespace eosio {

    using std::string;

    typedef uint128_t uuid; // пользовательский тип данных для хранения table get_global_id
    typedef uint64_t id_type; // пользовательский тип данных для хранения уникального id токена
    typedef string uri_type; // пользовательский тип данных для хранения уникальной строки токена

    class navi : public contract {
    public:
        navi(account_name self) : contract(self), tokens(_self, _self) {} // конструктор класса


        /*
         * Creates token with a symbol name for the specified issuer account.
         * Throws if token with specified symbol already exists.
         * @param issuer - Account name of the token issuer
         * @param symbol - Symbol code of the token
         * */
        void create(account_name issuer, string symbol); // интерфейс функции navi::Create (описана в .cpp)


        /*
         * Issues specified number of tokens with previously created symbol to the account name "to".
         * Each token is generated with an unique token_id assigned to it. Requires authorization from the issuer.
         * Any number of tokens can be issued.
         * @param to Account name of tokens receiver
         * @param quantity Number of tokens to issue for specified symbol (positive integer number)
         * @param uris Vector of URIs for each issued token (size is equal to tokens number)
         * @param name Name of issued tokens
         * @param memo Action memo (max. 256 bytes)
         * */
        void issue(account_name to, // интрефейс функции navi::Issue (описана в .cpp)
                   asset quantity,
                   // параметр передается в формате вектор массива строк адреса (“N@”+ number + “-” + container1 + “-”
                   // + container2 + … ) // our NFT ( ERC721 )
                   // example ["N@", "3", "-", "333", "-", "33", "-", "303", "-", "111"]
                   vector<string> naviaddress,
                   // vector<string> uris,
                   string name,
                   string memo);


        /*
         * Transfers 1 token with specified "id" from account "from" to account "to".
         * Throws if token with specified "id" does not exist, or "from" is not the token owner.
         * @param from Account name of token owner
         * @param to Account name of token receiver
         * @param id Unique ID of the token to transfer
         * @param memo Action memo (max. 256 bytes)
         * */
        void transfer(account_name from, // интрефейс фунции navi::Transfer (описана в .cpp)
                      account_name to,
                      id_type id,
                      string memo);


        /*
         * @param id
         * @param payer
         * */
       void setrampayer(account_name payer, id_type id); // интрефейс функции navi::SetRAMpayer


        /*
         * @notice Burns 1 token with specified "id" owned by account name "owner".
         * @param owner Account name of token owner
         * @param id Unique ID of the token to burn
         * */
        void burn(account_name owner, // интерфейс функции navi::Burn(описана в.cpp)
                  id_type token_id);


        /*
         * @param owner
         * @param id
         * @param ipfs
         * @param size
         * */
        void addmeta(account_name owner, uint64_t id, const string ipfs, uint8_t size);


        /*
         * Structure keeps information about the balance of tokens
         * for each symbol that is owned by an account.
         * This structure is stored in the multi_index table.
         * */
        // @abi table accounts i64
        struct account {  // для table multi_index - хранит баланс в структтуре balance{qual, symbol} по ключу
            asset balance;
            uint64_t primary_key() const { return balance.symbol.name(); }
        };


        /*
         * Structure keeps information about the total supply
         * of tokens for each symbol issued by "issue" account.
         * This structure is stored in the multi_index table.
         **/
        // @abi table stat i64
        struct stats {
            // для table multi_index - хранит общую эмиссию в структуре supply{qual, symbol} и адресе эмитента по ключу
            asset supply;
            account_name issuer;

            uint64_t primary_key() const { return supply.symbol.name(); }
            account_name get_issuer() const { return issuer; }
        };


        /*
         * Structure keeps information about each issued token.
         * Each token is assigned a global unique ID when it is issued.
         * Token also keeps track of its owner, stores assigned URI and its symbol code.
         * This structure is stored in the multi_index table "tokens".
         **/
        // @abi table token i64
        struct token {
            id_type id;          // уникальный 64 bit идентификатор,
            uri_type naviaddress; // string sha256 hash
            // uri_type uri;      // RFC 3986 standart
            account_name owner;  // token owner
            asset value;         // token value (1 SYS)
            string name;	 // token name (SYMBOL)
            string ipfs_hash; // передается из return ipfs add [uri] - multi_hash
            uint8_t ipfs_size;

            id_type primary_key() const { return id; }

            account_name get_owner() const { return owner; }
            string get_naviaddress() const { return naviaddress; }
            asset get_value() const { return value; }
            // string get_uri() const { return uri; }
            uint64_t get_symbol() const { return value.symbol.name(); }
            uint64_t get_name() const { return string_to_name(name.c_str()); }
            string get_ipfs() const { return ipfs_hash; }
            uint8_t get_ipfs_size() const { return ipfs_size; }

            EOSLIB_SERIALIZE(token, (id)(naviaddress)(owner)(value)(name)(ipfs_hash)(ipfs_size))
        };


        /*
         * Account balance table
         * Primary index:
         * owner account name
         **/
        // @abi table token i64
        using account_index = eosio::multi_index<N(accounts), account>;


        /*
         * Issued tokens statistics table
         * Primary index:
         * token symbol name
         * Secondary indexes:
         * issuer account name
         * */
        using currency_index = eosio::multi_index<N(stat), stats,
                indexed_by< N( byissuer ), const_mem_fun< stats, account_name, &stats::get_issuer> > >;


        /*
         * Issued tokens table
         * Primary index:
         * token id
         * Seconday indexes:
         * owner account name
         * token symbol name
         * issued token name
         * */
        using token_index = eosio::multi_index<N(token), token,
            indexed_by< N( byowner ), const_mem_fun< token, account_name, &token::get_owner> >,
            indexed_by< N( bysymbol ), const_mem_fun< token, uint64_t, &token::get_symbol> >,
            indexed_by< N( byname ), const_mem_fun< token, uint64_t, &token::get_name> > >;

        // при добавлении индекса валится компиляция
            // indexed_by< N( byipfs ), const_mem_fun< token, string, &token::get_ipfs> >;
            //indexed_by< N( byipfssize ), const_mem_fun< token, uint8_t, &token::get_ipfs_size> > >;


    private:

        friend eosiosystem::system_contract;

        token_index tokens;

        void _mint(account_name owner, account_name ram_payer, asset value, string naviaddress, string name);

        void _sub_balance(account_name owner, asset value);

        void _add_balance(account_name owner, asset value, account_name ram_payer);

        void _sub_supply(asset quantity);

        void _add_supply(asset quantity);

        string _vctr2str(vector<string>_vctr);

        string _generateID(string _straddr);

        string _checksum256_to_string(checksum256 c);
    };
 } // namespace eosio