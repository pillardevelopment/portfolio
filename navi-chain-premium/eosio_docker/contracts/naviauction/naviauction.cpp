#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>


using namespace eosio;

class naviauction : public eosio::contract {
  private:

    uint64_t _priceRatio = 2;

    account_name _storeControllerAddress;

    uint64_t _defaultAuctionTimeSec = 1 * 60 * 60 * 24; //1 day

    bool isnewowner( account_name owner ) {
      notetable noteobj(_self, _self);

      auto notes = noteobj.get_index<N(getbyowner)>();
      auto note = notes.find(owner);

      return note == notes.end();
    }

    // @abi table auction i64
    struct auction {
        uint64_t      prim_key;
        uint64_t      naviaddress;
        account_name  owner;      // account name for the owner
        account_name  buyer; // 5-13 char eos address
        uint64_t      curbid; // last bid
        uint64_t      minbd; // min bid
        uint64_t      buynwprice; // buy now price
        std::string   note;      // the note message
        uint64_t      endtime; // the store the last create block time
        bool          ended;
        bool          baseauct;

      // primary key
      auto primary_key() const { return prim_key; }
      // secondary key: owner
      uint64_t get_by_index() const { return owner; }
    };



    // create a multi-index table and support secondary key
    typedef eosio::multi_index< N(auction), auction,
      indexed_by< N(getbyowner), const_mem_fun<auction, uint64_t, &auction::get_by_index> >
      > notetable;



    void _takeAddressProcess( account_name _account, uint64_t& _index ) {

        notetable obj(_self, _self); // code, scope

        auto itr = obj.find(_index);

        obj.modify( itr, _account, [&]( auto& auction ) {

            INLINE_ACTION_SENDER(naviaddress, transfer)(
                    N(naviaddress),
                    {_account,N(active)},
                    { auction.owner, _account, _index, std::string("transfer NAVIADDRESS") } );
        });

            trnsfright(auction.owner, auction.buyer, auction.naviaddress);
        });
    }


public:
    using contract::contract;

    // @abi action
    void create( account_name _owner,
                 std::string& _note,
                 uint64_t&    _index,
                 uint64_t&    _minbid,
                 uint64_t&    _buynow,
                 uint64_t&    _duration ) {

      require_auth( _owner );

      notetable obj(_self, _self); // code, scope

        // auto itr = obj.find(_index);
        // eosio_assert(itr == obj.end(), "auction for _index already exists");

      if (isnewowner(_owner)) {

            obj.emplace(_self, [&](auto& _auction) {
                _auction.prim_key    = obj.available_primary_key(); // ok
                _auction.owner        = _owner; // ok
                _auction.naviaddress = _index;
                _auction.buyer = _owner; // ok
                _auction.curbid = 0;
                _auction.minbd = _minbid;
                _auction.buynwprice = _buynow;
                _auction.note        = _note;
                _auction.endtime = now() + _duration;
                _auction.ended = false; // ok
                _auction.baseauct = false; // ok
            });

      } else {

        auto notes = obj.get_index<N(getbyowner)>();
        auto &note = notes.get(_owner);

        obj.modify( note, _self, [&]( auto& address ) {
            address.naviaddress = _index;
          address.note        = _note;
          address.endtime   = now();
        });
      }
    }



    // @abi action
    void createbase ( account_name    _owner,
                      uint64_t&  _index,
                      uint64_t&  _basePrice,
                      uint64_t&  _bid,
                      std::string& _note,
                      uint32_t&  _duration) {
        require_auth( _owner );

        notetable obj(_self, _self); // code, scope

        // auto itr = obj.find(_index);
        // eosio_assert(itr == obj.end(), "auction for _index already exists");

        obj.emplace(_self, [&](auto& _auction) {
            _auction.prim_key    = obj.available_primary_key();
            _auction.owner        = _owner;
            _auction.buyer = _owner;
            _auction.naviaddress = _index;
            _auction.curbid = 0;
            _auction.minbd = _basePrice;
            _auction.buynwprice = _bid * _priceRatio;
            _auction.note        = _note;
            _auction.endtime = now() + _duration;
            _auction.ended = false;
            _auction.baseauct = true;
        });
    }



    // @abi action
    void trnsfright( account_name _account, account_name _newOwner,  uint64_t& _index ) {
        require_auth( _account );

        notetable obj(_self, _self); // code, scope

        auto itr = obj.find(_index);

        eosio_assert(itr != obj.end(), "Index for auction not found");

        obj.modify( itr, _account, [&]( auto& auction ) {
            auction.owner = _newOwner;
            auction.buyer = _newOwner;
            auction.endtime = now();
            auction.ended = true;
        });
    }



    // @abi action
    void bid( account_name _account, uint64_t& _index, uint64_t& _bid) {

        require_auth( _account );

        notetable obj(_self, _self); // code, scope

        uint64_t commission = _bid/10;

        uint64_t dep_or_burn = _bid - commission;

        auto itr = obj.find(_index);

        eosio_assert(itr != obj.end(), "Index for auction not found");

        obj.modify( itr, _account, [&]( auto& auction ) {
            eosio_assert(_bid >= auction.minbd, "invalid bid value");
            eosio_assert(_bid > auction.curbid, "invalid bid value");
            eosio_assert(auction.ended == false, "auction is over");
            eosio_assert(now() <= auction.endtime, "auction is over");

            auction.curbid = _bid;
            auction.buyer = _account;

        INLINE_ACTION_SENDER(eosio.token, transfer)(
                N(eosio.token),
                {_account,N(active)},
                { _account, auction.owner, auction.curbid, std::string("bid") } );
        });
    }



    // @abi action
    void takeaddr( account_name _account, uint64_t&    _index ) {

        require_auth( _account );
        notetable obj(_self, _self); // code, scope

        auto itr = obj.find(_index);
        eosio_assert(itr != obj.end(), "Index for auction not found");

        _takeAddressProcess(_account, _index);
    }



    // @abi action
    void buyitnow( account_name _account, uint64_t& _index ) {

        require_auth( _account );
        notetable obj(_self, _self); // code, scope

        auto itr = obj.find(_index);
        eosio_assert(itr != obj.end(), "Index for auction not found");

        uint64_t _buynwprice;

        obj.modify( itr, _account, [&]( auto& auction ) {
            eosio_assert(auction.ended == false, "auction is over");
            eosio_assert(auction.buynwprice != 0, "invalid price value");
            eosio_assert(auction.curbid < auction.curbid, "invalid bid value");
            eosio_assert(auction.owner == _account, "autotification error");

            _buynwprice = auction.buynwprice;
            bid(_index, _buynwprice, _account);
            _takeAddressProcess(_account, _index );
        });
    }
};

EOSIO_ABI( naviauction, (create)(createbase)(trnsfright)(bid)(takeaddr)(buyitnow) )
