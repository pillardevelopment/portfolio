import React, { Component } from 'react';
import { Api, Rpc, SignatureProvider } from 'eosjs';
import { TextDecoder, TextEncoder } from 'text-encoding';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import ScatterJS from 'scatterjs-core';
import ScatterEOS from 'scatterjs-plugin-eosjs';

ScatterJS.plugins( new ScatterEOS() );
console.log(ScatterJS+ " Successful connected to DAPP");
let scatter;

const network = {
    blockchain:'eos',
    chainId:'',
    host:'localhost',
    port: 8888,
    protocol:'http'
};

const connectionOptions = {initTimeout:10000}

ScatterJS.scatter.connect("Navi.Auction Demo", connectionOptions).then(connected => {
    if(!connected)  return false;

    scatter = ScatterJS.scatter;
    const requiredFields = { accounts:[network] };
    scatter.getIdentity(requiredFields).then(() => {

    }).catch(error => {
        console.error(error);
    });
});

// eosio endpoint
const endpoint = "http://localhost:8888";

const styles = theme => ({
    card: {
        margin: 15,
    },
    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    formButton: {
        marginTop: theme.spacing.unit,
        width: "100%",
    },
    pre: {
        background: "#ccc",
        padding: 5,
        marginBottom: 0,
    },
});

// Index component
class Index extends Component {

    constructor(props) {
        super(props)
        this.state = {
            noteTable: [] // to store the table rows from smart contract
        };
        this.handleFormEventCreate = this.handleFormEventCreate.bind(this);
        this.handleFormEventCreateBase = this.handleFormEventCreateBase.bind(this);
        this.handleFormEventBid = this.handleFormEventBid.bind(this);
        this.handleFormEventBuyNow = this.handleFormEventBuyNow.bind(this);
        this.handleFormEventTakeAddress = this.handleFormEventTakeAddress.bind(this);
        this.handleFormEventTransferRight = this.handleFormEventTransferRight.bind(this);
    }

    async handleFormEventCreate(event) {
        event.preventDefault();

        // collect form data
        let account = event.target.account.value;
        let note = event.target.note.value;
        let naviaddress = event.target.naviaddress.value;
        let currentbid = event.target.currentbid.value;
        let buyout = event.target.buyout.value;
        let duration = event.target.duration.value;

        // prepare variables for the switch below to send transactions
        let actionName = "";
        let actionData = {};

        // define actionName and action according to event type
        switch (event.type) {
            case "submit":
                actionName = "create";
                actionData = {
                    _owner: account,
                    _note: note,
                    _index: naviaddress,
                    _minbid: currentbid,
                    _buynow: buyout,
                    _duration: duration,
                };
                break;
            default:
                return;
        }


        // eosjs function call: connect to the blockchain
        const scatter_account = scatter.identity.accounts.find(x => x.blockchain === 'eos');
        const opts = { authorization:[`${scatter_account.name}@${scatter_account.authority}`], requiredFields:{} };

        const rpc = new Rpc.JsonRpc(endpoint);
        const signatureProvider = new SignatureProvider([opts]);
        const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

        try {
            const result = await api.transact({
                actions: [{
                    account: "naviauction",
                    name: actionName,
                    authorization: [{
                        actor: account,
                        permission: 'active',
                    }],
                    data: actionData,
                }]
            }, {
                blocksBehind: 3,
                expireSeconds: 30,
            });

            console.log(result);
            this.getTable();
        } catch (e) {
            console.log('Caught exception: ' + e);
        }
    }

    async handleFormEventBid(event) {
        event.preventDefault();

        let account = event.target.account.value;
        let naviaddress = event.target.naviaddress.value;
        let currentbid = event.target.currentbid.value;

        let actionName = "";
        let actionData = {};

        switch (event.type) {
            case "submit":
                actionName = "bid";
                actionData = {
                    _account: account,
                    _index: naviaddress,
                    _bid: currentbid,
                };
                break;
            default:
                return;
        }

        const scatter_account = scatter.identity.accounts.find(x => x.blockchain === 'eos');
        const opts = { authorization:[`${scatter_account.name}@${scatter_account.authority}`], requiredFields:{} };

        const rpc = new Rpc.JsonRpc(endpoint);
        const signatureProvider = new SignatureProvider([opts]);
        const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

        try {
            const result = await api.transact({
                actions: [{
                    account: "naviauction",
                    name: actionName,
                    authorization: [{
                        actor: account,
                        permission: 'active',
                    }],
                    data: actionData,
                }]
            }, {
                blocksBehind: 3,
                expireSeconds: 30,
            });

            console.log(result);
            this.getTable();
        } catch (e) {
            console.log('Caught exception: ' + e);
        }
    }

    async handleFormEventCreateBase(event) {
        event.preventDefault();

        let account = event.target.account.value;
        let note = event.target.note.value;
        let naviaddress = event.target.naviaddress.value;
        let basePrice = event.target.basePrice.value;
        let minimalbid = event.target.minimalbid.value;
        let duration = event.target.duration.value;

        let actionName = "";
        let actionData = {};

        switch (event.type) {
            case "submit":
                actionName = "createbase";
                actionData = {
                    _owner: account,
                    _index: naviaddress,
                    _basePrice: basePrice,
                    _bid: minimalbid,
                    _note: note,
                    _duration: duration,
                };
                break;
            default:
                return;
        }

        const scatter_account = scatter.identity.accounts.find(x => x.blockchain === 'eos');
        const opts = { authorization:[`${scatter_account.name}@${scatter_account.authority}`], requiredFields:{} };

        const rpc = new Rpc.JsonRpc(endpoint);
        const signatureProvider = new SignatureProvider([opts]);
        const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

        try {
            const result = await api.transact({
                actions: [{
                    account: "naviauction",
                    name: actionName,
                    authorization: [{
                        actor: account,
                        permission: 'active',
                    }],
                    data: actionData,
                }]
            }, {
                blocksBehind: 3,
                expireSeconds: 30,
            });

            console.log(result);
            this.getTable();
        } catch (e) {
            console.log('Caught exception: ' + e);
        }
    }

    async handleFormEventTransferRight(event) {
        event.preventDefault();

        let currentOwner = event.target.currentOwner.value;
        let newOwner = event.target.newOwner.value;
        let index = event.target.index.value;

        let actionName = "";
        let actionData = {};

        switch (event.type) {
            case "submit":
                actionName = "trnsfright";
                actionData = {
                    _account: currentOwner,
                    _newOwner: newOwner,
                    _index: index,
                };
                break;
            default:
                return;
        }
        const scatter_account = scatter.identity.accounts.find(x => x.blockchain === 'eos');
        const opts = { authorization:[`${scatter_account.name}@${scatter_account.authority}`], requiredFields:{} };

        const rpc = new Rpc.JsonRpc(endpoint);
        const signatureProvider = new SignatureProvider([opts]);
        const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

        try {
            const result = await api.transact({
                actions: [{
                    account: "naviauction",
                    name: actionName,
                    authorization: [{
                        actor: currentOwner,
                        permission: 'active',
                    }],
                    data: actionData,
                }]
            }, {
                blocksBehind: 3,
                expireSeconds: 30,
            });

            console.log(result);
            this.getTable();
        } catch (e) {
            console.log('Caught exception: ' + e);
        }
    }

    async handleFormEventBuyNow(event) {
        event.preventDefault();

        let account = event.target.account.value;
        let index = event.target.index.value;

        let actionName = "";
        let actionData = {};

        switch (event.type) {
            case "submit":
                actionName = "buyitnow";
                actionData = {
                    _account: account,
                    _index: index,
                };
                break;
            default:
                return;
        }

        const scatter_account = scatter.identity.accounts.find(x => x.blockchain === 'eos');
        const opts = { authorization:[`${scatter_account.name}@${scatter_account.authority}`], requiredFields:{} };

        const rpc = new Rpc.JsonRpc(endpoint);
        const signatureProvider = new SignatureProvider([opts]);
        const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

        try {
            const result = await api.transact({
                actions: [{
                    account: "naviauction",
                    name: actionName,
                    authorization: [{
                        actor: account,
                        permission: 'active',
                    }],
                    data: actionData,
                }]
            }, {
                blocksBehind: 3,
                expireSeconds: 30,
            });

            console.log(result);
            this.getTable();
        } catch (e) {
            console.log('Caught exception: ' + e);
        }
    }

    async handleFormEventTakeAddress(event) {
        event.preventDefault();

        let account = event.target.account.value;
        let index = event.target.index.value;

        let actionName = "";
        let actionData = {};

        switch (event.type) {
            case "submit":
                actionName = "takeaddr";
                actionData = {
                    _account: account,
                    _index: index,
                };
                break;
            default:
                return;
        }

        const scatter_account = scatter.identity.accounts.find(x => x.blockchain === 'eos');
        const opts = { authorization:[`${scatter_account.name}@${scatter_account.authority}`], requiredFields:{} };

        const rpc = new Rpc.JsonRpc(endpoint);
        const signatureProvider = new SignatureProvider([opts]);
        const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

        try {
            const result = await api.transact({
                actions: [{
                    account: "naviauction",
                    name: actionName,
                    authorization: [{
                        actor: account,
                        permission: 'active',
                    }],
                    data: actionData,
                }]
            }, {
                blocksBehind: 3,
                expireSeconds: 30,
            });

            console.log(result);
            this.getTable();
        } catch (e) {
            console.log('Caught exception: ' + e);
        }
    }

    // gets table data from the blockchain
    // and saves it into the component state: "noteTable"
    getTable() {
        const rpc = new Rpc.JsonRpc(endpoint);
        rpc.get_table_rows({
            "json": true,
            "code": "naviauction",   // contract who owns the table
            "scope": "naviauction",  // scope of the table
            "table": "auction",    // name of the table as specified by the contract abi
            "limit": 500,
        }).then(result => this.setState({ noteTable: result.rows }));
    }

    componentDidMount() {
        this.getTable();
    }
    render() {
        const { noteTable } = this.state;
        const { classes } = this.props;

        const generateCard = (key, naviaddress, owner, curbid, buynwprice, endtime, note) => (
            <Card className={classes.card} key={key}>
                <CardContent>
                    <Typography variant="headline" component="h6" >
                        {naviaddress}
                    </Typography>
                    <Typography style={{fontSize:14}}>
                        {owner}
                    </Typography>
                    <Typography style={{fontSize:14}}>
                        {curbid}
                    </Typography>
                    <Typography style={{fontSize:14}}>
                        {buynwprice}
                    </Typography>
                    <Typography style={{fontSize:10}} color="textSecondary" gutterBottom>
                        {new Date(endtime*1000).toString()}
                    </Typography>
                    <Typography component="pre">
                        {note}
                    </Typography>
                </CardContent>
            </Card>
        );
        let noteCards = noteTable.map((row, i) =>
            generateCard(i, row.naviaddress, row.owner, row.curbid, row.buynwprice, row.endtime,  row.note));

        // HTML
        return (
            <div>
                <AppBar position="static" color="default">
                    <Toolbar>
                        <Typography variant="title" color="inherit">
                            Navi.Auction Demo Сhallenge
                        </Typography>
                    </Toolbar>
                </AppBar>
                
                <Paper className={classes.paper}>
                    <form onSubmit={this.handleFormEventCreate}>
                        <TextField
                            name="account"
                            autoComplete="off"
                            label="Account Owner"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="naviaddress"
                            autoComplete="off"
                            label="NaviAddress Index"
                            margin="normal"
                            fullWidth
                        />

                        <TextField
                            name="currentbid"
                            autoComplete="off"
                            label="Minimal Price"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="buyout"
                            autoComplete="off"
                            label="Buy Out Price"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="duration"
                            autoComplete="off"
                            label="Duration"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="note"
                            autoComplete="off"
                            label="Note (Optional)"
                            margin="normal"
                            multiline
                            rows="2"
                            fullWidth
                        />
                        <Button
                            variant="contained"
                            color="secondary"
                            className={classes.formButton}
                            type="submit">
                            CREATE
                        </Button>
                    </form>
                </Paper>

                <Paper className={classes.paper}>
                    <form onSubmit={this.handleFormEventCreateBase}>
                        <TextField
                            name="account"
                            autoComplete="off"
                            label="Account Owner"
                            margin="normal"
                            width="200"
                        />

                        <TextField
                            name="naviaddress"
                            autoComplete="off"
                            label="NaviAddress Index"
                            margin="normal"
                            width="200"
                        />

                        <TextField
                            name="basePrice"
                            autoComplete="off"
                            label="Base Price"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="minimalbid"
                            autoComplete="off"
                            label="Minimal Bid Price"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="duration"
                            autoComplete="off"
                            label="Duration"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="note"
                            autoComplete="off"
                            label="Note (Optional)"
                            margin="normal"
                            multiline
                            rows="2"
                            fullWidth
                        />
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.formButton}
                            type="submit">
                            CREATE BASE_AUCTION
                        </Button>
                    </form>
                </Paper>

                <Paper className={classes.paper}>
                    <form onSubmit={this.handleFormEventBid}>
                        <TextField
                            name="naviaddress"
                            autoComplete="off"
                            label="Auction Number"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="account"
                            autoComplete="off"
                            label="Account Owner"
                            margin="normal"
                            width="200"
                        />

                        <TextField
                            name="currentbid"
                            autoComplete="off"
                            label="BID"
                            margin="normal"
                            width="200"
                        />

                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.formButton}
                            type="submit">
                            BID
                        </Button>
                    </form>
                </Paper>

                <Paper className={classes.paper}>
                    <form onSubmit={this.handleFormEventTransferRight}>
                        <TextField
                            name="index"
                            autoComplete="off"
                            label="Auction Number"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="currentOwner"
                            autoComplete="off"
                            label="Current Owner"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="newOwner"
                            autoComplete="off"
                            label="New Owner"
                            margin="normal"
                            width="200"
                        />

                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.formButton}
                            type="submit">
                            Transfer Right
                        </Button>
                    </form>
                </Paper>

                <Paper className={classes.paper}>
                    <form onSubmit={this.handleFormEventTakeAddress}>
                        <TextField
                            name="index"
                            autoComplete="off"
                            label="Auction Number"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="account"
                            autoComplete="off"
                            label="Account"
                            margin="normal"
                            width="200"
                        />

                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.formButton}
                            type="submit">
                            Take Address
                        </Button>
                    </form>
                </Paper>

                <Paper className={classes.paper}>
                    <form onSubmit={this.handleFormEventBuyNow}>
                        <TextField
                            name="index"
                            autoComplete="off"
                            label="Auction Number"
                            margin="normal"
                            width="200"
                        />
                        <TextField
                            name="account"
                            autoComplete="off"
                            label="Account"
                            margin="normal"
                            width="200"
                        />

                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.formButton}
                            type="submit">
                            Buy Now
                        </Button>
                    </form>
                </Paper>

                {noteCards}
            </div>
        );
    }
}
export default withStyles(styles)(Index);